## Panorama
Tabs: 
- Events
    + Booking
    + Invites
- Packages
    + List
- Visits
    + History
    + Upcoming
    + Create Visit
- Requests
    + Open Requests
    + Completed
    + Create Requests
- Chat
    + Chat UI with Customer Service
- Settings
    + Family Permission

### Redux
- actions: types and redux actions
- reducers: redux reducers
- store: middleware and configuration

### Router and Screens
Routes are contained in src/root.js

### Global files
- Apis: Located in src/const/apis.js
- Lang: Located in src/const/lang.js

### Run IOS
- Podfile is used to install dependencies
- Open ios/panorama.xcworkspace
- Run & Build on Xcode

### Run IOS on Device
- Configure IP on RCTWebSocketExecutor.m
- Run & Build on Xcode
