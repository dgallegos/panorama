import React from 'react'
import {StyleSheet,Platform} from 'react-native'

export const MainStyle = StyleSheet.create({
  tabTop:{
    // flex: 1,
    marginTop: (Platform.OS=='android')?50:80,
    flexGrow: 1
  },
  tabTopDetail:{
    // flex: 1,
    marginTop: (Platform.OS=='android')? 50 : 65,
    flexGrow: 1
  },
  searchBar:{
    paddingTop:8,
    paddingBottom:4,
    backgroundColor: '#fff',
    height: 30,
    margin: 6,
    textAlign: 'center',
    fontSize: 14,
    borderRadius:5,
  },
  centerOnScreen:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  spaceBetween:{
    flexDirection:"row",
    justifyContent:"space-between"
  },
  divisorVerticalBar:{
    alignSelf:"center",
    borderWidth:0.5,
    borderColor:"#9c9595",
    marginHorizontal:8,
    height:14
  },
  detailScreen:{
    marginTop:(Platform.OS=='android')?64:80,
  }
})
