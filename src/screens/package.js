import React, {Component} from 'react';
import { View, Text } from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
// Components
import {MainStyle} from '../styles/mainStyle';
import InLobby from './package/inLobby';
import Sent from './package/sent.js';
import Delivered from './package/delivered.js';
import KeyboardSpacer from 'react-native-keyboard-spacer';
//Language
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Package extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <View style={MainStyle.tabTop}>
        <ScrollableTabView
          style={{flex: 1, alignItems: 'stretch'}}
          renderTabBar={() =>
            <DefaultTabBar
              backgroundColor='#1B8F9F'
              inactiveTextColor='#38BCD2'
              activeTextColor='#B8E986'
              tabStyle={{ paddingBottom: 0}}
              textStyle={{fontWeight: 'normal', fontSize: 16}}
              underlineStyle={{backgroundColor: '#B8E986', }}
              style={{height: 40,}}
            />
          }
        >
          <InLobby tabLabel={I18n.t('in_lobby')}/>
          <Sent tabLabel={I18n.t('sent')}/>
          <Delivered tabLabel={I18n.t('delivered')}/>
        </ScrollableTabView>
        <KeyboardSpacer/>
      </View>
    )
  }
}
