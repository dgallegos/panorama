import React, {Component} from 'react';
import { View, Text } from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import OpenOrders from './request/open_orders'
import CompleteOrders from './request/complete'
import NewOrder from './request/new_order'
import {MainStyle} from '../styles/mainStyle';

//Languaje
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Request extends Component{
  render(){
    return(
      <View style={[MainStyle.tabTop, {backgroundColor: '#EFEFEF'}]}>
        <ScrollableTabView
          style={{flex: 1, alignItems: 'stretch'}}
          renderTabBar={() =>
            <DefaultTabBar
              backgroundColor='#1B8F9F'
              inactiveTextColor='#38BCD2'
              activeTextColor='#FFFFFF'
              tabStyle={{ paddingBottom: 0}}
              textStyle={{fontWeight: 'normal', fontSize: 15}}
              underlineStyle={{backgroundColor: 'white', }}
              style={{height: 40}}
            />
          }
          locked
        >
          <OpenOrders tabLabel={I18n.t('open_orders')}/>
          <CompleteOrders tabLabel={I18n.t('complete')}/>
        </ScrollableTabView>
      </View>
    )
  }
}
