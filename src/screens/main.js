import React, { Component } from 'react'
import {Text,View,StyleSheet,ListView,Image,RefreshControl,TouchableOpacity,TouchableHighlight,Platform,StatusBar} from 'react-native'
import _ from "lodash"
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n';
import { Bars } from 'react-native-loader';
// Components
import {MainStyle} from '../styles/mainStyle';
import * as Utils from "../components/utils.js"
import * as Apis from "../const/Apis.js";
//Language
import Lang from '../const/lang.js';
I18n.fallbacks = true
I18n.translations= Lang;
// Redux
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


/**
 * Main Component where events and news are rendered
 */
export default class Main extends Component{
  constructor(){
    super();
    this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      ds:[],
      isRefreshing: false,
      loading: true
    }
  }

  componentDidMount(){
    if(Platform.OS == 'ios') StatusBar.setBarStyle('light-content', true);
    this.getData()
  }

  /**
   * Fetches news and events then merges it onto a single array
   */
  getData(){
    return Utils.fetchApi(`${Apis.NEWS}`)
    .then( data =>{
      
      let dataShow=[];
      const news = data.data.news || [];
      const events = data.data.events || [];
      
      dataShow.push(...news);
      dataShow.push(...events);
      dataShow = _.orderBy(dataShow,['created_at'],["desc"])
      this.setState({
          ds: dataShow,
          loading: false
      })
      // return
    })
    .catch(err => {
      this.setState({
          ds: []
      });
    })
  }

  /**
   * Fetches data on refresh
   */
  onRefresh(){
    this.setState({
      isRefreshing: true
    });

    this.getData()
    .then(()=>{
      this.setState({
        isRefreshing: false
      })
    })
  }

  /**
   * get next dates
   * @param array dates 
   */
  nextDates(dates){
    let sortedDates = _.sortBy(dates, ['date_start',"time_start"])
    let nextDates = [];

    sortedDates.forEach(date=>{
      let d = date.date_start;
      let now = moment(new Date()).format("YYYY-MM-DD HH:mm:mm");
      if(now <= d){
        nextDates.push(date);
      }
    });

    return nextDates;
  }

  renderLoading(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }

  /**
   * Renders the row to show the event or news
   * @param  object data event or news data]
   */
  renderRow(data){
    let language = (I18n.locale=='es') ? "es" : "en";
    let image;
    let nextDate = this.nextDates(data.dates);
    if(data.images.hasOwnProperty(0))
      image = {uri: `${Apis.crm}/${data.images[0].path}`};
    else
      image = require('../assets/Event-1-1.png');
    
    return (
      <TouchableOpacity
        style={styles.eventListView}
        onPress={() => Actions.event({event: data})}>
        <View>
          <Image source={image} style={styles.eventImage}>
            <View style={styles.eventTextContainer}>
              <Text style={styles.eventListViewText}>{data.titles[language].toUpperCase()}</Text>
              <View style={styles.row}>
                <View style={{marginRight: 10}}>
                  <Text style={styles.eventListViewDetailText}>{nextDate.hasOwnProperty(0) ? moment(nextDate[0].date_start).calendar() : ''}</Text>
                </View>
                <View>
                  <Text style={[styles.eventListViewDetailText, {textDecorationLine: 'underline', marginRight: 10}]}>read more</Text>
                </View>
              </View>
            </View>
          </Image>
        </View>
      </TouchableOpacity>
    )
  }

  render(){
    if(this.state.loading){
      return this.renderLoading();
    }

    if(this.state.ds.length > 0 ){
      return(
        <View style={[styles.container,MainStyle.tabTop]}>
            {/*<StatusBar hidden={true} />*/}
          <View style={{backgroundColor: '#F6F6F6'}}>
            <Text style={{textAlign: 'center', fontSize: 12, color: '#1699AD', padding: 7}}>
              {I18n.t('this_week_panorama')}
            </Text>
          </View>
          <ListView
            style={{flex: 1}}
            dataSource={this.ds.cloneWithRows(this.state.ds)}
            renderRow={this.renderRow.bind(this)}
            enableEmptySections={true}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
                tintColor="#1DEAFF"
                title={I18n.t('loading')}
                titleColor="#1DEAFF"
                colors={['#ff0000', '#00ff00', '#0000ff']}
                progressBackgroundColor="#ffff00"
              />
            }
          />
        </View>
      )
    }else{
      return(
        <View style={{flexGrow:1,alignItems:"center",justifyContent:"center"}}>
          <TouchableOpacity onPress={()=>this.onRefresh()}>
              <Text  style={{color: '#089CC5', fontSize: 16}}>{I18n.t("no_news")}</Text>
          </TouchableOpacity>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  container:{
    marginBottom: 25
  },
  eventListView:{
    // flex: 1, 
    height: 120, 
    flexGrow: 1,
    marginBottom: 15
  },
  eventImage:{
    // flex:1,
    height: 120, 
    flexGrow: 1,
    justifyContent:'flex-end'
  },
  eventTextContainer:{
    padding: 15, 
    backgroundColor: 'transparent'
  },
  eventListViewText:{
    color: 'white', 
    fontSize: 12
  },
  row: {
    flexDirection: 'row'
  },
  eventListViewDetailText:{
    color: 'white', 
    fontSize: 10
  }
})
