import React, {Component} from 'react';
import {Text, View, Keyboard, TouchableOpacity, Platform} from 'react-native';
import {GiftedChat, Bubble, MessageText, Composer} from 'react-native-gifted-chat';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n';
import {ENTITYCRM} from '../const/Apis.js';
//Components
import {MainStyle} from '../styles/mainStyle';
import * as chatActions from '../actions/chat_actions.js';
import * as taskActions from '../actions/task_actions.js';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// Others
import * as utils from '../components/utils.js';
//Language
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;
import firebase from '../firebase.js';

console.disableYellowBox = true;

class Chat extends Component{
  constructor(){
    super();
    // initialize firebase and get chat reference
    db = firebase.database().ref('chat');
    // firebase references
    this._messageRef    = db.child('room-messages');
    this._roomRef       = db.child('room-metadata');
    this._roomUsersRef  = db.child('room-users');
    this._users         = db.child('user');
    // bind methods
    this.onSend = this.onSend.bind(this);
    this.renderBubble = this.renderBubble.bind(this)
    this.renderCustomView = this.renderCustomView.bind(this)
    this.renderMessageText = this.renderMessageText.bind(this)
    this.renderComposer = this.renderComposer.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

    this.state = {
      loading: true,
      error: false,
      keyboardUp: false
    }
  }

  componentWillMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  _keyboardDidShow () {
    this.setState({keyboardUp: true})
  }

  _keyboardDidHide () {
    this.setState({keyboardUp: false})
  }

  componentDidMount(){
    const self = this;
    //auth observer
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        self._userId = firebase.auth().currentUser.uid;
       } //else {
      // //   self.setState({loading: false, error: true});
      // // }
    });

    utils.userInfo().then(resident => {
      self.user = resident;
      //login
      self._signin(resident)
      .then(_ => {
        self._getRoom(resident)
      })
      .catch(err => {
        console.log(err);
        if(err.code == 'auth/user-not-found'){
          self._login(resident)
          .then( _ => {
            self._createRoom(resident);
          });
        }     
      });
      
    });
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    this.props.actions.flushChat();
  }

  _login(resident){
    return firebase.auth()
        .createUserWithEmailAndPassword(resident.email, 'Panorama17!')
        .catch(error => this.setState({error: true}))
  }

  _signin(resident){
    return firebase.auth().signInWithEmailAndPassword(resident.email, 'Panorama17!')
  } 

  _persistToEntity(resident){
    const body = {
      chat_url: `${resident.email.split('@')[0]}_${resident.user_id}`,
      user_chat: resident.email
    };

    utils.fetchApi(`${ENTITYCRM}/${resident.resident_id}`,{},body,'PUT')
    .then(data =>{
      console.log(data);
    })
    .catch(err => console.log(err))
  }

  _createRoom(resident){
    let self = this;
    const roomName = `${resident.email.split('@')[0]}_${resident.user_id}`;
    let newRoomRef = this._roomRef.push();

    let newRoom = {
      id: newRoomRef.key,
      name: roomName,
      type: 'public',
      createdByUserId: this._userId,
      createdAt: firebase.database.ServerValue.TIMESTAMP
    }

    newRoomRef.set(newRoom, function(error) {
      if (!error) {
        self._roomId = newRoomRef.key;
        self._fetchMessages(self._roomId);
        self.setState({loading: false})
        //Save to db
        self._persistToEntity(resident);
      }else{
        // console.log(error)
        self.setState({error: true})
      }
    });
  }

  _getRoom(resident){
    let self = this;
    const roomName = `${resident.email.split('@')[0]}_${resident.user_id}`;
    self._roomRef.once('value', function(snapshot){
      const rooms = snapshot.val();
      const room = rooms ? Object.keys(rooms).filter(room => rooms[room].name == roomName): [];
      
      if(room.length == 0){
        self._createRoom(resident);
        return;
      }
      
      self._roomId = room[0];
      self._fetchMessages(self._roomId)
      self.setState({loading: false})
    })
  }

  _fetchMessages(roomId){
    const {actions, chat} = this.props;
    const self = this;
    
    //add listener when a message is added
    const ref = self._messageRef.child(roomId).orderByChild('timestamp');
    
    ref.on('value', function(snapshot) {

      const data = snapshot.val();
      if(data){
        let messages = Object.keys(data).map( key => ({
          _id: key,
          text: data[key].message,
          user:{
            _id: data[key].userId,
            name: data[key].name,
          },
          createdAt: data[key].timestamp
        }));
        
        if(chat.messages.length == messages.length){
          return;
        }
        let allMessages = GiftedChat.append(chat.messages, messages)
      
        allMessages.sort((a,b) => b.createdAt - a.createdAt)
        actions.addMessage(allMessages);
      }

      self.setState({loading: false});
    });
  }

  
  onSend(messages){
    const message = messages[0]
    
    this._messageRef.child(this._roomId)
    .push({
      userId: firebase.auth().currentUser.uid,
      message: message.text,
      timestamp: firebase.database.ServerValue.TIMESTAMP,
      name: this.user.full_name
    });
  }

  renderBubble(props) {
    // console.log(props)
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#fff',
            borderRadius : 6,
          },
          right: {
            backgroundColor: "rgba(24, 174, 195, 0.8)",
            borderRadius: 6
          }
        }}
      />
    );
  }

  renderCustomView(props){
    // console.log(props)
    if(props.user._id != props.currentMessage.user._id)
      return (
        <View style={{borderTopRightRadius: 6}}>
          <Text style={{fontSize: 10, marginHorizontal: 10, color: '#949A94'}}>{props.currentMessage.user.name}</Text>
        </View>
      )
  }

  renderMessageText(props){
    return(
      <MessageText
        {...props}
        textStyle={{
          left: {
            fontSize: 14
          },
          right: {
            fontSize: 14
          }
        }}
      />
    )
  }

  renderComposer(props){
    return(
      <Composer
        {...props}
        textInputStyle={{
          fontSize: 14,
        }}
      />
    )
  }

  render(){
    const {chat, actions} = this.props;
    if(this.state.loading){
      return (
        <View style={[MainStyle.tabTop, {marginBottom: 0}]}>
          <Spinner visible={this.state.loading} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 12}} overlayColor='rgba(0,0,0,0)'/>
        </View>
      )
    }
    return(
      <View style={[MainStyle.tabTop, {backgroundColor: '#fff', marginBottom: 0, paddingBottom: 0}]}>
        
          {
            !this.state.error ?
              <View style={{flexGrow:1, backgroundColor: '#EFEFEF', marginBottom: 50}}>
                <View style={{flexGrow:1, position: 'relative'}}>
                  <GiftedChat
                    messages={chat.messages}
                    onSend={this.onSend}
                    user={{
                      _id: firebase.auth().currentUser.uid
                  }}
                    renderBubble={this.renderBubble}
                    renderCustomView={this.renderCustomView}
                    renderMessageText={this.renderMessageText}
                    renderComposer={this.renderComposer}
                    bottomOffset={Platform.OS == 'android' ? 0 : 100}
                    onLongPress={(ctx, message) => ctx.actionSheet().showActionSheetWithOptions({
                      options: ['Create Task']
                    }, (buttonIndex) => {
                      switch(buttonIndex){
                        case 0: 
                          actions.updateTaskForm('task_description', message)
                          Actions.task_form_chat();
                      }
                    })}
                  /> 
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 50}}>
                  <View style={{flexGrow: 1, alignItems: 'stretch', backgroundColor: '#8A8F8A', margin: 4, padding: 0}}>
                    <TouchableOpacity
                      style={{flexGrow: 1, margin: 0, padding: 0, justifyContent: 'center'}}
                      onPress={() => Actions.visitors_form_chat()}
                    >
                      <Text style={{color: '#fff', textAlign: 'center'}}>{I18n.t('authorize_visit')} > </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexGrow: 1, alignItems: 'stretch', backgroundColor: '#8A8F8A', margin: 4}}>
                    <TouchableOpacity
                      style={{flexGrow: 1, margin: 0, padding: 0, justifyContent: 'center'}}
                      onPress={() => Actions.task_form_chat()}
                    >
                      <Text style={{color: '#fff', textAlign: 'center'}}>{I18n.t('request_task')} > </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View> : 
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>Ha habido un problema conectandose al chat</Text>
              </View>
          }
      </View>
    )
  }
}

export default connect(
  state => ({...state}),
  dispatch => ({actions: bindActionCreators({...chatActions, ...taskActions}, dispatch)})
)(Chat);