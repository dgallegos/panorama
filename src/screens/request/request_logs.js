import React, {Component} from 'react';
import { View, Text, ListView, RefreshControl, Alert, TouchableHighlight, StyleSheet } from 'react-native';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n';
import { Bars } from 'react-native-loader';
import moment from 'moment';
import {MainStyle} from '../../styles/mainStyle';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as taskActions from '../../actions/task_actions.js';
import * as asyncActions from '../../actions/async_actions.js';
import {AfterInteractions} from 'react-native-interactions';
import Spinner from 'react-native-loading-spinner-overlay';
// Lang
import Lang from '../../const/lang';
I18n.fallbacks = true
I18n.translations= Lang;

class TaskLogs extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
      task: {}
    }
  }

  componentDidMount(){
    const {tasks, actions} = this.props;
    actions.loadTaskLogs(tasks.taskLog.id)
      .then(logs =>{
        this.setState({
          ds: this.ds.cloneWithRows(logs)
        });
      })
  }

  renderRow(log){
    return(
      <TouchableHighlight
        underlayColor='rgba(255,255,255,1)'
        style={styles.rowFront}
      >
        <View style={{marginVertical: 5, marginTop: 10}}>
          <View style={{flexDirection: 'row', flex: 1}}>
            <View style={{flex: 1, paddingLeft: 10}}>
              <Text style={{fontSize: 11, marginRight: 20, color: '#a6a9aa', fontWeight: '300'}}>{moment(log.created_at).format('DD/MM/YY h:mm a')}</Text>
            </View>
            <View style={{flex: 1}}>
              <Text style={{fontSize: 11, textAlign: 'right',marginRight: 20, color: '#a6a9aa', fontWeight: '300'}}>Posted by {log.user ? log.user.profiles[0].full_name: ''}</Text>
            </View>
          </View>
          <View style={{marginTop: 5}}>
            <Text style={{paddingLeft: 10, fontSize: 14, marginRight: 20, color: '#6E716E', fontWeight: '300', paddingBottom: 5}}>{log.action}</Text>
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  renderLoading(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }


  render(){
    const { ds} = this.state;
    const {tasks} = this.props;
    const {taskLog: task} = tasks;

    return(
      <AfterInteractions placeholder={<Spinner visible={true} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 12}} overlayColor='rgba(0,0,0,0)'/>}>
        <View style={[MainStyle.tabTop,{backgroundColor: '#EFEFEF'}]}>
          <View style={[styles.rowFront, {marginBottom: 15 }]}>
            <View style={{flexDirection: 'row', marginHorizontal: 20, marginBottom: 5, marginTop: 10}}>
              <View style={{flexGrow: 1}}>
                <Text style={{fontSize: 11, color: '#a6a9aa', fontWeight: 'bold'}}>Task-{task.id} | {task.discipline.discipline}</Text>
              </View>
              <View>
                <Text style={{fontSize: 11, color: '#a6a9aa'}}>{moment(task.created_at).format('DD/MM/YYYY h:m A')}</Text>
              </View>
            </View>
            <View style={{flexGrow: 1, marginVertical: 2, flexDirection: 'row'}}>
              <View style={{flexGrow: 1, marginLeft: 20, marginRight: 5}}>
                <Text style={{fontSize: 16, fontWeight: '100'}}>{task.task_description}</Text>
              </View>
              <View style={{marginRight: 20}}>
                <View style={{backgroundColor: '#74B62A', borderRadius: 4, borderWidth: 1, borderColor: '#74B62A'}}>
                  <Text style={{fontSize: 13, color: '#fff', padding: 3, }}>{task.status.name}</Text>
                </View>
              </View>
            </View>
            <Text style={{fontSize: 12, textAlign: 'right', marginRight: 20, marginTop: 10, color: '#a6a9aa', fontWeight: '300'}}>
              Previsita: {moment(task.due_on).format('DD/MM/YY h:mm a')}
            </Text>
          </View>
          <ListView
            style={{flex: 1}}
            dataSource={ds}
            renderRow={log => this.renderRow(log)}
            enableEmptySections={true}
          />
          { 
            ds.rowIdentities[0].length == 0 &&
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text style={{color: '#089CC5', fontWeight: '600', fontSize: 16}}>{I18n.t('no_history')}</Text>
            </View>
          }
        </View>
      </AfterInteractions>
    )
  }
}

const styles = StyleSheet.create({
   rowFront: {
    // alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
});


export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...taskActions,...asyncActions},dispatch)})
)(TaskLogs);
