import React from 'react';
import {TouchableHighlight, Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

const ListItem = ({name, scheduled_at, approved_by, approved_at, status, assigned_to}) => (
  <TouchableHighlight
    onPress={()=>console.log('onPress')}
    underlayColor='rgba(255,255,255,1)'
    style={styles.rowFront} 
  >
    <View style={{flex: 1, marginVertical: 12, flexDirection: 'row'}}>
      <View style={{flex: 1, marginHorizontal: 45}}>
        <Text style={{fontSize: 14}}>{name}</Text>
        <Text style={{fontSize: 13}}>Schedule {scheduled_at}</Text>
        <Text style={{fontSize: 13}}>Approved by: {approved_by}</Text>
        <Text style={{color: '#74B62A', fontSize: 13}}>Assigned to: {assigned_to}</Text>
      </View>
    </View>
  </TouchableHighlight>
);

const styles = StyleSheet.create({
   rowFront: {
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
});

export default ListItem;