import React, {Component} from 'react';
import {Text, View, TouchableOpacity, TextInput, Alert} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
import Calendar from 'react-native-calendar';
import {ScrollCardPicker} from '../../components/scroll_time_picker_card.js';
import moment from 'moment';
import Button from '../../components/button.js';
import {Actions} from 'react-native-router-flux';
import {AfterInteractions} from 'react-native-interactions';
import Spinner from 'react-native-loading-spinner-overlay';
//Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as taskActions from '../../actions/task_actions.js';
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const customDayHeadings = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const customMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May',
  'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const calendarStyles = {
  calendarHeading:{
    borderBottomWidth: 0,
    borderTopWidth: 0
  },
  calendarContainer:{
    backgroundColor: '#fff'
  }
}

class TaskCalendar extends Component{
  componentDidMount(){
    console.log(this.props)
  }

  validation(){
    const {date, hour} = this.props.tasks.form;
    if(!hour){
      Alert.alert(I18n.t('validation'),I18n.t('select_hour'), [{
        text: I18n.t('accept')
      }]);
      return;
    }

    if(moment(moment(date).startOf('day').add(hour, 'h')).isBefore()){
      Alert.alert(I18n.t('validation'),I18n.t('select_future_date'), [{
        text: I18n.t('accept')
      }]);
      return;
    }

    if(moment(date).format('ddd') == 'sab.' || 
        moment(date).format('ddd') == 'dom.' || 
        moment(date).format('ddd') == 'Sun' || 
        moment(date).format('ddd') == 'Sat'){
      Alert.alert(I18n.t('validation'),I18n.t('no_work_weekends'), [{
        text: I18n.t('accept')
      }]);
      return;
    }

    Actions.pop();
  }

  handleDateChange(date){
    const {actions, tasks} = this.props;
    actions.updateTaskForm('date', moment(date).format('YYYY-MM-DD'));
    actions.loadDisciplineDatesCalendar(tasks.form.discipline_id,moment(date).format('YYYY-MM-DD'));
  }

  getHours(hoursNotAvailable, date){
    let hours = Array.from({length: 9}, (v,i) => 8 + i).reduce((res, val) =>{
      res[val] = 1;
      return res;
    },{});
    
    if(moment().startOf('day').isSame(moment(date))){
      Object.keys(hours).forEach(hour => {
        if(moment().startOf('day').add(hour, 'h').isBefore(moment())){
          hours[hour] = 0;
        }
      });
    }

    hoursNotAvailable.map(h => hours[parseInt(h)] = 0);

    return Object.keys(hours).filter(hour => hours[hour] == 1);
  }

  render(){
    const {actions, tasks, routes} = this.props;
    const hours = this.getHours(tasks.disciplineDatesCalendar[tasks.form.date] || [], tasks.form.date);
    return(
      <AfterInteractions placeholder={<Spinner visible={true} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 12}} overlayColor='rgba(0,0,0,0)'/>}>
        <View style={[MainStyle.tabTop,{backgroundColor: '#f5f5f5'}]}>
          <Text style={{margin: 20, textAlign:'center', fontSize: 16, color: '#838387'}}>Disponibilidad</Text>
          <Calendar
            currentMonth={moment()}
            scrollEnabled
            showControls
            prevButtonText={I18n.t('prev')}
            nextButtonText={I18n.t('next')}
            weekStart={0}
            customStyle={calendarStyles}
            selectedDate={moment(tasks.form.date)}
            onDateSelect={date => this.handleDateChange(date)}

          />
          {/*Hours*/}
          {
            hours.length > 0 ? 
              <View style={{marginLeft: 5, marginBottom: 20}}>
                <ScrollCardPicker
                  items={hours}
                  pressed={tasks.form.hour}
                  onPress={hour =>  actions.updateTaskForm('hour', hour)}
                  display={ i => `${i > 12 ? i % 12 : i % 13 } ${i >= 12 ? 'PM' : 'AM'}` }
                  scrollViewStyles={{marginTop: 10}}
                  selectedBackgroundColor='#A2ABAE'
                />
              </View> :
              <View style={{marginVertical: 20}}>
                <Text style={{textAlign: 'center', fontWeight: '100', fontSize: 12, color: '#989E95', fontStyle: 'italic'}}>{I18n.t('no_available_hours')}</Text>
              </View>
          }
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
            <Button
              onPress={() => this.validation()}
              style={{backgroundColor: '#1EA9C7', padding: 10, marginHorizontal: 10, alignItems: 'center', width: 130}}
            >
              <Text style={{color: '#fff'}}>{I18n.t('go_back')}</Text>
            </Button>
            <Button
              onPress={() => {
                routes.scene.onSubmit();
                Actions.pop();
              }}
              style={{backgroundColor: '#0CAC63', padding: 10, marginHorizontal: 10, alignItems: 'center', width: 130}}
            >
              <Text style={{color: '#fff'}}>{I18n.t('save')}</Text>
            </Button>
          </View>
        </View>
      </AfterInteractions>
    )
  }
}

export default connect(
  (state, ownProps) => ({tasks: state.tasks, routes: state.routes}), 
  dispatch => ({
    actions: bindActionCreators({...taskActions}, dispatch)
  })
)(TaskCalendar);

