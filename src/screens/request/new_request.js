import React, { Component } from 'react';
import {View, Text, TouchableOpacity, Alert, ScrollView, Image, Platform  } from 'react-native';
import { MainStyle } from '../../styles/mainStyle.js';
import update from 'react-addons-update';
import DatePicker from '../../components/date_picker.js';
import TextInput from '../../components/text_input.js';
import Select from '../../components/picker.js';
import Button from '../../components/button.js';
import Icon from 'react-native-vector-icons/FontAwesome';
import {v4} from 'react-native-uuid';
import moment from 'moment';
import { REQUEST } from '../../const/Apis.js';
import { fetchApi, userInfo } from '../../components/utils.js';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';


const options = {
  title: 'Seleccionar Imagen',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  },
  cameraType: 'front',
  takePhotoButtonTitle: 'Tomar Foto',
  cancelButtonTitle: 'Cancelar',
  chooseFromLibraryButtonTitle: 'Seleccionar Imagen'
};

export default class NewRequestModal extends Component{
  constructor(props){
    super(props)
    this.state={
      form:{
        comments: '',
        contact_id: '',
        status_id: 1,
        due_on: moment(),
        images: [],
        priority_id: 1,
        task_description: ''
      },
      disabled: false
    }
  }

  //Header methods
  onChange(value, field){
    this.setState({
      form: update(this.state.form,{
        $merge:{
          [value]: field
        }
      })
    });
  }
  
  renderImages(){
    let { images } = this.state.form;
    return (
      <View style={{flexDirection:'column', alignItems: 'center'}}>
        <ScrollView
          automaticallyAdjustContentInsets={false}
          horizontal={true}
          style={{width:300,margin:5}}
        >
          {
            images.map(img =>
              <TouchableOpacity key={img.id} style={{margin: 3}} onPress={() => this.imageStatus(img.id)} >
                <Image style={{ height:50, width:50, marginRight: 5}} source={{uri: img.img}} />
              </TouchableOpacity>
            )
          }
        </ScrollView>
        <TouchableOpacity
          style={{alignItems:"center",justifyContent:"center"}}
          onPress={()=>this.select()}
        >
          <Icon style={{textAlign:'center'}} size={30} name="camera"/>
        </TouchableOpacity>
      </View>
    )
  } 

  imageStatus(id){
    const imgOptions = {
      ...options,
      customButtons: [{
        name:'delete' ,
        title:'Eliminar'
      }]
    };

    this.pickImage(imgOptions, 'edit', id);
  }

  select(){
    if(this.state.form.images.length == 2){
      Alert.alert('Solo puedes enviar dos imagenes', null ,[{
        text: 'Aceptar'
      }]);
      return;
    }
    this.pickImage(options, 'add');
  }

  pickImage(options, action, id = null){
    let { images } = this.state.form;
    console.log(ImagePicker.showImagePicker)
    ImagePicker.showImagePicker(options, response => {

      if(response.customButton == 'delete'){
        let index = images.findIndex(x=> x.id==id)
        this.setState({ 
          form: update(this.state.form,{
            images: {
              $splice: [[1,index]]
            }
          }) 
        });
      }else{
        if(response.error){
          console.log(response.error,null);
          return;
        }

        if (!response.didCancel) {
          const source = {img: 'data:image/png;base64,' + response.data, isStatic: true};

          if(action === 'add'){
            source.id = v4();
            images.push(source);
          }

          if(action === 'edit'){
            let index = images.findIndex(x=> x.id==id)
            images[index].img = source.img;
          }

          this.setState({ 
            form: update(this.state.form,{
              $merge: {
                images
              } 
            }) 
          });
        }
      }
    });
  }

  onSubmit(){
    this.setState({disabled: true})

    let body = {
      ...this.state.form
    }

    userInfo().then(userData=>{
      body.contact_id = 3
      fetchApi(REQUEST,{},body,'POST')
      .then(data=> {
        this.setState({
          disabled: false
        });
        if(data.valid){
          Alert.alert('Info','Se ha creado su solicitud', [{
            text: 'Aceptar', onPress: () => Actions.pop()
          }])
          return;
        }else{
          Alert.alert('Info','Ha habido un error al crear su solicitud', null ,[{
            text: 'Aceptar'
          }]);
        }
      })
    });
  }

  render(){
    return(
      <View style={[MainStyle.tabTop,{backgroundColor: '#EFEFEF'}]}>
        <Text style={{fontSize: 18, fontWeight: '400', textAlign: 'center', marginTop: 20}}>Que deseas reportar</Text>
        <Text style={{textAlign: 'center'}}>Agrega una descripcion de la tarea a solicitar</Text>
        <View style={{backgroundColor: '#fff', marginVertical: 40, flex: 1}}>
          <TextInput
            value={this.state.form.task_description}
            onChange={task_description => this.onChange('task_description',task_description)}
            placeholder="Task Description"
            label='Task Description'
            inputStyles={{textAlign: 'left'}}
            inputContainerStyles={{padding: 5}}
            containerStyles={{borderBottomWidth: 1, borderBottomColor: 'rgb(197,195,201)'}}
          />
          <DatePicker
            label='Suggested Date'
            date={this.state.form.due_on}
            mode="datetime"
            placeholder="Select Date"
            format="DD/MM/YY h:mm a"
            customStyles={{
              dateInput: {
                margin: 10,
                borderWidth: 0
              },
              dateText:{
                paddingLeft: 15
              }
            }}
            onChange={due_on => this.onChange('due_on', due_on)}
          />
          <TextInput
            value={this.state.form.comments}
            onChange={comments => this.onChange('comments',comments)}
            placeholder="Comments"
            label='Comments'
            numberOfLines={2}
            inputStyles={{textAlign: 'left'}}
            inputContainerStyles={{padding: 5}}
            containerStyles={{borderBottomWidth: 1, borderBottomColor: 'rgb(197,195,201)'}}
            multiline
          />
        </View>
        {this.renderImages()}
        {/*Button*/}
          <Button 
            style={{backgroundColor: '#18AEC3', height: 45, marginTop: 20, justifyContent: 'center'}}
            onPress={this.onSubmit.bind(this)}
            disabled={this.state.disabled}
          >
            <Text style={{ textAlign:'center', color: 'white', marginVertical: 10, marginHorizontal: 60, fontWeight: 'bold'}}>Create</Text>
          </Button>
      </View>
    )
  }
}