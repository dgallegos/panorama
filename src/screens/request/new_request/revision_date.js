import React from 'react';
import {View, Text} from 'react-native';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';
import DatePicker from '../../../components/date_picker.js';
import moment from 'moment';

class RevisionDate extends React.Component{
  render(){
    const {actions, form, nextStep} = this.props
    return (
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1}}>
        <ModalButtonsTop 
          oneLabel='Back' 
          twoLabel='Next' 
          oneAction={actions.decrementStep}
          twoAction={nextStep}
          middleText='Select entry date'
        />
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <DatePicker
            date={form.due_on.format('DD/MM/YY hh:mm a')}
            mode="datetime"
            placeholder="Select Date"
            format="DD/MM/YY hh:mm a"
            label='Entry Date'
            customStyles={{
              dateInput: {
                margin: 10,
                borderWidth: 0,
              },
              dateText:{
                paddingLeft: 50,
              }
            }}
            onChange={due_on => 
              actions.updateTaskForm('due_on', moment(due_on,"DD/MM/YY hh:mm a"))
            }
          />
        </View>
      </View>
    )
  } 
};

export default RevisionDate;