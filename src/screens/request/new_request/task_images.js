import React from 'react';
import {Text, View} from 'react-native';
// Components
import TextInput from '../../../components/text_input.js'

export default class TaskImages extends React.Component{
  render(){
    const {task_description, cancel} = this.props
    return(
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 17}}>
            New Request
          </Text>
          <Text style={{fontSize: 13, marginTop: 8}}>
            Request a new task or repair
          </Text>
          <TextInput
            value={task_description}
            onChange={onChange}
            placeholder="Task Description"
            inputStyles={{textAlign: 'left', backgroundColor: 'white'}}
            containerStyles={{borderWidth: 1, borderColor: '#d3d3d3', marginHorizontal: 15, marginVertical: 10}}
          />
        </View>
        <ModalButtons 
          oneLabel='Cancel' 
          twoLabel='Confirm' 
          oneAction={cancel}
          twoAction={next}
        />
      </View>
    )
  }
}