import React from 'react';
import {Text, View, ListView, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux';
// Components
import {MainStyle} from '../../../styles/mainStyle.js';
import Icon from "react-native-vector-icons/EvilIcons"
import {Bars} from 'react-native-loader';
import { fetchApi, userInfo } from '../../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../../const/Apis.js';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as taskActions from '../../../actions/task_actions';
import * as modalActions from '../../../actions/modal_actions';

class Disciplines extends React.Component{
  constructor(props){
    super(props)
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows(props.disciplines)
    }
  }

  componentWillReceiveProps(nextProps){
    const {taskDisciplines} = this.props;
    if(nextProps.taskDisciplines != taskDisciplines)
      this.setState({
        ds: this.ds.cloneWithRows(nextProps.disciplines)
      });
  }
  /**
   * Toggles check by adding discipline to taskDisciplines state (redux)
   * @param  object discipline contains id and name
   */
  toggleSelection(discipline){
    const {taskDisciplines, actions} = this.props
    if(taskDisciplines.filter(d => d.id == discipline.id).length > 0){
      actions.removeTaskDiscipline(discipline)
    }else{
      actions.addTaskDiscipline(discipline)
    }
  }

  /**
   * Helper to toggle check icon
   * @param  object discipline contains id and name
   */
  selected(discipline){
    const { taskDisciplines } = this.props
    if(taskDisciplines.filter(d => d.id == discipline.id).length > 0)
      return true;
    return false
  }

  render(){
    const {ds} = this.state;
    const {disciplines} = this.props;
    return (
      <View style={{flex: 1, marginTop: 80}}>
        { ds.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              renderRow={ discipline =>
                <Row 
                  discipline={discipline} 
                  toggleSelection={this.toggleSelection.bind(this)}
                  selected={this.selected.bind(this)}
                />
              }
              dataSource={ds}
              enableEmptySections
            />
          </View>:
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }
      </View>
    )
  }
}

const Row = ({discipline, toggleSelection, selected})=>(
  <View style={{flex:1,alignItems:"center",borderBottomWidth:1,borderColor:"#e4e1e1",paddingVertical:10,justifyContent:"space-between",paddingLeft:15,paddingRight:15,backgroundColor:"#fff",flexDirection:"row"}}>
    <Text>{discipline.discipline}</Text>
    <TouchableOpacity onPress={()=> toggleSelection(discipline)}>
      <Icon name={ selected(discipline) ? "check":"minus" } size={35} color={ selected(discipline) ? "green":"gray"}/>
    </TouchableOpacity>
  </View>
)

export default connect(
  state => ({...state.tasks}), 
  dispatch => ({actions: bindActionCreators({...taskActions, ...modalActions},dispatch)})
)(Disciplines);