import React from 'react';
import {View, Text} from 'react-native';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';

class ConfirmRequest extends React.Component{
  render(){
    const {actions, form, onSubmit} = this.props;
    console.log(form)
    return(
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 17}}>
            Task Confirmation
          </Text>
          <Text style={{fontSize: 13, marginTop: 8, textAlign: 'center'}}>
              Task: {form.task_description} will be scheduled on {form.due_on.format('DD/MM/YY hh:mm a')}
          </Text>
        </View>
        <ModalButtons 
          oneLabel='Back' 
          twoLabel='Confirm' 
          oneAction={actions.decrementStep}
          twoAction={() => actions.saveTask(onSubmit)}
        />
      </View>
    )
  }
}


export default ConfirmRequest;