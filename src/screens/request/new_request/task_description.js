import React from 'react';
import {Text, View} from 'react-native';
// Components
import TextInput from '../../../components/text_input.js'
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';

export default class TaskDescription extends React.Component{
  componentDidMount(){
    const {actions} = this.props
    this.props.actions.changeHeightModal(280);
  }

  render(){
    const {actions, form, nextStep} = this.props
    console.log(this.props)
    return(
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 17}}>
            New Request
          </Text>
          <Text style={{fontSize: 13, marginTop: 8}}>
            Request a new task or repair
          </Text>
          <TextInput
            value={form.task_description}
            onChange={ task_description => actions.updateTaskForm('task_description', task_description)}
            placeholder="Task Description"
            inputStyles={{textAlign: 'left', backgroundColor: 'white', paddingHorizontal: 10}}
            containerStyles={{borderWidth: 1, borderColor: '#d3d3d3', marginHorizontal: 15, marginVertical: 10}}
          />
          <TextInput
            value={form.comments}
            onChange={ comments => actions.updateTaskForm('comments', comments)}
            placeholder="Comments"
            inputStyles={{textAlign: 'left', backgroundColor: 'white', paddingHorizontal: 10}}
            containerStyles={{borderWidth: 1, borderColor: '#d3d3d3', marginHorizontal: 15, marginVertical: 10}}
            multi={true}
            numberOfLines={2}
          />
        </View>
        <ModalButtons 
          oneLabel='Cancel' 
          twoLabel='Next' 
          oneAction={actions.decrementStep}
          twoAction={nextStep}
        />
      </View>
    )
  }
}