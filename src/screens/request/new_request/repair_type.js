import React from 'react';
import {Text, View} from 'react-native'
import {Actions} from 'react-native-router-flux';
import Button from '../../../components/button.js';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';
import Icon from 'react-native-vector-icons/Ionicons';


export default class RepairType extends React.Component{
  componentDidMount(){
    const {actions, disciplines} = this.props
    console.log(disciplines.length)
    this.props.actions.changeHeightModal(140 + disciplines.length * 10);
  }

  render(){
    const {actions, disciplines, nextStep} = this.props
    return (
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
          <Text style={{fontWeight: 'bold'}}>Type of repair</Text>
          <View style={{justifyContent: 'center', flexDirection: 'row', alignItems: 'center'}}>
            <Text>Choose discipline</Text>
            <Button 
              style={{marginHorizontal: 10}}
              onPress={()=> {
                actions.toggleModal()
                Actions.request_disciplines()
              }}
            >
              <Icon name='ios-add-circle-outline' color='#18AEC3' size={30}/>
            </Button>
          </View>
          {
            disciplines.map((discipline, idx) => 
              <View key={idx}>
                <Text style={{fontStyle: 'italic'}}>{discipline.discipline}</Text>
              </View>
            )
          }
        </View>
        <ModalButtons 
          oneLabel='Back' 
          twoLabel='Next' 
          oneAction={actions.decrementStep}
          twoAction={nextStep}
        />
      </View>
    )
  }
}

