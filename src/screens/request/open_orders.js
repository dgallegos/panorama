import React, {Component} from 'react';
import { View, Text, ListView, RefreshControl, Alert, TouchableHighlight, StyleSheet } from 'react-native';
import SwipeRowCustom from '../../components/swipe_row_custom.js';
import ListOpenItem from './open_list_items.js';
import { SwipeListView} from 'react-native-swipe-list-view';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n';
import { Bars } from 'react-native-loader';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as taskActions from '../../actions/task_actions.js';
import * as modalActions from '../../actions/modal_actions.js';
import * as asyncActions from '../../actions/async_actions.js';
// Lang
import Lang from '../../const/lang';
I18n.fallbacks = true
I18n.translations= Lang;

class OpenOrders extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.tasks.tasks.length !== this.props.tasks.tasks.length){
      this.setState({
        ds: this.ds.cloneWithRows(nextProps.tasks.tasks.filter(task => task.status_id != 5))
      });
    }
  }

  componentDidMount(){
    this.getRequests(); 
  }

  getRequests(){
    const {actions} = this.props;
    return actions.loadTasks( data => {
      if(data.valid){
        console.log('componentDidMount OpenOrders getRequests')
        actions.populateTasks(data.data || [])
        this.setState({
          ds: this.ds.cloneWithRows(data.data.filter(task => task.status_id != 5) || [])
        });
      }else{
        Alert.alert('Error',I18n.t('error'), [{
          text: I18n.t('accept')
        }]);
      }
    }); 
  }

  onRefresh(){
    const {actions} = this.props;
    actions.setRefreshing(true);
    this.getRequests()
    .then(()=> actions.setRefreshing(false));
  }

  fetchLogs(task){
    const {actions} = this.props;
    
    actions.setCurrentTaskLog(task);

    Actions.task_logs();
  }


  renderRow(task){
    return(
      <TouchableHighlight
        underlayColor='rgba(255,255,255,1)'
        style={styles.rowFront}
        onPress={() => this.fetchLogs(task)} 
      >
        <View style={{marginVertical: 5 }}>
          <View style={{flexDirection: 'row', marginHorizontal: 20, marginBottom: 5}}>
            <View style={{flex: 1}}>
              <Text style={{fontSize: 11, color: '#a6a9aa', fontWeight: 'bold'}}>Task-{task.id} | {task.discipline.discipline}</Text>
            </View>
            <View>
              <Text style={{fontSize: 11, color: '#a6a9aa'}}>{moment(task.created_at).format('DD/MM/YYYY h:m A')}</Text>
            </View>
          </View>
          <View style={{flex: 1, marginVertical: 2, flexDirection: 'row'}}>
            <View style={{flex: 1, marginLeft: 20, marginRight: 5}}>
              <Text style={{fontSize: 16, fontWeight: '100', }}>{task.task_description}</Text>
            </View>
            <View style={{marginRight: 20}}>
              <View style={{backgroundColor: '#74B62A', borderRadius: 4, borderWidth: 1, borderColor: '#74B62A'}}>
                <Text style={{fontSize: 13, color: '#fff', padding: 3, }}>{task.status.name}</Text>
              </View>
            </View>
          </View>
          <Text style={{fontSize: 12, textAlign: 'right', marginRight: 20, marginTop: 10, color: '#a6a9aa', fontWeight: '300'}}>Previsita: {moment(task.due_on).format('DD/MM/YY h:mm a')}</Text>
        </View>
      </TouchableHighlight>
    )
  }

  renderLoading(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }


  render(){
    const { ds } = this.state;
    const {async} = this.props;
    
    if(!async.fetch_success_tasks && !async.fetch_failure_tasks){
      return this.renderLoading();
    }

    return(
      <View style={{marginTop: 15, flex: 1, marginBottom: 50}}>
        <ListView
          style={{flex: 1}}
          dataSource={ds}
          renderRow={task => this.renderRow(task)}
          enableEmptySections={true}
          refreshControl={
            <RefreshControl
              refreshing={async.is_refreshing}
              onRefresh={this.onRefresh.bind(this)}
              tintColor="#1DEAFF"
              title={I18n.t('loading')}
              titleColor="#1DEAFF"
              colors={['#ff0000', '#00ff00', '#0000ff']}
              progressBackgroundColor="#ffff00"
            />
          }
        />
        { 
          ds.rowIdentities[0].length == 0 &&
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{color: '#089CC5', fontWeight: '600', fontSize: 16}}>{I18n.t('no_tasks')}</Text>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
   rowFront: {
    // alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
});


export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...taskActions, ...modalActions, ...asyncActions},dispatch)})
)(OpenOrders);
