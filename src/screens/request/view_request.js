import React, {Component} from 'react';
import {View, StyleSheet, Text,ListView} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang.js';
I18n.fallbacks = true
I18n.translations= Lang;

const list = [
  {name: ()=> I18n.t('inspection')},
  {name: ()=> I18n.t('quote')},
  {name: ()=> I18n.t('work_order')}
]; 

export default class RequestView extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2 });
    this.state= {
      ds: this.ds.cloneWithRows(list)
    }
  }

  renderRow(row){
    return(
      <View style={{padding: 20, borderBottomWidth: 1}}>
        <Text>{row.name()}</Text>
      </View>
    )
  }

  render(){
    return(
      <View style={MainStyle.tabTop}>
        <ListView
          dataSource={this.state.ds}
          renderRow={this.renderRow.bind(this)}
        />
      </View>
    )
  } 
}