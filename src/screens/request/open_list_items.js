'use strict';
import React from 'react';
import {TouchableHighlight, Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import * as Progress from 'react-native-progress';

const ListOpenItem = (props) => (
  <TouchableHighlight
    onPress={()=> Actions.view_request({request: props})}
    underlayColor='rgba(255,255,255,1)'
    style={styles.rowFront} 
  >
    <View style={{flex: 1, marginVertical: 12, flexDirection: 'row'}}>
      <View style={{flex: 1, marginHorizontal: 10}}>
        <Text style={{fontSize: 14}}><Text style={{fontWeight: 'bold'}}>{props.task_description}</Text></Text>
        <Text style={{fontSize: 13}}>Schedule: {props.due_on}</Text>
        <Text style={{textAlign:'center' ,padding:7, color:'#0076FF',fontWeight: 'bold'}}>{props.progress}%</Text>
        <Progress.Bar progress={props.progress/100} width={360} height={1} color={'#0076FF'}/>
        <View style={{flexDirection: 'row'}}>
	        <View style={{marginRight: 32}}>
	        	<Text style={{ fontSize: 8}}>Order</Text>
	      	</View>
	      	<View style={{marginRight: 9}}>
	        	<Text style={ styles.step }>Inspection</Text>
	      	</View>
	      	<View style={{marginRight: 9}}>
	        	<Text style={ styles.step }>Approve quote</Text>
	      	</View>
	      	<View style={{marginRight: 9}}>
	        	<Text style={ styles.step }>Execution</Text>
	      	</View>
	      	<View style={{marginRight: 9}}>
	        	<Text style={ styles.step }>Done</Text>
	      	</View>
	    </View>

	    <View style={{padding:10,flexDirection: 'column'}} >
	    	<Text style={{color:'#9B9B9B',fontSize: 10}}>Approved by: <Text style={{fontWeight: 'bold'}}>{props.approved_by}</Text></Text>
        	<Text style={{color:'#9B9B9B',fontSize: 10}}>Assigned to: <Text style={{fontWeight: 'bold'}}>{props.assigned_to}</Text></Text>
	    </View>
        
      </View>
    </View>
  </TouchableHighlight>
);

const styles = StyleSheet.create({
   rowFront: {
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
  step:{
  	marginRight: 40,
  	fontSize:8,
  	color:'#9B9B9B'
  }
});

export default ListOpenItem;