import React, {Component} from 'react';
import { RefreshControl, View, Text, ListView, TouchableHighlight, StyleSheet } from 'react-native';
import SwipeRowCustom from '../../components/swipe_row_custom.js';
import ListItem from './list_item.js';
import { SwipeListView} from 'react-native-swipe-list-view';
import { Bars } from 'react-native-loader';
import I18n from 'react-native-i18n';
import moment from 'moment';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as taskActions from '../../actions/task_actions.js';
import * as modalActions from '../../actions/modal_actions.js';
import * as asyncActions from '../../actions/async_actions.js';
// Lang
import Lang from '../../const/lang';
I18n.fallbacks = true
I18n.translations= Lang;

class Today extends Component{
  constructor(props){
    super(props);
    let ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      ds: ds.cloneWithRows(this.props.tasks.tasks.filter(task => task.status_id == 5))
    }
  }

  renderRow(task){
    return(
      <TouchableHighlight
        underlayColor='rgba(255,255,255,1)'
        style={styles.rowFront} 
      >
        <View style={{marginVertical: 5 }}>
          <View style={{flexDirection: 'row', marginHorizontal: 20, marginBottom: 5}}>
            <View style={{flex: 1}}>
              <Text style={{fontSize: 11, color: '#a6a9aa', fontWeight: 'bold'}}>Task-{task.id} | {task.discipline.discipline}</Text>
            </View>
            <View>
              <Text style={{fontSize: 11, color: '#a6a9aa'}}>{moment(task.created_at).format('DD/MM/YYYY h:m A')}</Text>
            </View>
          </View>
          <View style={{flex: 1, marginVertical: 2, flexDirection: 'row'}}>
            <View style={{flex: 1, marginLeft: 20, marginRight: 5}}>
              <Text style={{fontSize: 16, fontWeight: '100', }}>{task.task_description}</Text>
            </View>
            <View style={{marginRight: 20, backgroundColor: '#74B62A', borderRadius: 3, borderWidth: 1, borderColor: '#74B62A'}}>
              <Text style={{fontSize: 13, color: '#fff', padding: 2, }}>{task.status.name}</Text>
            </View>
          </View>
          <Text style={{fontSize: 12, textAlign: 'right', marginRight: 20, marginTop: 10, color: '#a6a9aa', fontWeight: '300'}}>Previsita: {moment(task.due_on).format('DD/MM/YY h:mm a')}</Text>
        </View>
      </TouchableHighlight>
    )
  }
  
  renderLoading(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }


  render(){
    const { ds } = this.state;
    const {async} = this.props;
    
    return(
      <View style={{marginTop: 15, flex: 1}}>
        <ListView
        style={{flex: 1}}
        dataSource={ds}
        renderRow={task => this.renderRow(task)}
        enableEmptySections={true}
      />
        {/*
        <SwipeListView
          dataSource={ds}
          enableEmptySections
          renderRow={ open_task => (
            <ListOpenItem
              {...open_task}
            />
          )}
          renderHiddenRow={event => (
            <SwipeRowCustom
              onPressRepeat={() => console.log('cancel')}
            />
          )}
          rightOpenValue={-75}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
              tintColor="#1DEAFF"
              title="Loading more good news"
              titleColor="#1DEAFF"
              colors={['#ff0000', '#00ff00', '#0000ff']}
              progressBackgroundColor="#ffff00"
            />
          }
        />
        */}
        { 
          ds.rowIdentities[0].length == 0 &&
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{color: '#089CC5', fontWeight: '600', fontSize: 16}}>{I18n.t('no_completed_tasks')}</Text>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
   rowFront: {
    // alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
});



export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...taskActions, ...modalActions, ...asyncActions},dispatch)})
)(Today);
