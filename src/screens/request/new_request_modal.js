import React from 'react';
import {Text, View, Alert } from 'react-native';
import { Actions} from 'react-native-router-flux';
import moment from 'moment';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as taskActions from '../../actions/task_actions.js';
import * as modalActions from '../../actions/modal_actions.js';
import {ModalButtons, ModalButtonsTop} from '../../components/modal_buttons.js';
// Components
import TextInput from '../../components/text_input.js'
import TaskDescription from './new_request/task_description.js';
import Disciplines from './new_request/disciplines.js';
import RevisionDate from './new_request/revision_date.js';
import TaskImages from './new_request/task_images.js';
import RepairType from './new_request/repair_type.js';
import ConfirmRequest from './new_request/confirm_request.js';
import DisciplinesPicker from './new_request/discipline_picker.js';

class NewRequestModal extends React.Component{
  componentDidMount(){
    const {actions} = this.props;
    actions.loadDisciplines();
  }
  
  onSubmit(data){
    const {actions} = this.props;
    if(data.valid){
      actions.loadTasks( d => actions.populateTasks(d.data));

      Alert.alert('Info','Task has been scheduled', [{
        text: 'Aceptar', onPress: () => actions.toggleModal('RESET')
      }]);
    }else{
      Alert.alert('Info','There has been an error', [{
        text: 'Aceptar'
      }]);
    }
  }

  validate(){
    const {tasks, actions, modal} = this.props;
    const {form} = tasks;
    switch(modal.step){
      case 1:
        if(form.task_description.length > 8){
          actions.incrementStep();
          return;
        }
        Alert.alert('Explain us better what your problem is', null ,[{
          text: 'Accept'
        }]);
        return false
      case 2: 
        if(tasks.taskDisciplines.length > 0){
          actions.incrementStep();
          return;
        }
        Alert.alert('Please select at least one discipline', null ,[{
          text: 'Accept'
        }]);
        return false;
      case 3:
        if(moment(moment(form.due_on)).isAfter(moment())){
          actions.incrementStep();
          return;
        }
        Alert.alert('Please select a future date', null ,[{
          text: 'Accept'
        }]);
        return false;
      default:
        return true;
    }
  }

  renderStep(){
    const {actions, tasks, modal} = this.props;
    console.log(tasks)
    switch(modal.step){
      case 1:
        return(
          <TaskDescription
            form={tasks.form}
            actions={actions}
            nextStep={this.validate.bind(this)}
          />
        );

      case 2:
        return (
          <RepairType
            actions={actions}
            disciplines={tasks.taskDisciplines}
            nextStep={this.validate.bind(this)}
          />
        )
      
      // case 2:
      //   return (
      //     <DisciplinesPicker
      //       actions={actions}
      //       disciplines={tasks.taskDisciplines}
      //       nextStep={this.validate.bind}
      //     />
      //   );

      case 3: 
        return(
          <RevisionDate
            form={tasks.form}
            actions={actions}
            nextStep={this.validate.bind(this)}
          />
        );

      case 4: 
        return(
          <ConfirmRequest
            form={tasks.form}
            actions={actions}
            onSubmit={this.onSubmit.bind(this)}
          />
        )
    }
  }

  render(){
    return(
      <View style={{flex: 1}}>
        {this.renderStep()}
      </View>
    )
  }
}

export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...taskActions, ...modalActions},dispatch)})
)(NewRequestModal)
