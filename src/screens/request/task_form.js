import React, {Component} from 'react';
import {Text, View, TouchableOpacity, TextInput, Alert, Platform, ScrollView} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
import {Actions} from 'react-native-router-flux';
import Calendar from 'react-native-calendar';
import {ScrollCardPicker} from '../../components/scroll_time_picker_card.js';
import * as taskActions from '../../actions/task_actions';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome'
import Button from '../../components/button.js';
import {AfterInteractions} from 'react-native-interactions';
import Spinner from 'react-native-loading-spinner-overlay';
//Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const dates = [{hour: '3pm', }]
const days = [{month: 'MON', date: '17 Apr'}, {month: 'TUE', date: '18 Apr'}, {month: 'WED', date: '19 Apr'}, {month: 'THU', date: '20 Apr'}, {month: 'FRI', date: '21 Apr'}]

class TaskForm extends Component{
  componentWillReceiveProps(nextProps){   
    const {actions} = this.props; 
    if(this.props.async.fetch_success_request_task != nextProps.async.fetch_success_request_task){
      if(nextProps.async.fetch_success_request_task){
        Alert.alert(I18n.t('success'),I18n.t('saved_success'), [{
          text: I18n.t('accept'),
          onPress: () => {
            actions.flushTaskState();
            actions.loadTasks( d => actions.populateTasks(d.data));
            Actions.pop();
          }
        }]);
        return;
      }
    }

    if(this.props.async.fetch_failure_request_task != nextProps.async.fetch_failure_request_task){
      if(nextProps.async.fetch_failure_request_task){
        Alert.alert(I18n.t('failure'),I18n.t('saved_failure'), [{
          text: I18n.t('accept')
        }]);
      }
      return;
    }
  }

  componentDidMount(){
    const {actions} = this.props;
    actions.loadDisciplines();
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.flushTaskState();
  }

  handleDiscipline(discipline){
    const {actions,tasks} = this.props;
    actions.updateTaskForm('discipline_id',discipline);
    actions.loadDisciplineDates(discipline);
  }

  getHours(hoursNotAvailable, date){
    let hours = Array.from({length: 9}, (v,i) => 8 + i).reduce((res, val) =>{
      res[val] = 1;
      return res;
    },{});
    
    if(moment().startOf('day').isSame(moment(date))){
      Object.keys(hours).forEach(hour => {
        if(moment().startOf('day').add(hour, 'h').isBefore(moment())){
          hours[hour] = 0;
        }
      });
    }

    hoursNotAvailable.map(h => hours[parseInt(h)] = 0);

    return Object.keys(hours).filter(hour => hours[hour] == 1);
  }

  getDates(){
    let start = moment();

    const lengths = {
      'Mon': 5,'Tue': 7,'Wed': 7,'Thu': 7,'Fri': 7,'Sat': 7,'Sun': 6,'sáb.': 7,'dom.': 7,'lun.': 5,'mar.': 7,'mié.': 7,'jue.': 7,'vie.': 7
    }

    return Array.from({length: lengths[moment().format('ddd')]}, (v,i) => i).map(day => {
      let dayOfWeek = moment().add(day,'day').format('ddd');
      if( dayOfWeek == 'Sat' || dayOfWeek == 'sáb.'){
        return undefined;
      }
      if( dayOfWeek == 'Sun' || dayOfWeek == 'dom.'){
        return undefined
      }
      return moment().add(day, 'day').format('YYYY-MM-DD')
    });
  }

  _validateDiscipline(){
    const {tasks, actions} = this.props;
    if(!tasks.form.discipline_id){
      Alert.alert(I18n.t('validation'),I18n.t('select_discipline'), [{
        text: I18n.t('accept')
      }]);
      return false;
    }
    return true;
  }

  _validateDescription(){
    const {tasks, actions} = this.props;
    if(!tasks.form.task_description){
      Alert.alert(I18n.t('validation'),I18n.t('empty_description'), [{
        text: I18n.t('accept')
      }]);
      return false;
    }
    return true;
  }


  handleTimeChange(hour, date){
    const {actions} = this.props;
    if(!this._validateDiscipline()) return;
    actions.updateBatchTaskForm({
      hour,
      date
    });
  }

  onCalendar(){
    const {routes, actions} = this.props;

    if(!this._validateDiscipline() || !this._validateDescription()) return;
    
    if(routes.scene.name == 'task_form_chat'){
      Actions.task_calendar_chat({onSubmit: () => this.onSubmit()});
      return;
    }

    Actions.task_calendar({onSubmit: () => this.onSubmit()});
  }

  onSubmit(){
    const {tasks, actions} = this.props;
    const { task_description, date, hour} = tasks.form;
    if(!this._validateDiscipline()){
      return;
    }

    if(task_description == ''){
      Alert.alert(I18n.t('validation'),I18n.t('task_description_required'), [{
        text: I18n.t('accept')
      }]);
      return;
    }

    if(date == '' || hour ==''){
      Alert.alert(I18n.t('validation'),I18n.t('date_required'), [{
        text: I18n.t('accept')
      }]);
      return;
    }
    
    actions.updateBatchTaskForm({
      due_ending: moment(date).startOf('day').add(parseInt(hour) + 1, 'h'),
      due_on: moment(date).startOf('day').add(hour, 'h')
    });

    actions.saveTask();
  }

  render(){
    const {actions, tasks} = this.props;
    return (
      <AfterInteractions placeholder={<Spinner visible={true} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 12}} overlayColor='rgba(0,0,0,0)'/>}>
        <View style={[MainStyle.tabTop,{backgroundColor: '#f5f5f5'}]}>
          <ScrollView
              style={{marginBottom: 50}}
              keyboardShouldPersistTaps
            >
            {/*Task Description*/}
            <View style={{borderWidth: 1, margin: 15, borderColor: '#dcdcdc', paddingHorizontal: 10, paddingVertical: 5}}>
              <TextInput
                value={tasks.form.task_description}
                onChangeText={task_description => actions.updateTaskForm('task_description', task_description)}
                multiline={true}
                numberOfLines={4}
                style={{height: Platform.OS == 'android' ? 60: 40 , fontSize: 14}}
                autoCorrect={false}
                placeholder={I18n.t('task_description')}
              />
            </View>
            {/*Disciplines*/}
            <View style={{alignItems:'center'}}>
              <ScrollCardPicker
                items={tasks.disciplines}
                pressed={tasks.form.discipline_id}
                renderCard={(item, discipline) => 
                  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginRight: 20}}>
                    
                      <Icon name={discipline == item.id ? "check-circle" : 'circle-thin' } size={25} color={'#5a5a5a' } />
                    <Text style={{marginLeft: 5}}>{item.discipline}</Text>
                  </View>
                }
                onPress={discipline => this.handleDiscipline(discipline.id)}
              />
            </View>
            {/*Hours' header*/}
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginBottom: 20}}>
              <Text style={{fontWeight: '100'}}>{I18n.t('available_dates')}</Text>
              <TouchableOpacity
              onPress={() => this.onCalendar()}>
                <Text style={{fontWeight: 'bold'}}>{I18n.t('set_date')}></Text>
              </TouchableOpacity>
            </View>
            {/*Hours*/}
            {
              tasks.form.discipline_id && 
              this.getDates().map( (date, key) =>{
                if(date)
                  return (
                      <View key={key} style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginHorizontal: 10}}>
                        <View style={{marginTop: 5, marginRight: 5}}>
                          <Text style={{textAlign: 'center', fontWeight: '100', fontSize: 12}}>
                          {moment(date).format('ddd')}
                          </Text>
                          <Text style={{textAlign: 'center', fontWeight: '100', fontSize: 10}}>
                          {moment(date).format('D MMM')}
                          </Text>
                        </View>
                        <View style={{flex: 1, marginLeft: 5}}>
                          {
                            this.getHours(tasks.disciplineDates[date] || [], date).length > 0 ? 
                              <ScrollCardPicker
                                items={this.getHours(tasks.disciplineDates[date] || [], date)}
                                pressed={(tasks.form.hour && tasks.form.date == date) ? tasks.form.hour : ''}
                                onPress={hour => this.handleTimeChange(hour, date)}
                                display={ i => `${i > 12 ? i % 12 : i % 13 } ${i >= 12 ? 'PM' : 'AM'}` }
                                scrollViewStyles={{marginTop: 10}}
                                selectedBackgroundColor='#A2ABAE'
                              /> :
                              <View>
                                <Text style={{textAlign: 'center', fontWeight: '100', fontSize: 12, color: '#989E95', fontStyle: 'italic'}}>{I18n.t('no_available_hours')}</Text>
                              </View>
                          }
                        </View>
                      </View>
                  )
              })
            }
            {
              !tasks.form.discipline_id &&
              <View style={{marginHorizontal: 20, height: 200, alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={{color: '#A6ADB0'}}>{I18n.t('available_dates')}</Text>
                  <Text style={{color: '#A6ADB0'}}>{I18n.t('select_discipline')}</Text>
              </View>
            }
            <View style={{marginTop: 10}}>
              <Text style={{textAlign: 'center'}}>
                {I18n.t('date')}: {tasks.form.date && moment(tasks.form.date).startOf('day').add(tasks.form.hour, 'h').format('DD/MM/YY hh:mm a')}
              </Text>
            </View>
            <View style={{marginHorizontal: 30}}>
              <Button 
                style={{backgroundColor: '#18AEC3', height: 50, marginTop: 20}}
                onPress={()=> this.onSubmit()} 
              >
                <Text style={{color: 'white', textAlign: 'center', marginTop: 15, fontSize: 15}}>
                  {I18n.t('save')}
                </Text>
              </Button>
            </View>
          </ScrollView>
        </View>
      </AfterInteractions>
    )
  }
}

export default connect(
  state => ({
    tasks: state.tasks, 
    async: state.async,
    routes: state.routes
  }), 
  dispatch => ({actions: bindActionCreators({...taskActions}, dispatch)})
)(TaskForm)


