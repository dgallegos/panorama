import React, {Component} from 'react';
import { View, Text } from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import I18n from 'react-native-i18n'
//Components
import {MainStyle} from '../styles/mainStyle';
import Family from "./my_home/family.js";
import Settings from "./my_home/settings.js";
//Language
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Home extends Component{
  constructor(props){
    super(props);
    this.state = {
      modalOpen:false,
      form:'',
    }
  }
  
  render(){
    return(
      <View style={MainStyle.tabTop}>
        <ScrollableTabView
          style={{flex: 1, alignItems: 'stretch'}}
          renderTabBar={() =>
            <DefaultTabBar
              backgroundColor='#1B8F9F'
              inactiveTextColor='#38BCD2'
              activeTextColor='#FFFFFF'
              tabStyle={{ paddingBottom: 0}}
              textStyle={{fontWeight: 'normal', fontSize: 15}}
              underlineStyle={{backgroundColor: '#B8E986'}}
              style={{height: 40, paddingVertical: 10,borderWidth:1}}
            />
          }
        >
            <Family tabLabel={I18n.t('family')}/>
            <Settings tabLabel={I18n.t('settings')}/>
        </ScrollableTabView>
        <KeyboardSpacer/>
      </View>
    )
  }
}
