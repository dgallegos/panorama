import React, {Component} from 'react';
import { Text, View, TouchableOpacity, StyleSheet, TextInput, ScrollView, Picker, Alert } from 'react-native';
import update from 'react-addons-update';
import {Actions} from 'react-native-router-flux';
import I18n from 'react-native-i18n';
import moment from 'moment';
// Fetch
import { fetchApi, userInfo } from '../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../const/Apis.js';
// Form Elements
import Button from '../../components/button.js';
import Select from '../../components/picker.js';
import DatePicker from '../../components/date_picker.js';
import Autocomplete from 'react-native-autocomplete-input';
// import TextInput from '../../components/text_input.js';
import ImagePicker from 'react-native-image-picker';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';
import * as modalActions from '../../actions/modal_actions';
import {ModalButtons, ModalButtonsTop} from '../../components/modal_buttons.js';

import Visitor from './new_visit/visitors.js';
import Access from './new_visit/select_location.js';
import SelectDate from './new_visit/select_date.js';
import Confirmation from './new_visit/confirmation.js';

class NewVisitModal extends Component{
  constructor(props){
    super(props);
    this.state = {
      step: 1,
      disabled: false,
      locations: [],
      location_id: '',
      scheduled_datetime: moment().startOf('day'),
      limit_scheduled_datetime: moment().endOf('day'),
      notes: '',
    }

    this._bind('prevStep','nextStep','onChange','onSubmit');
  } 

  _bind(...methods) {
    methods.forEach(method => this[method] = this[method].bind(this));
  }


  componentDidMount(){
    userInfo().then(userData => {
      Promise.all([fetchApi(COMMON_AREA), fetchApi(`${VISITOR}/${userData.resident_id}`)])
      .then(data => {
        const [common_area, visitors] = data;
        // console.log(data)
        if(common_area.valid && visitors.valid){
          this.setState({
            locations: common_area.data,
            visitors: visitors.data,
            location_id: common_area.data.length ? common_area.data[0].id : ''
          });
        }
        else{
          console.log('error')
        }
      })
    });
  }

  onChange(obj){
    this.setState(obj);
  }

  onSubmit(){
    const {actions} = this.props;
    this.setState({
      disabled: true
    });

    actions.saveVisitor(this.state, data => {
      this.setState({
        disabled: false
      });
      if(data.valid){
        actions.flushState()
        actions.loadToday()
        Alert.alert('Success','Visit has been scheduled', [{
          text: 'Aceptar', onPress: () => actions.toggleModal()
        }]);
      }else{
        Alert.alert('Error','Ha habido un error', [{
          text: 'Aceptar'
        }]);
      }
    });
  }

  validate(){
    const {visitors} = this.props;
    switch(this.state.step){
      case 1:
        if(Object.keys(visitors.visitVisitors).length > 0)
          return true
        Alert.alert('Seleccione al menos un visitante', null ,[{
          text: 'Aceptar'
        }]);
        return false
      case 2: 
        if(moment(moment(this.state.scheduled_datetime)).isBefore(moment(this.state.limit_scheduled_datetime)))
          return true;
        Alert.alert('Fecha de salida debe ser mayor a la de entrada', null ,[{
          text: 'Aceptar'
        }]);
        return false;
      case 3:
        if(this.state.location_id)
          return true;
        return false;
      default:
        return true;
    }
  }

  prevStep(){
    if(this.state.step > 1)
      this.setState({
        step: this.state.step - 1
      })
    else
      this.props.actions.toggleModal(null);
  }

  nextStep(){
    if(this.validate())
      this.setState({
        step: this.state.step + 1
      })
  }


  renderStep(){
    const {actions, visitors} = this.props;
    switch(this.state.step){
      case 1:
        return(
          <Visitor 
            cancel={this.prevStep} 
            next={this.nextStep}
            actions={actions}
            visitors={visitors.visitVisitors}
          />
        );
        
      case 2: 
        return(
          <SelectDate
            prev={this.prevStep}
            next={this.nextStep}
            date={this.state.scheduled_datetime}
            limit_date={this.state.limit_scheduled_datetime}
            onChange={this.onChange}
            actions={actions}
          />
        );

      case 3: 
        return(
          <Access
            prev={this.prevStep}
            next={this.nextStep}
            locations={this.state.locations}
            location={this.state.location_id}
            actions={actions}
            onChange={this.onChange}
          />
        );

      case 4:
        return(
          <Confirmation
            prev={this.prevStep}
            confirm={this.onSubmit}
            visitors={visitors.visitVisitors}
            location={this.state.locations.find(l => l.id == this.state.location_id).name}
            scheduled_datetime={this.state.scheduled_datetime}
            actions={actions}
          />
        );
    }
  }

  render(){
    return(
      <View style={{flex: 1}}>
        {this.renderStep()}
      </View>
    )
  }
}

export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...visitorActions, ...modalActions},dispatch)})
)(NewVisitModal)


