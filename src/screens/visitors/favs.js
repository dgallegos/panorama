import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity, Alert} from 'react-native';
import SearchBar from '../../components/search_bar.js';
import ListItem from './list_item.js';
import Icon from 'react-native-vector-icons/Ionicons';
import {MainStyle} from '../../styles/mainStyle';
import { fetchApi, userInfo } from '../../components/utils.js';
import { VISIT_VISITOR, RESIDENT_VISITOR } from '../../const/Apis.js';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Bars } from 'react-native-loader';
//Redux
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';

class Favorites extends Component{
  constructor(props){
    super(props);
    this.dsFavs = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.dsLast = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      searchText: '',
      renderPlaceholderOnly: true,
      dsFavs: this.dsFavs,
      dsLast: this.dsLast
    }
  }

  componentDidMount(){
    //Get user data
    userInfo().then(userData=>{
      //fetch fav and suggestions
      Promise.all([`${VISIT_VISITOR}?id=${userData.resident_id}&count=1`, `${RESIDENT_VISITOR}/${userData.resident_id}`].map(url => fetchApi(url)))
      .then(data => {
        const [suggestions, fav] = data;
        // get visitors
        this.setState({
          renderPlaceholderOnly: false,
          dsLast: this.dsLast.cloneWithRows(suggestions),
          dsFavs: this.dsFavs.cloneWithRows(fav.data),
          suggestions,
          favorites: fav.data
        });
      })
    });
  }

  onChange(query){
    if(query == ''){
      this.setState({
        searchText: query,
        dsFavs: this.dsFavs.cloneWithRows(this.state.favorites),
        dsLast: this.dsLast.cloneWithRows(this.state.suggestions)
      });
      return;
    }

    let filterFavs = this.filterVisitors(this.state.favorites, query);
    let filterLast = this.filterVisitors(this.state.suggestions, query);

    this.setState({
      searchText: query,
      dsFavs: this.dsFavs.cloneWithRows(filterFavs),
      dsLast: this.dsLast.cloneWithRows(filterLast)
    });
  }

  filterVisitors(array, query){
    const regex = new RegExp(`${query}`,'gi');
    return array.filter(r => regex.test(r.name));
  }

  favRequest(cb, options={}, id=''){
    userInfo().then(userData=>{
      options.body.resident_id = userData.resident_id
      console.log(options)
      fetchApi(`${RESIDENT_VISITOR}/${id}`,{},options.body, options.method)
      .then(data => cb(data, this.state.favorites, this.dsFavs))
    });
  }

  removeFromFavs(favorite){
    const cb = (data, favorites, ds) => {
      if(data.valid){
        let newFavorites = favorites.filter(fav => fav.visitor_id !== favorite.visitor_id);
        //remove favorite
        this.setState({
          favorites: newFavorites,
          dsFavs: ds.cloneWithRows(newFavorites)
        });
        Actions.pop();
      }else{
        console.log('error', data)
      }
    } 

    let options = {
      body: {
        visitor_id: favorite.visitor_id,
        is_favorite: false
      },
      method: 'DELETE'
    };

    this.favRequest(cb, options, favorite.visitor_id);
  }

  addToFavs(suggestion){
    const cb = (data, favorites, ds) => {
      if(data.valid){
        let newFavorites = [...favorites, ...data.result];
        //add favorite
        this.setState({
          favorites: newFavorites,
          dsFavs: ds.cloneWithRows(newFavorites)
        });

        Actions.pop();
      }else{
        Alert.alert(data.message, null ,[{
          text: 'Aceptar', onPress: () => Actions.pop()
        }])
      }
    } 

    let options = {
      body: {
        visitor_id: suggestion.visitor_id,
        is_favorite: true
      },
      method: 'POST'
    };

    this.favRequest(cb, options)
  }

  renderPlaceholderView(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }

  render(){
    const { searchText, dsFavs, dsLast, favorites, suggestions } = this.state;
    if(this.state.renderPlaceholderOnly){
      return this.renderPlaceholderView();
    }

    return(
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#EFEFEF'}}>
        {/*Search Bar*/}
        <SearchBar
          styleContainer={{backgroundColor:'#C9C9CE'}}
          style={MainStyle.searchBar}
          value={searchText}
          placeholder='Search'
          onChange={this.onChange.bind(this)}
        />
        {/*Favorites*/}
        <View style={{marginVertical: 10, marginHorizontal: 20}}>
          <Text style={{color: '#9B9B9B'}}>My Favorites</Text>
        </View>

        { dsFavs.rowIdentities[0].length ?
          <ListView
            renderRow={favorite =>
              <View style={{backgroundColor: '#fff', flexDirection: 'row', alignItems: 'center', borderWidth: 0.5, borderColor: '#D9D9D9'}}>
                <TouchableOpacity 
                  style={{marginHorizontal: 12, marginVertical: 10}}
                  onPress={()=>Actions.make_favorite({favAction: () => this.removeFromFavs(favorite), title: 'remove', visitor_name: favorite.visitor.name})}
                >
                  <Icon name='ios-star' size={25} color={'#febf00'}/>
                </TouchableOpacity>
                <Text>{favorite.visitor.name}</Text>
              </View>
            }
            dataSource={dsFavs}
            enableEmptySections
          /> :
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }
      {/*Suggestions*/}
        <View style={{marginVertical: 10, marginHorizontal: 20}}>
          <Text style={{color: '#9B9B9B'}}>Suggestions - Last 30 days</Text>
        </View>

        { dsLast.rowIdentities[0].length ?
          <ListView
            renderRow={suggestion => (
              <View style={{flexDirection: 'row', backgroundColor: '#fff', borderWidth: 0.5, borderColor: '#D9D9D9'}}>
                <View style={{flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                  <TouchableOpacity 
                    style={{marginHorizontal: 10, marginVertical: 10}} 
                    onPress={()=>{
                      this.props.actions.setCurrentVisitor(suggestion);
                      Actions.access();
                    }}
                  >
                    <Icon name='ios-add-circle-outline' size={25} color='#2AA7DC'/>
                  </TouchableOpacity>
                  <Text>{suggestion.name}</Text>
                </View>
                <TouchableOpacity 
                  style={{marginHorizontal: 10, marginVertical: 10}}
                  onPress={()=>Actions.make_favorite({favAction: () => this.addToFavs(suggestion), title: 'add', visitor_name: suggestion.name})}
                >
                  <Icon name='ios-star-outline' size={25} color='#2AA7DC'/>
                </TouchableOpacity>
              </View>
            )}
            dataSource={dsLast}
            enableEmptySections
          /> :
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }
      </View>
    )
  }
}

export default connect(
  state => ({...state}),
  dispatch => ({actions: bindActionCreators({...visitorActions},dispatch)})
)(Favorites);
