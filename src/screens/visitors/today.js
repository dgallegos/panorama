import React, {Component} from 'react';
import { View, Text, ListView, InteractionManager, Alert, RefreshControl, TouchableOpacity} from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';
import moment from 'moment';
import update from 'react-addons-update';
import { Bars } from 'react-native-loader';
import { Actions } from 'react-native-router-flux';
import I18n from 'react-native-i18n'
//Components
import { fetchApi, userInfo } from '../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../const/Apis.js';
import SwipeRow from '../../components/swipe_row.js';
import ListItem from './list_item.js';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';
import * as modalActions from '../../actions/modal_actions';
// import * as asyncActions from '../../actions/async_actions';
//Language
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

class Today extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      ds: this.ds.cloneWithRows([]),
      form: {
        scheduled_datetime: moment()
      },
      isRefreshing: false
    }
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount(){
    this.getVisits();
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.visitors.visitors.length !== this.props.visitors.visitors.length){
      this.setState({
        ds: this.ds.cloneWithRows(nextProps.visitors.visitors)
      });
    }
  }

  getVisits(){
    return this.props.actions.loadToday(visitors =>
      this.setState({
        ds: this.ds.cloneWithRows(visitors)
      })
    );
  }

  onRefresh(){
    this.setState({isRefreshing: true});
    this.getVisits()
    .then(()=> this.setState({isRefreshing: false}));
  }


  onChange(value, field){
    this.setState({
      form: update(this.state.form,{
        $merge:{
          [value]: field
        }
      })
    });
  }

  renderLoading(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }

  fetchVisit(visitor, body){
    fetchApi(`${VISIT}/${visitor.visit.id}?date=today`,{}, body, 'PUT')
    .then(data=> {
      if(data.valid){
        Actions.pop();
        this.props.actions.removeVisitor(visitor);
      }else{
        Alert.alert('Info',data.message, null ,[{
          text: 'Aceptar', onPress: () => Actions.pop()
        }]);
      }
    })
  }

  moveVisit(visitor){
    this.fetchVisit(visitor, {
        scheduled_datetime: this.state.form.scheduled_datetime
      })
  } 

  cancelVisit(visitor){
    this.fetchVisit(visitor, {
        status: 'Cancelado'
      });
  }

  render(){
    const { ds } = this.state;
    const { modal, actions, async } = this.props
    
    if(!async.fetch_success_today && !async.fetch_failure_today){
      return this.renderLoading();
    }
    
    return(
      <View style={{marginTop: 15, flex: 1, marginBottom: 50}}>
        <SwipeListView
          dataSource={ds}
          enableEmptySections={true}
          renderRow={ visitor => (
            <ListItem
              {...visitor}
            />
          )}
          renderHiddenRow={visitor => (
            <SwipeRow
              onPressEdit={() => Actions.move_cancel({
                  actionAccept: () => this.moveVisit(visitor), 
                  action: 'move', 
                  onChange: this.onChange
                }
              )}
              onPressDelete={()=> Actions.move_cancel({
                actionAccept: () => this.cancelVisit(visitor), 
                action: 'cancel'
                })
              }
              editText='Move'
              deleteText='Cancel'
            />
          )}
          rightOpenValue={-150}
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
              tintColor="#1DEAFF"
              title={I18n.t('loading')}
              titleColor="#1DEAFF"
              colors={['#ff0000', '#00ff00', '#0000ff']}
              progressBackgroundColor="#ffff00"
            />
          }
        /> 
        { 
          ds.rowIdentities[0].length == 0 &&
          <View style={{flex: 1, alignItems: 'center'}}>
            <Text style={{color: '#089CC5', fontSize: 16}}>
              {I18n.t('no_visitors_today')}
            </Text>
          </View>
        }
      </View>
    )
  }
}

Today.contextTypes = {
  openModal: React.PropTypes.func
};

const mapStateToProps = state => ({...state});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...visitorActions, ...modalActions}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Today);

