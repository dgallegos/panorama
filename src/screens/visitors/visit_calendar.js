import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Dimensions, ListView, Alert, ScrollView} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {MainStyle} from '../../styles/mainStyle';
import Calendar from 'react-native-calendar';
import {ScrollCardPicker} from '../../components/scroll_time_picker_card.js';
import moment from 'moment';
import Button from '../../components/button.js';
import {AfterInteractions} from 'react-native-interactions';
import Spinner from 'react-native-loading-spinner-overlay';
//Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as visitorsActions from '../../actions/visitors_actions.js';
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const customDayHeadings = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const customMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May',
  'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const calendarStyles = {
  calendarHeading:{
    borderBottomWidth: 0,
    borderTopWidth: 0
  },
  calendarContainer:{
    backgroundColor: '#fff'
  }
}

class VisitCalendar extends Component{
  constructor(){
    super();
    this.state = {
      date: moment(),
      hour: ''
    }
  }

  onChangeHour(hour){
    const {actions} = this.props;
    
    actions.updateVisitForm('scheduled_datetime', moment(this.state.date).startOf('day').add(hour, 'h'));
    actions.updateVisitForm('limit_scheduled_datetime', moment(this.state.date).startOf('day').add(hour + 4, 'h'));

    this.setState({hour});
  }

  render(){
    return(
      <AfterInteractions placeholder={<Spinner visible={true} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 12}} overlayColor='rgba(0,0,0,0)'/>}>
        <View style={[MainStyle.tabTop,{backgroundColor: '#f5f5f5'}]}>
          <View>
            <Text style={{margin: 20, textAlign:'center', fontSize: 16, color: '#838387'}}>
              Seleccione fecha
            </Text>
            <Calendar
              currentMonth={moment()}
              scrollEnabled
              showControls
              prevButtonText={I18n.t('prev')}
              nextButtonText={I18n.t('next')}
              weekStart={0}
              customStyle={calendarStyles}
              selectedDate={this.state.date}
              onDateSelect={date => this.setState({date})}
            />
            <ScrollCardPicker
              items={Array.from({length: 23}, (v,i)=> i + 1)}
              pressed={this.state.hour}
              onPress={ i => this.onChangeHour(i)}
              display={ i => `${i > 12 ? i % 12 : i % 13 } ${i >= 12 ? 'PM' : 'AM'}` }
              containerStyle={{marginVertical: 20}}
              selectedBackgroundColor='#A2ABAE'
              contentOffset={{y: 0, x: 70*6}}
            />
            <View>
              <Button
                onPress={() => Actions.pop()}
                style={{backgroundColor: '#1EA9C7', padding: 10, marginHorizontal: 100, alignItems: 'center'}}
              >
                <Text style={{color: '#fff'}}>{I18n.t('go_back')}</Text>
              </Button>
            </View>
          </View>
        </View>
      </AfterInteractions>
    )
  }
}

export default connect(state => ({visitors: state.visitors}), dispatch => ({
  actions: bindActionCreators({...visitorsActions}, dispatch)
}))(VisitCalendar);