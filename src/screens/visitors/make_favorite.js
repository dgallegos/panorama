import React, {Component} from 'react';
import { Text, View } from 'react-native';
import Modal from '../../components/modal.js';
import {Actions} from 'react-native-router-flux';

const description = {
  add: 'I understand this will remove the privileges of easy access to this person.',
  remove: 'I understand this authorization give privileges easy access to this person.'
};

const titles={
  add: 'Add',
  remove: 'Delete'
}

class MakeFavorite extends Component{
  state = {
    disabled: false,
    title: 'Test'
  }

  acceptModal(){
    this.props.favAction();
  }

  closeModal(){
    Actions.pop();
  }

  render(){
    return(
      <Modal
        title={`${titles[this.props.title]} "${this.props.visitor_name}"`}
        accept={this.acceptModal.bind(this)}
        cancel={this.closeModal} 
        disabledButton={this.state.disabled} 
      >
        <View style={{padding: 20}}>
          <Text style={{textAlign: 'center'}}>
            {description[this.props.title]}
          </Text>
        </View>
      </Modal>
    )
  }
}

export default MakeFavorite;
