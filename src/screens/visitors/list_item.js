import React from 'react';
import {TouchableHighlight, Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

const ListItem = ({name, location, time, date, status}) => (
  <TouchableHighlight
    onPress={()=>console.log('onPress')}
    underlayColor='rgba(255,255,255,1)'
    style={styles.rowFront} 
  >
    <View style={{flex: 1, marginVertical: 12, flexDirection: 'row'}}>
      <View style={{flex: 1, marginHorizontal: 45}}>
        <Text style={{fontSize: 14}}>{name}</Text>
        <Text style={{fontSize: 13}}>{location}</Text>
        <Text style={{fontSize: 13}}>{date}</Text>
      </View>

      <View style={{marginRight: 24}}>
        <Text style={{color: '#74B62A', fontSize: 13}}>{status}</Text>
      </View>
    </View>

  </TouchableHighlight>
);

const styles = StyleSheet.create({
   rowFront: {
    alignItems: 'flex-start',
    backgroundColor: '#fff',
    justifyContent: 'center',
    margin: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
  },
});

export default ListItem;