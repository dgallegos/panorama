import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Dimensions, ListView, Alert, ScrollView, Platform, Keyboard} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
import * as visitorActions from '../../actions/visitors_actions';
import {Actions} from 'react-native-router-flux';
import Autocomplete from 'react-native-autocomplete-input';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScrollCardPicker} from '../../components/scroll_time_picker_card.js';
import Button from '../../components/button.js';
import moment from 'moment';
import {AfterInteractions} from 'react-native-interactions';
import Spinner from 'react-native-loading-spinner-overlay';
var Contacts = require('react-native-contacts')

//Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//Language
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const Placeholder = () => (
  <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
    <Bars size={15} color="#18AEC3" />
  </View>
)


class VisitForm extends Component{
  constructor(props){
    super(props)
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.state={
      query: '',
      clicked: false,
      schedule: I18n.t('now'),
      location: ''
    }
  }

  componentWillReceiveProps(nextProps){   
    const {actions} = this.props; 
    if(this.props.async.fetch_success_visit != nextProps.async.fetch_success_visit){
      if(nextProps.async.fetch_success_visit){
        Alert.alert(I18n.t('success'),I18n.t('saved_success'), [{
          text: I18n.t('accept'),
          onPress: () => {
            actions.flushState();
            actions.loadToday();
            Actions.pop();
          }
        }]);
        
        return;
      }
    }

    if(this.props.async.fetch_failure_visit != nextProps.async.fetch_failure_visit){
      if(nextProps.async.fetch_failure_visit){
        Alert.alert(I18n.t('failure'),I18n.t('saved_failure'), [{
          text: I18n.t('accept')
        }]);
      }
      return;
    }
  }

  componentDidMount(){
    const { actions } = this.props;
    
    return Promise.all([actions.loadHistory(), actions.loadCommonAreas(), this.getContacts()]);
  }

  getContacts(){
    const { actions } = this.props;

    return Contacts.getAll((err, contacts) => {
      if(err === 'denied'){
        // x.x
      } else {

        const phonebookContacts = contacts.map(contact => ({
          id: contact.recordID,
          name: `${contact.givenName} ${contact.familyName}` 
        }));
        console.log(phonebookContacts)
        actions.addHistoryVisitors(phonebookContacts);
      }
    });
  }

  _filterData(query){
    if(query == '') return [];

    const {visitors} = this.props;
    const regex = new RegExp(`${query.trim()}`,'i');
    const data = visitors.history.filter(visitor => visitor.name.search(regex) >= 0);
    data.push({name: 'empty'})
    return data;
  }

  renderVisitors(visitor){
    const {actions} = this.props;
    return (
      <View style={{flexDirection: 'row', backgroundColor:'#fff', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#e5e5e7'}}>
        <View style={{flexGrow: 1}}>
          <Text style={{paddingLeft: 10}}>{visitor.name}</Text>
        </View>
        <TouchableOpacity 
          style={{backgroundColor: '#ED7B65', padding: 10}}
          onPress={() => actions.removeVisitVisitor(visitor.id)}>
          <Icon name='times' color='#fff' size={20}/>
        </TouchableOpacity>
      </View>
    )
  }

  /**
   * Adds nonexistent visitor to visit (other than the selected in list)
   * @param  object visitor contains id and name
   */
  _addNewVisitor(visitor){
    const {actions, visitors} = this.props;
    const existName = Object.keys(visitors.history).filter(key => visitors.history[key].name == this.state.searchText);
    
    if(existName.length > 0){
      Alert.alert('Error','Visitor name already exists', [{
          text: 'Aceptar'
        }]);
      return;
    }

    if(this.state.searchText == ''){
      Alert.alert('Error','Please add a valid visitor', [{
          text: 'Aceptar'
        }]);
      return;
    }
    // create a non existent id to be added to visitVisitor state
    let flag = true;
    let newId;
    while(flag){
      newId = Math.random()*10000;
      if(!visitors.visitVisitors[newId])
        flag = false
    }

    // actions.addHistoryVisitors([{
    //   id: newId,
    //   name: this.state.query
    // }]);

    actions.addVisitVisitor({
      id: newId,
      name: this.state.query
    });
  }

  _handleClickItem(visitor){
    const {actions} = this.props;

    if(!visitor.id){
      this._addNewVisitor(visitor)
      
    }else{
      actions.addVisitVisitor(visitor)  
    }

    this.setState({ 
      query: '', 
      clicked: true
    });
  }

  handleNewVisitor(){
    return this.state.query
  }

  handleSchedule(schedule){
    this.setState({schedule})
    
    const {actions, routes} = this.props;
    switch(schedule){
      case I18n.t('future'):
        if(routes.scene.name == 'visitors_form_chat'){
          Actions.visit_calendar_chat();
          return;
        }

        Actions.visit_calendar();
        return;

      case I18n.t('now'):
        actions.updateVisitForm('scheduled_datetime', moment())
        actions.updateVisitForm('limit_scheduled_datetime', moment().add(4, 'h'))
        return;

      case I18n.t('today'): 
        actions.updateVisitForm('scheduled_datetime', moment())
        actions.updateVisitForm('limit_scheduled_datetime', moment().endOf('day'))
        return;

      default:
        return;
    } 
  }

  validateRequest(){
    const {visitors} = this.props;

    if(this.state.location == ''){
      Alert.alert(I18n.t('validation'),I18n.t('empty_location'), [{
        text: I18n.t('accept')
      }]);
      return false;
    }

    if(Object.keys(visitors.visitVisitors).length == 0){
      Alert.alert(I18n.t('validation'),I18n.t('empty_visitors'), [{
        text: I18n.t('accept')
      }]);
      return false;
    }

    return true;
  }

  onSubmit(){
    if(!this.validateRequest()) return; 

    const {actions} = this.props;
    actions.saveVisitor({location: this.state.location})
  }


  render(){
    const {visitors, actions} = this.props;
    const visitVisitors = Object.keys(visitors.visitVisitors).map(id => visitors.visitVisitors[id])
    const data = this._filterData(this.state.query);
    const {query} = this.state;
    return(
      <AfterInteractions placeholder={<Spinner visible={true} textContent={"Loading..."} color="#18AEC3" textStyle={{color: "#18AEC3", fontWeight: '300', fontSize: 14}} overlayColor='rgba(0,0,0,0)'/>}>
        <View style={[MainStyle.tabTop,{backgroundColor: '#f5f5f5'}]}>
          {
              Platform.OS === 'ios' && 
                <View style={{position: 'relative', zIndex: 2,backgroundColor: '#fff'}}>
                  <Autocomplete
                    data={ query == '' ? [] : data}
                    defaultValue={query}
                    placeholder={I18n.t('search_visitor')}
                    listStyle={{zIndex: 1,flex: 1, height: data.length * 30 + 40, position: 'absolute', margin: 0, top: 40, padding: 0}}
                    listContainerStyle={{flex: 1, height: 400}}
                    inputContainerStyle={{borderWidth: 1,margin:  0, padding: 0, backgroundColor: '#fff'}}
                    style={{fontSize: 14}}
                    returnKeyType='done'
                    onChangeText={text => this.setState({query: text, clicked: false })}
                    renderItem={(row, _ , ri) =>
                      <View style={{width: Dimensions.get('window').width, margin: 0, padding: 0}}>
                        { row.name != 'empty' &&
                          <TouchableOpacity 
                            onPress={() => this._handleClickItem(row)}
                            style={{marginHorizontal: 5, paddingTop: 5, borderBottomWidth: 1, borderBottomColor: '#eee'}}>
                            <Text style={{color: '#5B5B5B', paddingVertical: 10}}>{row.name}</Text>
                          </TouchableOpacity>
                        }
                        { 
                          row.name == 'empty' && 
                          <TouchableOpacity
                            style={{paddingTop: 5}}
                            onPress={() => this._handleClickItem(row)}
                          >
                            <Text style={{textAlign:'center', fontWeight: '600', color: '#5B5B5B', paddingVertical: 15}}>
                              {I18n.t('plus_add_visitor')}                      
                            </Text>
                          </TouchableOpacity>
                        }
                      </View>
                    }
                  />
                </View>
            }
            {
              Platform.OS === 'android' && 
                <Autocomplete
                  data={ query == '' ? [] : data}
                  defaultValue={query}
                  placeholder={I18n.t('search_visitor')}
                  containerStyle={styles.autocompleteContainer}
                  listStyle={{marginTop: 0, top: 54}}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  inputContainerStyle={{margin:  0, padding: 0, backgroundColor: '#fff'}}
                  style={{fontSize: 14}}
                  onChangeText={text => this.setState({query: text, clicked: false })}
                  renderItem={(row, _ , ri) =>
                    <View style={{width: Dimensions.get('window').width, margin: 0, padding: 0}}>
                      { row.name != 'empty' &&
                        <TouchableOpacity 
                          onPress={() => this._handleClickItem(row)}
                          style={{marginHorizontal: 5, paddingTop: 5, borderBottomWidth: 1, borderBottomColor: '#eee'}}>
                          <Text style={{color: '#5B5B5B', paddingVertical: 10}}>{row.name}</Text>
                        </TouchableOpacity>
                      }
                      { 
                        row.name == 'empty' && 
                        <TouchableOpacity
                          style={{paddingTop: 5}}
                          onPress={() => this._handleClickItem(row)}
                        >
                          <Text style={{textAlign:'center', fontWeight: '600', color: '#5B5B5B', paddingVertical: 10}}>
                            {I18n.t('plus_add_visitor')}                      
                          </Text>
                        </TouchableOpacity>
                      }
                    </View>
                  }
                />
            }
          <ScrollView
            style={{marginTop: 40, marginBottom: 50}}
            keyboardShouldPersistTaps="always"
          >
            <View style={{ marginTop: Platform.OS === 'android'? 80 : 20, minHeight: 120}}>
              <Text style={{textAlign: 'center', color: '#838387'}}>{I18n.t('visitors').toUpperCase()}</Text>
              {
                visitVisitors.length > 0 ?
                  <ListView
                    dataSource={this.ds.cloneWithRows(visitVisitors)}
                    renderRow={this.renderVisitors.bind(this)}
                    style={{flexGrow: 1, marginVertical : 20}}

                    enableEmptySections
                  /> :
                  <View style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold'}}>{I18n.t('no_visitors')}</Text>
                  </View>
              }
            </View>
            <View style={{alignItems:'center'}}>
              <View>
                <Text style={{textAlign: 'center', color: '#838387'}}>{I18n.t('schedule_visit').toUpperCase()}</Text>
                <ScrollCardPicker
                  items={[I18n.t('now'), I18n.t('today'), I18n.t('future')]}
                  pressed={this.state.schedule}
                  onPress={this.handleSchedule.bind(this)}
                  cardStyles={{width: 100}}
                  containerStyle={{marginTop: 20}}
                  selectedBackgroundColor='#A2ABAE'
                />
              </View>
              { 
                  this.state.schedule == I18n.t('future') &&
                <View>
                  <Text>{I18n.t('in')}: {visitors.form.scheduled_datetime.format('DD/MM/YY hh:mm a')}</Text>
                  <Text>{I18n.t('out')}: {visitors.form.limit_scheduled_datetime.format('DD/MM/YY hh:mm a')}</Text>                
                </View>
              }
              <View>
                <Text style={{textAlign: 'center', color: '#838387', marginTop: 10}}>{I18n.t('location').toUpperCase()}</Text>
                <ScrollCardPicker
                  items={visitors.commonAreas.sort((a,b) => a.sort - b.sort).map(area => area.name)}
                  pressed={this.state.location}
                  onPress={location => this.setState({location})}
                  cardStyles={{width: 140}}
                  containerStyle={{marginTop: 20}}
                  selectedBackgroundColor='#A2ABAE'
                />
              </View>
            </View>
            <View style={{marginHorizontal: 10}}>
              <Button 
                style={{backgroundColor: '#18AEC3', height: 50, marginTop: 40}}
                onPress={()=> this.onSubmit()} 
              >
                <Text style={{color: 'white', textAlign: 'center', marginTop: 15, fontSize: 15}}>
                  {I18n.t('save')}
                </Text>
              </Button>
            </View>
          </ScrollView>
          
        </View>
      </AfterInteractions>
    )
  }
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    flexGrow: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    zIndex: 1,
    top: 0,
  }
});

const getHistory = (state) => 
  Object.keys(state.visitors.history).map(id => state.visitors.history[id]);

const mapStateToProps = state => ({
  visitors: {
    ...state.visitors,
    history: getHistory(state)
  },
  async: state.async,
  routes: state.routes,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...visitorActions}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(VisitForm);