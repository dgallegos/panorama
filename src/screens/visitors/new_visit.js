import React, {Component} from 'react';
import { Text, View, TouchableOpacity, ScrollView, Alert } from 'react-native';
import Modal from '../../components/modal.js';
import update from 'react-addons-update';
import {Actions} from 'react-native-router-flux';
import { fetchApi, userInfo } from '../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../const/Apis.js';
import { MainStyle } from '../../styles/mainStyle.js';
import Select from '../../components/picker.js';
import DatePicker from '../../components/date_picker.js';
import I18n from 'react-native-i18n';
import TextInput from '../../components/text_input.js';
import moment from 'moment';
import Autocomplete from 'react-native-autocomplete-input';
import Icon from 'react-native-vector-icons/FontAwesome';
import Button from '../../components/button.js';
import ImagePicker from 'react-native-image-picker';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';

class NewVisit extends Component{
  constructor(props){
    super(props);
    this.state = {
      disabled: false,
      locations: [],
      location_id: '',
      scheduled_datetime: moment().format('DD/MM/YYYY'),
      limit_scheduled_datetime: moment().format('DD/MM/YYYY'),
      notes: '',
      visitors: [],
      visitVisitors: [],
      query: '',
      clicked: false
    }

    this.selectQueryListItem.bind(this);
    this.deleteVisitor.bind(this);
    this.addVisitor.bind(this);
  }

  componentDidMount(){
    userInfo().then(userData => {
      Promise.all([fetchApi(COMMON_AREA), fetchApi(`${VISITOR}/${userData.resident_id}`)])
      .then(data => {
        const [common_area, visitors] = data;
        // console.log(data)
        if(common_area.valid && visitors.valid){
          this.setState({
            locations: common_area.data,
            visitors: visitors.data
          });
        }
        else{
          console.log('error')
        }
      })
    });
  }

  queryVisitors(query) {
    if (query === '') {
      return [];
    }

    const { visitors } = this.state;
    // console.log(visitors)
    const regex = new RegExp(`${query.trim()}`, 'i');
    return visitors.filter(visitor => visitor.name ? visitor.name.search(regex) >= 0 : false);
  }

  selectQueryListItem(query, clicked, name){
    this.setState({
      query: '', 
      clicked,
      visitVisitors: update(this.state.visitVisitors,{
        $push: [name]
      })
    });
  }

  deleteVisitor(idx){
    this.setState({
      visitVisitors: update(this.state.visitVisitors,{
        $splice: [[idx, 1]]
      })
    });
  }

  addVisitor(query){
    this.setState({
      visitVisitors: update(this.state.visitVisitors,{
        $push: [query]
      }),
      query: ''
    });
  }

  onSubmit(){
    this.setState({
      disabled: true
    });
    this.props.actions.saveVisitor(this.state, (data) => {
      this.setState({
        disabled: false
      });
      // console.log(data)
      if(data.valid){
        let visitors = [];
        data.data.forEach(visit =>{
          let v = visit.visitVisitors.map(visitVisitor => {
            return {
              name: visitVisitor.visitor.name, 
              location: 'Condo', 
              time: moment(visit.scheduled_datetime).format('h:mm'),
              status: visitVisitor.visitor.arrive ? 'Arrived' : 'Pending',
              visit: visit
            }
          });
          visitors = visitors.concat(v);
        });
        Alert.alert('Info','Se ha creado su solicitud', [{
          text: 'Aceptar', onPress: () => Actions.pop()
        }]);
        return visitors
      }else{
        Alert.alert('Info','Ha habido un error', [{
          text: 'Aceptar'
        }]);
        return [];
      }
    });
  }

  render(){
    const { query } = this.state;
    const visitors = this.queryVisitors(query);
    const comp = (s, s2) => s.toLowerCase().trim() === s2.toLowerCase().trim();
    return(
      <View style={[MainStyle.tabTop,{backgroundColor: '#EFEFEF'}]}>
        <View style={{flex: 1, zIndex: 1}}>
          <View style={{marginTop: 30}}>
            <Text style={{fontSize: 16, paddingBottom: 5, paddingLeft: 10, marginBottom: 10}}>{I18n.t('visit')}</Text>
          </View> 
          <View style={{backgroundColor: '#fff', zIndex: 1}}>
            <Select
              onChange={location_id=>this.setState({location_id})}
              value={this.state.location_id}
              options={this.state.locations}
              display='name'
              keyVal='id'
              label={I18n.t('common_area')}
            />
            <DatePicker
              label='Entry'
              date={this.state.scheduled_datetime}
              mode="datetime"
              placeholder="Select Date"
              format="DD/MM/YYYY h:mm a"
              customStyles={{
                dateInput: {
                  margin: 10,
                  borderWidth: 0
                },
                dateText:{
                  paddingLeft: 50
                }
              }}
              onChange={scheduled_datetime => this.setState({scheduled_datetime})}
            />
            <DatePicker
              label='Exit'
              date={this.state.limit_scheduled_datetime}
              mode="datetime"
              placeholder="Select Date"
              format="DD/MM/YYYY h:mm a"
              customStyles={{
                dateInput: {
                  margin: 10,
                  borderWidth: 0
                },
                dateText:{
                  paddingLeft: 50
                }
              }}
              onChange={limit_scheduled_datetime => this.setState({limit_scheduled_datetime})}
            />
            <TextInput
              value={this.state.notes}
              onChange={notes => this.setState({notes})}
              placeholder="Notes"
              label='Notes'
              numberOfLines={2}
              inputStyles={{textAlign: 'left'}}
              inputContainerStyles={{padding: 5}}
              containerStyles={{borderBottomWidth: 1, borderBottomColor: 'rgb(197,195,201)'}}
              multiline
            />
            
          </View>
          {/*Autocomplete*/}
          <View style={{flexDirection:'row', borderBottomWidth: 1, borderBottomColor: 'rgb(197,195,201)', alignItems: 'center', position: 'relative', zIndex: 1,backgroundColor: '#fff'}}>
            <View style={{flex: 1}}>
              <Text style={{textAlignVertical :'center', marginLeft: 10, fontWeight: '500'}}>Select Visitor</Text>
            </View>
            <Autocomplete
              autoCapitalize="none"
              autoCorrect={false}
              data={((visitors.length === 1 && comp(query, visitors[0].name)) || this.state.clicked) ? [] : visitors}
              defaultValue={query}
              containerStyle={{borderWidth: 0, zIndex: 1}}
              inputContainerStyle={{borderWidth: 0, margin: 0, }}
              onChangeText={text => this.setState({ query: text, clicked: false })}
              placeholder="Enter visitor name"
              listStyle={{zIndex: 1, position: 'absolute'}}
              style={{backgroundColor: 'transparent', fontSize: 14}}
              renderItem={({ name }) => (
                <TouchableOpacity style={{padding: 5}} onPress={() => this.selectQueryListItem(query, true, name)}>
                  <Text>
                    {name} 
                  </Text>
                </TouchableOpacity>
              )}
            />
            <TouchableOpacity 
              style={{backgroundColor: '#18AEC3', padding: 10}}
              onPress={()=>this.addVisitor(query)}
            >
              <Icon name='plus' color='#fff' size={20}/>
            </TouchableOpacity>
          </View>
          {/*Visitors*/}
          <View style={{marginTop: 20, borderBottomWidth: 1, borderBottomColor: 'rgba(128,128,128,.2)'}}>
            <Text style={{fontSize: 16, paddingBottom: 5, paddingLeft: 5, marginBottom: 10}}> {I18n.t('your_visits')} </Text>
          </View>
          <ScrollView style={{height: 160}}>
            {
              this.state.visitVisitors.map( (v,idx) => 
                <TouchableOpacity
                  style={{backgroundColor: '#fff', paddingLeft: 10, borderBottomWidth: 1, borderBottomColor: 'rgb(197,195,201)'}}
                  onPress={()=> this.deleteVisitor(idx) }
                  key={idx}
                >
                  <View style={{flexDirection:'row', alignItems: 'center'}}>
                    <View style={{flex: 1}} >
                      <Text>{v}</Text>
                    </View>
                    <View style={{backgroundColor: '#18AEC3', padding: 10}}>
                      <Icon name='trash' color='#fff' size={20}/>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            }
          </ScrollView>
        </View>
        {/*Button*/}
        <Button 
          style={{backgroundColor: '#18AEC3', height: 45, marginTop: 20, justifyContent: 'center'}}
          onPress={this.onSubmit.bind(this)}
          disabled={this.state.disabled}
        >
          <Text style={{ textAlign:'center', color: 'white', marginVertical: 10, marginHorizontal: 60, fontWeight: 'bold'}}>Create</Text>
        </Button>
      </View>
    )
  }
}

const mapStateToProps = state => ({...state});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(visitorActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NewVisit);
