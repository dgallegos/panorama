import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import Modal from '../../components/modal.js';
import I18n from 'react-native-i18n';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';
//Utils
import { fetchApi, userInfo } from '../../components/utils.js';
//Language
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

class QuickVisit extends Component{
  state = {
    disabled: false
  }

  componentDidMount(){
    const {actions, visitors} = this.props;

    actions.addVisitVisitor({
      id: Math.random()*100,
      name: visitors.currentVisitor.name
    });
  }

   componentWillReceiveProps(nextProps){   
    const {actions} = this.props; 
    if(this.props.async.fetch_success_visit != nextProps.async.fetch_success_visit){
      if(nextProps.async.fetch_success_visit){
        Alert.alert(I18n.t('success'),I18n.t('saved_success'), [{
          text: I18n.t('accept')
        }]);
        actions.flushState();
        actions.loadToday();
        Actions.pop();
        return;
      }
    }

    if(this.props.async.fetch_failure_visit != nextProps.async.fetch_failure_visit){
      if(nextProps.async.fetch_failure_visit){
        Alert.alert(I18n.t('failure'),I18n.t('saved_failure'), [{
          text: I18n.t('accept')
        }]);
      }
      return;
    }
  }

  addVisit(){
    const {actions} = this.props;
    this.setState({disabled: true});

    actions.saveVisitor({location: 11})
      .then(_ => this.setState({disabled: false}));
  }

  closeModal(){
    Actions.pop();
  }

  onChange(date){
    const {actions} = this.props;
    
    actions.updateBatchVisitorForm({
      scheduled_datetime: moment(date).startOf('day'),
      limit_scheduled_datetime: moment(date).startOf('day').add(4, 'h')
    });
  }

  render(){
    const {visitors} = this.props;
    return(
      <Modal
        title={visitors.currentVisitor.name}
        accept={this.addVisit.bind(this)}
        cancel={this.closeModal.bind(this)} 
        disabledButton={this.state.disabled} 
      >
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{textAlign: 'center', padding: 20}}>
            Do you approve the entry of this person to the project? I understand this privilege is valid for one day.
          </Text>

          <View style={{flexDirection: 'row', marginTop: 10, paddingHorizontal: 50}}>
            <Text style={{paddingHorizontal: 5, fontWeight: '500', textAlignVertical: 'center', marginTop: 10}}>Fecha:</Text>
            <DatePicker
              date={visitors.form.scheduled_datetime}
              mode="date"
              placeholder="select date"
              format="DD/MM/YY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateInput: {
                  margin: 10,
                  borderWidth: 0
                }
              }}
              showIcon={false}
              onDateChange={this.onChange.bind(this)}
            />
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modalOverlay: {
    flex: 1, 
    justifyContent: 'center', 
    backgroundColor: 'rgba(0,0,0,0.5)', 
    alignItems: 'center'
  },
  modalContainer: {
    backgroundColor: '#fff', 
    marginHorizontal: 40, 
    marginVertical: 200
  },
  modalHeader: {
    height: 40, 
    backgroundColor: '#18AEC3', 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  modalHeaderText: {
    color: '#fff', 
    fontSize: 16, 
    fontWeight: '500'
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    flex: 1, 
    alignItems: 'center', 
    backgroundColor: 'rgba(24,174,195,.7)',
    padding: 10, 
    borderColor: '#fff'
  },
  buttonText: {
    color: '#fff', 
    fontWeight: '600'
  }
})

export default connect(
  state => ({...state}), 
  dispatch => ({actions: bindActionCreators({...visitorActions}, dispatch)})
)(QuickVisit);
