import React from 'react';
import {View, Text, ListView, TouchableOpacity, Alert} from 'react-native';
import SearchBar from '../../components/search_bar.js';
import {MainStyle} from '../../styles/mainStyle.js';
import * as Util from '../../components/utils';
import Icon from "react-native-vector-icons/EvilIcons"
import IIcon from "react-native-vector-icons/Ionicons"
import {Bars} from 'react-native-loader';
import { fetchApi, userInfo } from '../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../const/Apis.js';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';
import * as modalActions from '../../actions/modal_actions';


class AddVisitors extends React.Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.state={
      searchText: '',
      ds: this.ds.cloneWithRows([]),
      data:[],
      loading:true,
      renderPlaceholderView: false
    }
  }
  
  componentDidMount(){
    const { actions } = this.props;
    return actions.loadHistory();
  }

  componentWillReceiveProps(nextProps){
    const {visitors} = this.props;
    if(nextProps.visitors.history != visitors.history)
      this.setState({
        ds: this.ds.cloneWithRows(nextProps.visitors.history)
      });
  }

  filter(visitors, query){
    const regex = new RegExp(`${query}`,'gi');
    const filteredKeys = Object.keys(visitors).filter(key => regex.test(visitors[key].name));
    return filteredKeys.map(key => visitors[key])
  }

  onChange(query){
    const {visitors} = this.props; 
    if(query == ''){
      this.setState({
        searchText: query,
        ds: this.ds.cloneWithRows(visitors.history),
      });
      return;
    }

    let filteredData = this.filter(visitors.history, query);
    this.setState({
      searchText: query,
      ds: this.ds.cloneWithRows(filteredData)
    });
  }

  renderPlaceholderView(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }

  /**
   * Adds nonexistent visitor to visit (other than the selected in list)
   * @param  object visitor contains id and name
   */
  addNewVisitor(visitor){
    const {actions, visitors} = this.props;
    const existName = Object.keys(visitors.history).filter(key => visitors.history[key].name == this.state.searchText);
    
    if(existName.length > 0){
      Alert.alert('Error','Visitor name already exists', [{
          text: 'Aceptar'
        }]);
      return;
    }

    if(this.state.searchText == ''){
      Alert.alert('Error','Please add a valid visitor', [{
          text: 'Aceptar'
        }]);
      return;
    }
    // create a non existent id to be added to visitVisitor state
    let flag = true;
    let newId;
    while(flag){
      newId = Math.random()*100;
      if(!visitors.visitVisitors[newId])
        flag = false
    }
    actions.addHistoryVisitors([{
      id: newId,
      name: this.state.searchText
    }]);
    actions.addVisitVisitor({
      id: newId,
      name: this.state.searchText
    });
  }

  /**
   * Toggles check by adding visitor to visitVisitor state (redux)
   * @param  object visitor contains id and name
   */
  toggleVisitorSelection(visitor){
    const {visitors, actions} = this.props
    if(visitors.visitVisitors[visitor.id]){
      actions.removeVisitVisitor(visitor.id)
    }else{
      actions.addVisitVisitor(visitor)
    }
  }

  /**
   * Helper to toggle check icon
   * @param  object visitor contains id and name
   */
  selectedVisitor(visitor){
    const { visitors } = this.props
    if(visitors.visitVisitors[visitor.id] != null)
      return true;
    return false
  }

  render(){
    const { visitors } = this.props
    const {searchText, renderPlaceholderView, ds} = this.state;
    if(renderPlaceholderView) 
      return this.renderPlaceholderView();
    return (
      <View style={{flex: 1, marginTop: 80}}>
        {/*Search Bar*/}
        <View style={{flexDirection: 'row', justifyContent: 'center'}}> 
          <SearchBar
            styleContainer={{backgroundColor:'#C9C9CE', flex: 1}}
            style={MainStyle.searchBar}
            value={searchText}
            placeholder='Search'
            onChange={this.onChange.bind(this)}
            IconName={"search"}
            IconPosition={"left"}
          />
          <TouchableOpacity 
            style={{backgroundColor: '#C9C9CE'}}
            onPress={() => this.addNewVisitor()}
          >
            <IIcon size={40} name='md-add' color='#fff' style={{marginHorizontal: 5}}/>
          </TouchableOpacity>
        </View>
        { ds.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              renderRow={ data =>
                <Row 
                  visitor={data} 
                  toggleVisitorSelection={this.toggleVisitorSelection.bind(this)}
                  selectedVisitor={this.selectedVisitor.bind(this)}
                />
              }
              dataSource={ds}
              enableEmptySections
            />
          </View>:
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }
      </View>
    )
  }
}

const Row = ({visitor, toggleVisitorSelection, selectedVisitor})=>(
  <View style={{flex:1,alignItems:"center",borderBottomWidth:1,borderColor:"#e4e1e1",paddingVertical:10,justifyContent:"space-between",paddingLeft:15,paddingRight:15,backgroundColor:"#fff",flexDirection:"row"}}>
    <Text>{visitor.name}</Text>
    <TouchableOpacity onPress={()=> toggleVisitorSelection(visitor)}>
      <Icon name={ selectedVisitor(visitor) ? "check":"minus" } size={35} color={ selectedVisitor(visitor) ? "green":"gray"}/>
    </TouchableOpacity>
  </View>
)


const getHistory = (state) => 
  Object.keys(state.visitors.history).map(id => state.visitors.history[id]);

const mapStateToProps = state => ({
  visitors: {
    ...state.visitors,
    history: getHistory(state)
  }
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...visitorActions, ...modalActions}, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AddVisitors);

