import React, { Component } from 'react';
import { View, Text, TextInput, ListView} from 'react-native';
import SearchBar from '../../components/search_bar.js';
import ListItem from './list_item.js';
import {MainStyle} from '../../styles/mainStyle';
import { Bars } from 'react-native-loader';
import moment from 'moment';
import { fetchApi, userInfo } from '../../components/utils.js';
import { COMMON_AREA, VISITOR, VISIT } from '../../const/Apis.js';
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as visitorActions from '../../actions/visitors_actions';

class History extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.state = {
      searchText: '',
      ds: this.ds,
      visitors: [],
      renderPlaceholderOnly: true
    }
  }

  componentDidMount(){
    const { actions } = this.props;
    return actions.loadHistory()
  }

  componentWillReceiveProps(nextProps){
    const {allVisitorsHistory} = this.props;
    if(nextProps.allVisitorsHistory != allVisitorsHistory)
      this.setState({
        ds: this.ds.cloneWithRows(nextProps.allVisitorsHistory),
        renderPlaceholderOnly: false
      });
  }


  renderPlaceholderView(){
    return(
      <View style={{marginTop: 15, justifyContent: 'center',flex: 1, alignItems:'center'}}>
        <Bars size={15} color="#18AEC3" />
      </View>
    )
  }

  onChange(query){
    const {allVisitorsHistory} = this.props;
    if(query == ''){
      this.setState({
        searchText: query, 
        ds: this.ds.cloneWithRows(allVisitorsHistory)
      });
      return;
    }

    let filterData = this.filterVisitors(query);
    this.setState({
      searchText: query, 
      ds: this.ds.cloneWithRows(filterData)
    });
  }

  filterVisitors(query){
    const {allVisitorsHistory} = this.props;
    const regex = new RegExp(`${query}`,'gi');
    return allVisitorsHistory.filter(r => regex.test(r.name));
  }

  render(){
    const { searchText, ds, visitors } = this.state;

    if(this.state.renderPlaceholderOnly){
      return this.renderPlaceholderView();
    }

    return(
      <View style={{flex: 1, marginBottom: 50}}>
        <SearchBar
          styleContainer={{backgroundColor:'#C9C9CE'}}
          style={MainStyle.searchBar}
          value={searchText}
          placeholder='Search'
          onChange={this.onChange.bind(this)}
        />
        {
          ds.rowIdentities[0].length ?
          <ListView
            renderRow={(visitor) => <ListItem {...visitor}/>}
            dataSource={ds}
            enableEmptySections
          /> :
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }
      </View>
    )
  }
}

const mapStateToProps = state => ({...state.visitors});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...visitorActions }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(History);