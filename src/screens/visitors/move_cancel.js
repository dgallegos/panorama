import React, {Component} from 'react';
import { Text, View } from 'react-native';
import Modal from '../../components/modal.js';
import {Actions} from 'react-native-router-flux';
import DatePicker from '../../components/date_picker.js';
import update from 'react-addons-update';
import moment from 'moment';

const actions = {
  move: {
    description: 'Move this visit for another date',
    title: 'Move'
  },
  cancel: {
    description: 'Do you want to cancel the visit',
    title: 'Cancel'
  }
};


class MoveCancel extends Component{
  state = {
    disabled: false,
    scheduled_datetime: moment()
  }

  acceptModal(){
    this.props.actionAccept();
  }

  closeModal(){
    Actions.pop();
  }



  render(){
    console.log(this.props)
    return(
      <Modal
        title={`${actions[this.props.action].title}`}
        accept={this.acceptModal.bind(this)}
        cancel={this.closeModal} 
        disabledButton={this.state.disabled} 
      >
        <View style={{padding: 20}}>
          <Text style={{textAlign: 'center'}}>
            {actions[this.props.action].description}
          </Text>
          {
              this.props.action == 'move' &&
              <DatePicker
                date={this.state.scheduled_datetime}
                mode="datetime"
                placeholder="Select Date"
                format="DD/MM/YY h:mm a"
                customStyles={{
                  dateInput: {
                    margin: 10,
                    borderWidth: 0
                  },
                  dateText:{
                    paddingLeft: 50
                  }
                }}
                onChange={scheduled_datetime => {
                  this.props.onChange('scheduled_datetime', scheduled_datetime);
                  this.setState({
                    scheduled_datetime
                  })
                }}
              />
            }
        </View>
      </Modal>
    )
  }
}

export default MoveCancel;
