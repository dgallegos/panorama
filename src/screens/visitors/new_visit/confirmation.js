import React from 'react';
import {View, Text} from 'react-native';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';

class Confirmation extends React.Component{
  componentDidMount(){
    this.props.actions.changeHeightModal(180);
  }

  render(){
    const {visitors, scheduled_datetime, prev, confirm, location} = this.props
    
    return(
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 17}}>
            Confirmation Access
          </Text>
          <Text style={{fontSize: 13, marginTop: 8, textAlign: 'center'}}>
             {Object.keys(visitors).length} Visitor(s) {scheduled_datetime.format('DD/MM/YY hh:mm a')} to {location}
          </Text>
        </View>
        <ModalButtons 
          oneLabel='Cancel' 
          twoLabel='Confirm' 
          oneAction={prev}
          twoAction={confirm}
        />
      </View>
    )
  }
}


export default Confirmation;