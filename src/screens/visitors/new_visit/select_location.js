import React from 'react';
import {View, Text, Picker} from 'react-native';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';
import DatePicker from '../../../components/date_picker.js';

class Access extends React.Component{
  componentDidMount(){
    this.props.actions.changeHeightModal(250);
  }

  render(){
    const {prev, next, onChange, locations, location} = this.props
    return (
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1}}>
        <ModalButtonsTop 
          oneLabel='Back' 
          twoLabel='Next' 
          oneAction={prev}
          twoAction={next}
          middleText='Select Access'
        />
        <Picker
          selectedValue={location}
          onValueChange={(location_id)=> onChange({location_id})}
        >
          {
            locations.map(option => 
              <Picker.Item 
                key={option.id}
                label={option.name} 
                value={option.id}
              />)
          }
        </Picker>
      </View>
    )
  }
};

export default Access;