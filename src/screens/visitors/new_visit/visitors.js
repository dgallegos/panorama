import React from 'react';
import {View, Text, TextInput} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Button from '../../../components/button.js';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';
import Icon from 'react-native-vector-icons/Ionicons';

class Visitors extends React.Component{
  componentDidMount(){
    this.props.actions.changeHeightModal(180);
  }

  render(){
    const { cancel, next, actions, visitors } = this.props;
    
    return (
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1, borderRadius: 12}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 17}}>
            New Visitor
          </Text>
          <Text style={{fontSize: 13, marginTop: 8}}>
            I authorize access to
          </Text>
          <View style={{width: 200, marginTop: 15, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold'}}>Add Visitors</Text>
            <Button 
              style={{marginHorizontal: 10}}
              onPress={()=> {
                actions.toggleModal()
                Actions.add_visitors()
              }}
            >
              <Icon name='ios-add-circle-outline' color='#18AEC3' size={30}/>
            </Button>
          </View>
          <Text>{Object.keys(visitors).length} Visitante(s)</Text>
        </View>
        <ModalButtons 
          oneLabel='Cancel' 
          twoLabel='Next' 
          oneAction={cancel}
          twoAction={next}
        />
      </View>
    )
  }
};

export default Visitors;

