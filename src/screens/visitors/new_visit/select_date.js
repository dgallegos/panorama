import React from 'react';
import {View, Text} from 'react-native';
import {ModalButtons, ModalButtonsTop} from '../../../components/modal_buttons.js';
import DatePicker from '../../../components/date_picker.js';
import moment from 'moment';
class SelectDate extends React.Component{
  componentDidMount(){
    this.props.actions.changeHeightModal(180);
  }

  render(){
    const {prev, next, date, onChange, limit_date} = this.props
    return (
      <View style={{backgroundColor:'rgba(240,240,240,0.8)', flex: 1}}>
        <ModalButtonsTop 
          oneLabel='Back' 
          twoLabel='Next' 
          oneAction={prev}
          twoAction={next}
          middleText='Select entry date'
        />
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <DatePicker
            date={date.format('DD/MM/YY hh:mm a')}
            mode="datetime"
            placeholder="Select Date"
            format="DD/MM/YY hh:mm a"
            label='Entry Date'
            customStyles={{
              dateInput: {
                margin: 10,
                borderWidth: 0,
              },
              dateText:{
                paddingLeft: 50,
              }
            }}
            onChange={scheduled_datetime => 
              onChange({scheduled_datetime: moment(scheduled_datetime, "DD/MM/YY hh:mm a")})
            }
          />
          <DatePicker
            date={limit_date.format('DD/MM/YY hh:mm a')}
            mode="datetime"
            placeholder="Select Date"
            format="DD/MM/YY hh:mm a"
            label='Exit Date'
            customStyles={{
              dateInput: {
                margin: 10,
                borderWidth: 0,
              },
              dateText:{
                paddingLeft: 50,
              }
            }}
            onChange={limit_scheduled_datetime => 
              onChange({limit_scheduled_datetime: moment(limit_scheduled_datetime, "DD/MM/YY hh:mm a")})
            }
          />
        </View>
      </View>
    )
  } 
};

export default SelectDate;