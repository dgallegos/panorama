import React, {Component} from 'react';
import { View, Text, Image,Dimensions,StyleSheet,ScrollView, Platform} from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import Carousel from 'react-native-snap-carousel';
import HTMLView from 'react-native-htmlview';
import {MainStyle} from '../styles/mainStyle';
// Components
import Details from './event/details.js';
import Booking from './event/booking.js';
import Invite from './event/invite.js';
import BlurImage from './event/blur_image.js';
import * as Utils from '../components/utils.js';
import * as Apis from '../const/Apis.js'
//Language
import I18n from 'react-native-i18n'
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;


export default class Event extends Component{
  renderImage(img){
    let image;
    
    if(img.hasOwnProperty('path'))
      image = { uri:`${Apis.crm}/${img.path}`};
    else
      image = require('../assets/Event-1-1.png');

    return(
      <Image
       resizeMode={'stretch'}
       style={{flex:1,width:Dimensions.get('window').width}}
       source={image}
     />
    )
  }

  renderEvent(){
    const {event} = this.props;
    return(
      <ScrollableTabView
        style={{flex: 1, alignItems: 'stretch'}}
        renderTabBar={() =>
          <DefaultTabBar
            backgroundColor='black'
            inactiveTextColor='#9B9B9B'
            activeTextColor='#18AEC3'
            tabStyle={{ paddingBottom: 0}}
            textStyle={{fontWeight: 'normal', fontSize: 15}}
            underlineStyle={{backgroundColor: 'white', }}
            style={{height: 40}}
          />
        }
      >
        <Details tabLabel={I18n.t("details")} event={event} style={{flex:1}}/>
        <Booking tabLabel={I18n.t("booking")} event={event}/>
        <Invite tabLabel={I18n.t("invite")} event={event}/>
      </ScrollableTabView>
    )
  }

  renderNews(){
    const {event} = this.props;
    return(
      <ScrollView>
        <View style={{flex: 1, marginHorizontal: 10, padding: 10,marginVertical:10}}>
          <HTMLView value={`<div>${event.descriptions[I18n.locale]}</div>`} stylesheet={HtmlStyle}/>
        </View>
      </ScrollView>
    )
  }

  render(){
    const {event} = this.props;
    
    let image;
    if(event.images.hasOwnProperty(0))
      image = {uri: `${Apis.crm}/${event.images[0].path}`};
    else
      image = require('../assets/Event-1-1.png');
    // console.log(image)
    return(
      <View style={MainStyle.tabTopDetail}>
        <View>
          <Carousel
            items={event.images}
            renderItem={this.renderImage}
            sliderWidth={Dimensions.get('window').width}
            itemWidth={Dimensions.get('window').width}
            slideStyle={styles.slide}
            showsHorizontalScrollIndicator={false}
            snapOnAndroid={true}
            removeClippedSubviews={false}
          />
        <View style={styles.title}>
            <Text style={{color: 'white', fontSize: 18}}>
              {event.titles[I18n.locale]}
            </Text>
          </View>
        </View>

        
        <BlurImage blur='dark' image={image}>
          {event.type=="event" ? this.renderEvent() : null}
          {event.type=="news" ? this.renderNews() : null}
        </BlurImage>
      </View>
    )
  }
}

const bodyColor="#fff";
const HtmlStyle = StyleSheet.create({
  div:{
    marginHorizontal:10,
    color:bodyColor
  },
  strong:{
    color:bodyColor
  },
  p:{
    color:bodyColor
  },
  li:{
    color:bodyColor
  },
  blockquote:{
    color:"gray"
  }
});

const styles = StyleSheet.create({
  title:{
    position:"absolute",
    backgroundColor:"transparent",
    justifyContent:"flex-end",
    alignItems:"flex-start",
    top:130,
    paddingLeft:30,
    paddingBottom:35,
    height:100,
    width:500
  },
  slide:{
    height:215,
    flex:1
  }
})
