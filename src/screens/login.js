import React, { Component } from 'react';
import { StyleSheet, Text, View, Image,AsyncStorage,StatusBar,Platform, Alert} from 'react-native';
import { Actions,ActionConst,Scene } from 'react-native-router-flux';
import {Bars} from 'react-native-loader';
import buffer from 'buffer';
import Video from 'react-native-video';
import I18n from 'react-native-i18n';
import KeyboardSpacer from 'react-native-keyboard-spacer';
var resolveAssetSource = require('react-native/Libraries/Image/resolveAssetSource'); // for android Apk Release
// Components
import TextInput from '../components/text_input.js';
import Button from '../components/button.js';
import * as Utils from '../components/utils';
//Lang
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: '',
      password: '',
      error:'',
      loading:false,
      paused:false,
    }
  }


  onSubmit(){
    this.setState({
      loading:true
    });

    const {name, password} = this.state;

    let b = new buffer.Buffer(JSON.stringify({
      'user': name.toLowerCase().trim(),
      'password': password.trim()
    }));

    Utils.login(b.toString('base64'))
    .then(result=>{
      // console.log(result, this)
      this.setState({
        loading:false
      });
      if(result.valid){
        this.props.onLog()
        if(result.isDefaultPassword){
          Actions.reset_password({type: ActionConst.RESET});//Go to Reset Password Screen
        }else{
          Actions.tabbar({type: ActionConst.RESET});//Go to Home Screen
        }
      }else{
        Alert.alert(result.message ||I18n.t('login_problem'), null ,[{
          text: I18n.t('accept')
        }]);
      }
    })
  }

  repeatVideo(){
    this.player.seek(0)
    this.setState({
      paused:false
    });
  }

  render() {
    let VideoSource = Platform.OS=="android" ? {uri: "videoapp"} : resolveAssetSource(require('../assets/video/VideoAPP.mp4')) || {} ;
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />
        <Video
          ref={(ref) => {
            this.player = ref
          }}
          source={VideoSource}
          rate={1}
          style={styles.video}
          paused={this.state.paused}
          muted={true}
          repeat
          resizeMode="cover"
          />
        <Image source={require('../assets/logo.png')} style={{marginBottom: 60}}/>
        <TextInput
          onChange={name => this.setState({name: name.toLowerCase()})}
          placeholder='Username or Email'
          value={this.state.name}
          inputStyles={{textAlign: 'center', color: 'white'}}
          placeholderTextColor={'white'}
          containerStyles={styles.input}
        />
        <TextInput
          secureTextEntry={true}
          onChange={password => this.setState({password})}
          placeholder='Password'
          value={this.state.password}
          inputStyles={{textAlign: 'center', color: 'white'}}
          placeholderTextColor={'white'}
          containerStyles={[{marginTop: 20}, styles.input]}
        />
      <Button style={styles.button} onPress={()=>this.onSubmit()}>
            <Text style={styles.buttonText}>Let the Journey Begin</Text>
        </Button>
        {
          (this.state.loading)?<Bars size={10} color="#FFF"/>:null
        }
        <Text style={{color:'red'}}>{this.state.error}</Text>
        <KeyboardSpacer/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    // backgroundColor: 'blue'
  },
  input: {
    borderColor: 'white',
    borderWidth: 1,
    marginRight: 65,
    marginLeft: 65,
    height: 40
  },
  button: {
    margin: 10,
    marginRight: 65,
    marginLeft: 65,
    backgroundColor:'#18AEC3',
    height: 40,
    alignSelf: 'stretch'

  },
  buttonText: {
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
    paddingTop: 11,
    backgroundColor: 'transparent'
 },
 video:{
   position:'absolute',
   top: 0,
   left: 0,
   bottom: 0,
   right: 0
 }
});
