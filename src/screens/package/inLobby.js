import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity} from 'react-native';
//npm Package
import _ from 'lodash';
import moment from "moment";
import {Bars} from 'react-native-loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
//Components
import SearchBar from '../../components/search_bar.js';
import {MainStyle} from '../../styles/mainStyle';
import * as Util from '../../components/utils';
import * as Apis from '../../const/Apis.js'
//Language
import I18n from 'react-native-i18n'
import Lang from 'panorama/src/const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class InLobby extends Component{
  constructor(props){
    super(props);
    this.dsSend = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.dsReceiving = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2});
    this.state = {
      searchText: '',
      dsSend: this.dsSend.cloneWithRows([]),
      dsReceiving:this.dsReceiving.cloneWithRows([]),
      send:[],
      receiving:[],
      loading:true,
    }
  }

  componentWillMount(){
    this.getData()
  }

  getData(){
    Util.userInfo().then(user=>{
      Util.fetchApi(`${Apis.PACKAGE}?resident=${user.resident_id}`)
      .then(result=>{
        console.log(result);
        var send = _.filter(result.data,(data)=>{
          return data.flow_type=='2' && data.delivery_status==null
      });

        var receiving = _.filter(result.data,(data)=>{
          return data.flow_type=='1' && data.delivery_status==null || data.delivery_type=='Interno'
        })

        this.setState({
          dsSend: this.dsSend.cloneWithRows(send),
          dsReceiving: this.dsReceiving.cloneWithRows(receiving),
          send,
          receiving,
          loading:false
        })

      })
    })
  }

  onChange(query){
    if(query == ''){
      this.setState({
        searchText: query,
        dsSend: this.dsSend.cloneWithRows(this.state.send),
        dsReceiving: this.dsReceiving.cloneWithRows(this.state.receiving)
      });
      return;
    }

    let send = this.filter(this.state.send, query);
    let receiving = this.filter(this.state.receiving, query);

    this.setState({
      searchText: query,
      dsSend: this.dsSend.cloneWithRows(send),
      dsReceiving: this.dsReceiving.cloneWithRows(receiving)
    });
  }

  filter(array, query){
    const regex = new RegExp(`${query}`,'gi');
    return array.filter(r => regex.test(r.description) || regex.test(r.name_sender_name));
  }

  _renderListViews(){
    const { searchText, dsSend, dsReceiving, favorites, last,loading } = this.state;
    return(
      <View style={{flex:1}}>
        {/*Favorites*/}
        <View style={{marginVertical: 10, marginHorizontal: 20}}>
          <Text style={{color: '#9B9B9B'}}>To send</Text>
        </View>

        { dsSend.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              renderRow={data =><Row data={data}/>}
              dataSource={dsSend}
              enableEmptySections
            />
          </View>:
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>{I18n.t("no_package")}</Text>
          </View>
        }
      {/*Suggestions*/}
        <View style={{marginVertical: 10, marginHorizontal: 20}}>
          <Text style={{color: '#9B9B9B'}}>To pickup</Text>
        </View>

        { dsReceiving.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              style={{flex:1}}
              renderRow={data => <Row data={data}/> }
              dataSource={dsReceiving}
              enableEmptySections
            />
          </View>
           :
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>{I18n.t("no_package")}</Text>
          </View>
        }
      </View>

    )
  }

  render(){
    const { searchText, dsSend, dsReceiving, favorites, last,loading } = this.state;
    return(
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#EFEFEF'}}>
        {/*Search Bar*/}
        <SearchBar
          styleContainer={{backgroundColor:'#C9C9CE'}}
          style={MainStyle.searchBar}
          value={searchText}
          placeholder='Search'
          onChange={this.onChange.bind(this)}
          IconName={"search"}
          IconPosition={"left"}
        />
      {
        (loading) ?
          <View style={MainStyle.centerOnScreen}>
            <Bars size={10} color="#18AEC3"/>
          </View>
           :
          this._renderListViews()
      }
      </View>
    )
  }
}

const Row = ({data})=>(
  <TouchableOpacity
    onPress={()=>Actions.package_detail({data:data})}
    style={{backgroundColor: '#fff', flexDirection: 'row', alignItems: 'center', borderWidth: 0.5, borderColor: '#D9D9D9'}}>
    <View style={{marginHorizontal: 15, marginVertical: 20}}>
      <Icon name='archive' size={20} color={"#18AEC3"}/>
    </View>
    <View >
      <View style={{flexDirection:"row",justifyContent:"space-between"}}>
        <Text style={{fontSize:10,color:"#9c9595",fontSize:12}}>{moment(data.date).format('ddd D MMM YYYY h:mm a')}</Text>
        <View style={{alignSelf:"center",borderWidth:0.5,borderColor:"#9c9595",marginHorizontal:8,height:14}}/>
        <Text style={{fontSize:10,color:"#9c9595",fontSize:12}}>{data.name_sender_name}</Text>
      </View>
        <Text style={{color:"#9c9595",fontSize:16,top:5}}>{data.description}</Text>
    </View>

    <View style={{flex: 1, alignItems: 'flex-end'}}>
      <Icon style={{marginRight: 10}} color={"#18AEC3"} name='angle-right' size={30}/>
    </View>
  </TouchableOpacity>
)
