import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity} from 'react-native';

//npm Package
import _ from 'lodash';
import moment from "moment";
import {Bars} from 'react-native-loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';

//Components
import SearchBar from '../../components/search_bar.js';
import {MainStyle} from '../../styles/mainStyle';
import * as Util from '../../components/utils';
import * as Apis from '../../const/Apis.js'


//Languaje
import I18n from 'react-native-i18n'
import Lang from 'panorama/src/const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const favorites=[
  {name: 'Caja de Pandora'},
  {name: 'Esferas del Dragon'},
  {name: 'Bibla sagrada del coran'},
  {name: 'Mi hijo'},
];

const last = [
  {name: 'Caja misteriosa de la dip wec'},
  {name: 'Carta de desalojo'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
  {name: 'Sobre blanco'},
];

export default class Sent extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})

    this.state = {
      searchText: '',
      ds: this.ds.cloneWithRows([]),
      data:[],
      loading:true,
    }
  }

  componentWillMount(){
    this.getData()
  }

  getData(){
    Util.userInfo().then(user=>{
      Util.fetchApi(`${Apis.PACKAGE}?resident=${user.resident_id}&send=true`)
      .then(result=>{
        console.log(result);
        var data = _.filter(result.data,(data)=>{
          return data.flow_type=='1' || data.flow_type==''
        });
        //
        // var receiving = _.filter(result.data,(data)=>{
        //   return data.flow_type=='1' || data.delivery_type=='Interno'
        // })

        this.setState({
          ds: this.ds.cloneWithRows(data),
          data,
          loading:false
        })

      })
    })
  }

  onChange(query){
    if(query == ''){
      this.setState({
        searchText: query,
        ds: this.ds.cloneWithRows(this.state.data),
      });
      return;
    }

    let data = this.filter(this.state.data, query);

    this.setState({
      searchText: query,
      ds: this.ds.cloneWithRows(data),
    });
  }

  filter(array, query){
    const regex = new RegExp(`${query}`,'gi');
    return array.filter(r => regex.test(r.description) || regex.test(r.name_sender_name));
  }

  _renderListViews(){
    const { searchText, ds,loading } = this.state;
    return(
      <View style={{flex:1}}>
        {/*Favorites*/}
        <View style={{marginVertical: 10, marginHorizontal: 20}}>
          <Text style={{color: '#9B9B9B'}}>My package pick in Lobby</Text>
        </View>

        { ds.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              renderRow={data =><Row data={data}/>}
              dataSource={ds}
              enableEmptySections
            />
          </View>:
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>{I18n.t("no_package")}</Text>
          </View>
        }

      </View>

    )
  }

  render(){
    const { searchText, dsSend, dsReceiving, favorites, last,loading } = this.state;
    return(
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#EFEFEF'}}>
        {/*Search Bar*/}
        <SearchBar
          styleContainer={{backgroundColor:'#C9C9CE'}}
          style={MainStyle.searchBar}
          value={searchText}
          placeholder='Search'
          onChange={this.onChange.bind(this)}
          IconName={"search"}
          IconPosition={"left"}
        />
      {
        (loading) ?
          <View style={MainStyle.centerOnScreen}>
            <Bars size={10} color="#18AEC3"/>
          </View>
           :
          this._renderListViews()
      }
      </View>
    )
  }
}

const Row = ({data})=>(
  <TouchableOpacity
    onPress={()=>Actions.package_detail({data:data})}
    style={{backgroundColor: '#fff', flexDirection: 'row', alignItems: 'center', borderWidth: 0.5, borderColor: '#D9D9D9'}}>
    <View style={{marginHorizontal: 15, marginVertical: 20}}>
      <Icon name='archive' size={20} color={"#18AEC3"}/>
    </View>
    <View >
      <View style={{flexDirection:"row",justifyContent:"space-between"}}>
        <Text style={{fontSize:10,color:"#9c9595",fontSize:12}}>{moment(data.date).format('ddd D MMM YYYY h:mm a')}</Text>
        <View style={{alignSelf:"center",borderWidth:0.5,borderColor:"#9c9595",marginHorizontal:8,height:14}}/>
        <Text style={{fontSize:10,color:"#9c9595",fontSize:12}}>{data.name_sender_name || data.company_name_sender }</Text>
      </View>
        <Text style={{color:"#9c9595",fontSize:16,top:5}}>{data.description}</Text>
    </View>

    <View style={{flex: 1, alignItems: 'flex-end'}}>
      <Icon style={{marginRight: 10}} color={"#18AEC3"} name='angle-right' size={30}/>
    </View>
  </TouchableOpacity>
)
