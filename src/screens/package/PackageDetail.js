import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
import moment from "moment";
//Language
import I18n from 'react-native-i18n'
import Lang from 'panorama/src/const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

const rows = [
  {fieldName: 'company_name_receiver',type:'string'},
  {fieldName: 'company_name_sender',  type:'string'},
  {fieldName: 'company_receiver',     type:'string'},
  {fieldName: 'company_sender',       type:'string'},
  {fieldName: 'description',          type:'string'},
  {fieldName: 'date',                 type:'date',  format:'ddd D MMM YYYY h:mm a'},
  {fieldName: 'name_receiver_name',   type:'string'},
  {fieldName: 'name_sender_name',     type:'string'}
];

const RowLine = ({label, value}) => (
  <View style={{marginHorizontal: 10, marginBottom: 5}}>
    <View style={{marginVertical: 5}}>
      <Text style={{color:'#828384',fontWeight: '300', fontSize: 12}}>{label}</Text>
      <Text style={{color:'#525552',fontWeight: '200',fontSize: 16}}>{value}</Text>
    </View>
  </View>
);


export default class PackageDetail extends Component{
    constructor(props){
      super(props)
    }

    _renderRows(){
      const {data} = this.props;

      return rows.map( (row, key) => {
          let val = data[row.fieldName];
          if(!val) return (<View key={key}/>)

          if(row.type=='date')
            val = moment(data[row.fieldName]).format(row.format)
          
          return (
            <RowLine 
              key={key} 
              label={I18n.t(row.fieldName)} 
              value={val}
            />
          )
      })
    }

    render(){
      return(
        <View style={{flex:1, marginTop: 54, backgroundColor: 'rgba(151,151,151,0.2)',}}>
          <View style={{height: 30}}></View>
          <View style={{borderWidth: 1, borderColor: '#CECED1' ,backgroundColor: '#fff', marginTop:10, marginHorizontal: 10, padding: 5, borderRadius: 2, shadowColor: "#000000", shadowOpacity: 0.1, shadowRadius: 2, shadowOffset: {height: 1, width: -1}}}>
            {this._renderRows()}
          </View>
        </View>
      )
    }
}
