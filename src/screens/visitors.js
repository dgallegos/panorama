import React, {Component} from 'react';
import { View, Text } from 'react-native';
import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';
import I18n from 'react-native-i18n'
//Components
import Today from './visitors/today.js';
import History from './visitors/history.js';
import Favorites from './visitors/favs.js';
import {MainStyle} from '../styles/mainStyle';
//Language
import Lang from '../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Visitors extends Component{
  render(){
    return(
      <View style={[MainStyle.tabTop,{backgroundColor: '#EFEFEF'}]}>
        <ScrollableTabView
          style={{flex: 1, alignItems: 'stretch'}}
          renderTabBar={() =>
            <DefaultTabBar
              backgroundColor='#1B8F9F'
              inactiveTextColor='#38BCD2'
              activeTextColor='#B8E986'
              tabStyle={{ paddingBottom: 0}}
              textStyle={{fontWeight: 'normal', fontSize: 15}}
              underlineStyle={{backgroundColor: '#B8E986'}}
              style={{height: 40}}
            />
          }
          locked
        >
          <Today tabLabel={I18n.t('today')} style={{padding: 10}}/>
          <History tabLabel={I18n.t('history')}/>
          {/*<Favorites tabLabel={I18n.t('favorites')}/>*/}
        </ScrollableTabView>
      </View>
    )
  }
}
