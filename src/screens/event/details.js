import React, { Component } from 'react';
import { View, Text , StyleSheet, ScrollView} from 'react-native';
import I18n from 'react-native-i18n'
import moment from "moment"
import 'moment/locale/es';
import _ from "lodash"
import Icon from 'react-native-vector-icons/FontAwesome';
import * as URLS from 'panorama/src/const/Apis.js';
//Components
import * as Utils from '../../components/utils.js';
//Language
import Lang from 'panorama/src/const/lang.js';
I18n.fallbacks = true
I18n.translations= Lang;

export default class Details extends Component{
  componentDidMount(){
    const {event} = this.props;
    // Utils.fetchApi(`${URLS.BOOKING_CAPACITY}`)
    // .then( capacity => console.log(capacity));
  }

  getNextDate(dates){
    var sortDate = _.sortBy(dates,['date_start',"time_start"])
    var nextDate = '';
    var firstDate= [] ;

    sortDate.forEach(date=>{
      var d = date.date_start;
      var now = moment(new Date()).format("YYYY-MM-DD HH:mm:mm");
      if(now <= d){
        nextDate = d
        firstDate.push(d);
        return true;
      }
    })

    if(nextDate!=''){
      return moment(firstDate[0]).calendar()
    }else{
      return I18n.t("no_events");
    }

  }

  render(){
    const {event} = this.props;
    let language = (I18n.locale=='es')? "es":"en";
    moment.locale(I18n.locale)
    return(
      <View style={{flex: 1, marginTop: 20}}>
        <ScrollView
          style={{flex: 1}}>
          <Text style={styles.label}>{I18n.t("about_event")}</Text>
          <Text style={styles.text}>
            {event.descriptions[language]}
          </Text>

          <Text style={styles.label}>{I18n.t("next_event")}</Text>
          <Text style={styles.text}>
            {this.getNextDate(event.dates)}
          </Text>

          <Text style={styles.label}>{I18n.t("location")}</Text>
          <Text style={styles.text}>{event.location_name}</Text>

          <Text style={styles.label}>{I18n.t("price")}</Text>
          <Text style={styles.text}>
            {Utils.formatCurrency(event.price,event.currency_code)}
          </Text>
          <Text style={styles.label}>{I18n.t("capacity")}</Text>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.text}>
                {event.capacity}
              </Text>
            </View>
            <View>
              <Icon name='user' color='#fff' size={15}/>
            </View>
          </View>

        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label:{
    color: '#38BCD2',
    marginHorizontal: 20,
    marginTop:10
  },
  text:{
    marginHorizontal: 20,
    color: 'white'
  }
})
