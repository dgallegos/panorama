import React from 'react';
import { Image,View,Platform} from 'react-native';

const BlurImage = ({children,image,blur})=>(
  <Image blurRadius={8} style={{resizeMode:"cover",flex:1}} source={image}>
    <View style={{flex:1,backgroundColor:"rgba(5, 0, 0, 0.65)"}}>
      {children}
    </View>
  </Image>
)

// export default BlurImageIOS
export default BlurImage
