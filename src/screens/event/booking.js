import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/FontAwesome'
import _ from "lodash"
import moment from "moment"
import I18n from 'react-native-i18n'
//Compeonents
import Button from '../../components/button.js';
import * as Utils from '../../components/utils.js';
import * as Apis from "panorama/src/const/Apis.js"
//Language
import Lang from 'panorama/src/const/lang'
I18n.fallbacks = true
I18n.translations= Lang;
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as asyncActions from '../../actions/async_actions.js';

/**
 * Component that enables book an event
 */
class Booking extends Component{
  constructor(props){
    super(props);
    this.state = {
      quantity:1,
      currentIndex:0,
      availableDates:[],
    }
  }

  componentDidMount(){

    const {dates} = this.props.event;
    Utils.userInfo().then((user)=>{
      this.setState({
        UserName:user.full_name
      })
    })
    this.availableDates(dates)
  }


  availableDates(dates){
    let sortedDates = _.sortBy(dates, ['date_start',"time_start"])
    let availableDates = [];

    sortedDates.forEach(date=>{
      let d = date.date_start;
      let now = moment(new Date()).format("YYYY-MM-DD HH:mm:mm");
      if(now <= d){
        availableDates.push(date);
      }
    });

    this.setState({
      availableDates
    })
  }

  handleDateAtCalendar(){
    const {quantity, availableDates, currentIndex} = this.state; 
    if(availableDates.hasOwnProperty(currentIndex))
      return `${moment(availableDates[currentIndex].date_start).calendar()} - ${moment(availableDates[currentIndex].date_end).format("HH:mm")}`
    return I18n.t('no_available_time');
  }

  handleSumCurrentIndex(){
    if((this.state.availableDates.length -1 ) > this.state.currentIndex){
      this.setState({currentIndex:this.state.currentIndex + 1})
    }
  }

  handleResCurrentIndex(){
    if(this.state.currentIndex > 0){
      this.setState({currentIndex:this.state.currentIndex - 1})
    }
  }

  onSubmit(){
    const { actions } = this.props;
    const {currentIndex, availableDates, quantity} = this.state;
    actions.disableButton()
    Utils.userInfo().then(user => {
      if(!availableDates[currentIndex]){
        actions.enableButton()
        Alert.alert(null,I18n.t('no_available_dates'), [{
          text: I18n.t('accept')
        }]);
        return;
      }
        
      const body={
        "date_event_id" : availableDates[currentIndex].id,
        quantity,
        "entity_id" : user.resident_id
      }

      Utils.fetchApi(Apis.BOOKING_EVENT,{},body,"POST")
      .then((result)=>{
          actions.enableButton()
          if(result.valid)
            Alert.alert(null,I18n.t('success_booking'), [{
              text: I18n.t('accept')
            }]);
          else
            Alert.alert(null,I18n.t('error'), [{
              text: I18n.t('accept')
            }]);
      })
    })
  }

  increment(){
    this.setState({
      quantity: this.state.quantity + 1
    });
  }

  decrement(){
    if(this.state.quantity > 0)
      this.setState({
        quantity: this.state.quantity - 1
      });
  }

  render(){
    const {event, async} = this.props;
    const {quantity, availableDates, currentIndex, UserName} = this.state;
    return(
      <View style={{marginTop: 10, flex:1}}>
        <ScrollView 
          style={{flex: 1, marginHorizontal: 20}}
          contentContainerStyle={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: 'white'}}>
            {`${I18n.t('rsvp')} ${UserName}`}
          </Text>
          <Text style={{color: '#38BCD2', marginTop: 10}}>
            {I18n.t('quantity')}
          </Text>
          <View style={{flexDirection:"row", alignItems:"center", justifyContent:"center", marginVertical: 5}}>
            <TouchableOpacity onPress={()=>this.decrement()}>
              <FIcon name="minus" size={16} color={"#fff"}/>
            </TouchableOpacity>
            <Text style={{color: 'white', marginHorizontal: 25}}>
              {quantity}
            </Text>
            <TouchableOpacity onPress={()=>this.increment()}>
              <FIcon name="plus" size={16} color={"#fff"}/>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
            <TouchableOpacity onPress={()=>this.handleResCurrentIndex()}>
              <FIcon name="angle-left" size={30} color={"#fff"}/>
            </TouchableOpacity>
            <View>
              <View style={{alignItems:"center"}}>
                <Text style={styles.label}>
                  {I18n.t('date')}
                </Text>
                <Text style={styles.text}>
                  {
                    availableDates.hasOwnProperty(currentIndex) ? 
                      moment(availableDates[currentIndex].date_start).format("ddd D MMM YYYY") :
                      I18n.t('no_available_dates')
                  }
                </Text>
              </View>
              <View style={{alignItems:"center"}}>
                <Text style={styles.label}>
                  {I18n.t('schedule')}
                </Text>
                <Text style={styles.text}>
                  {this.handleDateAtCalendar()}
                </Text>
              </View>
            </View>

            <TouchableOpacity onPress={()=>this.handleSumCurrentIndex()}>
              <FIcon name="angle-right" size={30} color={"#fff"}/>
            </TouchableOpacity>
          </View>

          <Text style={styles.label}>
            {I18n.t('location')}
          </Text>
          <Text style={styles.text}>
            {event.location_name}
          </Text>

          <Text style={styles.label}>
            {I18n.t('price')}
          </Text>
          <Text style={styles.text}>
            {Utils.formatCurrency(event.price,event.currency_code)}
          </Text>

          <Button 
            style={{backgroundColor: '#18AEC3', height: 35, marginTop: 20}}
            disabled={async.disabled}
            onPress={()=>this.onSubmit()} >
            <Text style={{color: 'white', marginVertical: 10, marginHorizontal: 60}}>
              {I18n.t('booking')}
            </Text>
          </Button>
          <Text style={[styles.text,{ fontSize:11, top:10, marginBottom: 30, textAlign: 'center'}]}>
            {I18n.t('booking_understanding')}
          </Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label:{
    color: '#38BCD2',
    marginHorizontal: 20,
    marginTop:10
  },
  text:{
    marginHorizontal: 20,
    color: 'white'
  }
});

export default connect(
  state => ({...state}), 
  dispatch => ({
    actions: bindActionCreators(asyncActions, dispatch)
  }))(Booking);
