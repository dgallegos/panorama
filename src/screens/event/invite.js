import React, { Component } from 'react';
import ReactNative, { View, Text, Dimensions, ScrollView, Alert, TextInput} from 'react-native';
import Button from '../../components/button.js';
import I18n from 'react-native-i18n';
import Lang from 'panorama/src/const/lang';
import * as Utils from '../../components/utils.js';
import * as Apis from "panorama/src/const/Apis.js";
import KeyboardSpacer from 'react-native-keyboard-spacer';
//Language
I18n.fallbacks = true
I18n.translations= Lang;
//Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as asyncActions from '../../actions/async_actions.js';


class Invite extends Component{
  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      message: ''
    }
  }

  validate(){
    if(this.state.email == ''){
      Alert.alert(null,I18n.t('no_friend_email'), [{
        text: I18n.t('accept')
      }]);
      return ;
    }

    if(this.state.name == ''){
      Alert.alert(null,I18n.t('no_friend_name'), [{
        text: I18n.t('accept')
      }]);
      return false;
    }

    return true;
  }

  onSubmit(){
    const { actions, event } = this.props;
    actions.disableButton()
    Utils.userInfo().then(user => {
      if(!this.validate()){
        actions.enableButton()
        return;
      }

      const body={
        "event_id": event.id,
        "entity_id": user.resident_id,
        "persons": [
            {
                "first_name": this.state.name.trim(),
                "email": this.state.email.trim()
            },
        ]
      }

      Utils.fetchApi(Apis.EVENT_INVITE,{},body,"POST")
      .then((result)=>{
          console.log(result)
          actions.enableButton()
          if(result.valid)
            Alert.alert(null,I18n.t('success_invite'), [{
              text: I18n.t('accept')
            }]);
          else
            Alert.alert(null,I18n.t('error'), [{
              text: I18n.t('accept')
            }]);
      })
    })
  }

  render(){
    const {event, async} = this.props;
    return(
      <View style={{alignItems: 'center', flexGrow:1, height: 1000}}>
        <ScrollView
          ref='scroll'
          style={{flexGrow: 1}}
          contentContainerStyle={{marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flexGrow: 1, height: 1000}}>
            <TextInput
              value={this.state.name}
              onChangeText={text => this.setState({name: text})}
              placeholder={I18n.t('friend_name')}
              placeholderTextColor='#fff'
              style={{height: 40, borderColor: '#fff', borderWidth: 1, fontSize: 14, marginTop: 20, color: 'white'}}
              autoCorrect={false}
            />
            <TextInput
              value={this.state.email}
              onChangeText={text => this.setState({email: text})}
              placeholder={I18n.t('friend_email')}
              placeholderTextColor='#fff'
              style={{height: 40, borderColor: '#fff', borderWidth: 1, fontSize: 14, marginTop: 20, color: 'white'}}
              autoCorrect={false}
            />
            <TextInput
              ref='message'
              style={{height: 80, borderColor: '#fff', borderWidth: 1, fontSize: 14, marginTop: 20, color: 'white'}}
              autoCorrect={false}
              value={this.state.message}
              onChangeText={text => this.setState({message: text})}
              placeholder={I18n.t('personal_msg')}
              numberOfLines={6}
              multiline={true}
              placeholderTextColor='#fff'
              onFocus={(event) => {
                setTimeout(() => {
                  let scrollResponder = this.refs.scroll.getScrollResponder();
                  scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
                    ReactNative.findNodeHandle(this.refs.message),
                    100, //additionalOffset
                    true
                  );
                }, 50);
              }}
            />
            <View style={{height: 35}}>
              <Button 
                style={{backgroundColor: '#18AEC3', height: 35, marginTop: 20}}
                disabled={async.disabled}
                onPress={()=>this.onSubmit()}>
                <Text style={{color: 'white', marginVertical: 10, marginHorizontal: 95}}>
                  {I18n.t('tell_friend')}
                </Text>
              </Button>
            </View>
            <KeyboardSpacer topSpacing={400}/>
          </View>
        </ScrollView>
        
      </View>
    )
  }
}


export default connect(
  state => ({...state}), 
  dispatch => ({
    actions: bindActionCreators(asyncActions, dispatch)
  }))(Invite);