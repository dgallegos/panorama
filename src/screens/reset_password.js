import React, { Component } from 'react';
import { StyleSheet, Text, View, Image,AsyncStorage,StatusBar, TextInput  } from 'react-native';
import { Actions,ActionConst,Scene } from 'react-native-router-flux';
import {Bars} from 'react-native-loader';
import buffer from 'buffer';
import * as Apis from "../const/Apis"
var base64 = require('base-64').decode;

//-------------Components--------------------------
import Button from '../components/button.js';
import * as Utils from '../components/utils';
import KeyboardSpacer from 'react-native-keyboard-spacer';


export default class ResetPassword extends Component {
  constructor(props){
    super(props);
    this.state = {
      password: '',
      repeatPassword: '',
      error:'',
      loading:false,
      paused:false,
    }
  }

  async setData(data){
    await AsyncStorage.setItem("@UserData:access_token",)
  }

  onSubmit(){
    this.setState({
      loading:true
    });
    const { repeatPassword, password } = this.state
    let b = new buffer.Buffer(JSON.stringify({
      'password': password.trim(),
      'repeatPassword': repeatPassword.trim()
    }));
    b = b.toString('base64')

    AsyncStorage.multiGet(['Auth','userInfo']).then((datas)=>{
      let userInfo = JSON.parse(datas[1][1]);
      fetch(`${Apis.RESETPASSWORD}/${userInfo.user_id}?action=renew`,{
        method:'POST',
        headers:{
          'access_token':'movil',
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Pass':b,
        }
      })
      .then(response=>response.json())
      .then(result=>{
        this.setState({loading:false});
        // console.log(result);
        if(result.valid){
          var Auth = base64(datas[0][1])
          Auth = JSON.parse(Auth);

          Auth.password = password;

          let newB = new buffer.Buffer(JSON.stringify(Auth));
          newB= newB.toString('base64')

          AsyncStorage.setItem("Auth",newB).then(()=>{
            Actions.tabbar({type: ActionConst.RESET});//Go to Home Screen
          })
        }
        if(!result.valid){
          this.setState({error:result.message});
        }
      })


    })

  }

  prueba(){
    obj={"user":"cpadilla","password":"s"}
    let b = new buffer.Buffer(JSON.stringify(obj));
    b= b.toString('base64')
    AsyncStorage.setItem("Auth",b)
  }

  repeatVideo(){
    console.log("end");

    this.player.seek(0)
    this.setState({paused:false});
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <View style={{position:'absolute',top: 0,left: 0,bottom: 0,right: 0}}>
            <Image style={{resizeMode:"stretch"}} source={require("../assets/t2.jpg")}>
              <View style={{flex:1,backgroundColor:"rgba(29,215,255,0.60)"}}></View>
            </Image>
        </View>
        <Image source={require('../assets/logo.png')} style={{marginBottom: 60}}/>

        <TextInput
          secureTextEntry={true}
          onChangeText={password => this.setState({password})}
          placeholder='Set new Password'
          value={this.state.name}
          style={{marginHorizontal: 65, textAlign:'center', height: 40, borderColor: '#fff', borderWidth: 1, fontSize: 14, marginTop: 20, color: 'white'}}
          placeholderTextColor={'#fff'}
        />
        <TextInput
          secureTextEntry={true}
          onChangeText={repeatPassword => this.setState({repeatPassword})}
          placeholder='Type your password Again'
          value={this.state.repeatPassword}
          style={{marginHorizontal: 65, textAlign:'center', height: 40, borderColor: '#fff', borderWidth: 1, fontSize: 14, marginTop: 20, color: 'white'}}
          placeholderTextColor={'#fff'}
        />
        <Button style={styles.button} onPress={()=>this.onSubmit()}>
          <Text style={styles.buttonText}>Change Password</Text>
        </Button>
        {
          (this.state.loading) ? <View style={{backgroundColor:"transparent"}}><Bars size={10} color="#FFF"/></View>:null
        }
        <Text style={{color:'red'}}>{this.state.error}</Text>
        <KeyboardSpacer/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,1)',
    // backgroundColor: 'blue'
  },
  input: {
    // borderColor: 'white',
    borderColor: '#fff',
    borderWidth: 1,
    marginRight: 65,
    marginLeft: 65
  },
  button: {
    margin: 10,
    marginRight: 65,
    marginLeft: 65,
    backgroundColor: 'rgba(255,255,255,0.4)',
    // backgroundColor:'#18AEC3',
    height: 40,
    // borderWidth: 1,
    // borderColor: 'white',
    alignSelf: 'stretch'

  },
  buttonText: {
    fontSize: 14,
    color: '#117787',
    textAlign: 'center',
    paddingTop: 11,
    backgroundColor: 'transparent'
 },
 video:{
   position:'absolute',
   top: 0,
   left: 0,
   bottom: 0,
   right: 0
 }
});
