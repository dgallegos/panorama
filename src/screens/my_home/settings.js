import React, {Component} from 'react';
import {Text, View, TouchableOpacity, ScrollView, PanResponder, Animated, AsyncStorage} from 'react-native';
import {Actions, ActionConst} from 'react-native-router-flux';
import update from 'immutability-helper';
import I18n from 'react-native-i18n'
// Components
import Button from '../../components/button.js';
import ScrollTimePicker from '../../components/scroll_time_picker.js';
import {ScrollCardPicker} from '../../components/scroll_time_picker_card.js';
//Language
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;

export default class Settings extends Component{
  constructor(){
    super();
    this.state = {
      elts: [],
      pressedTime: -1,
      pressedLocation: '',
    }
  }

  componentWillMount(){
    const hours = [8,9,10,11,12,13,14,15,16,17];
    this.setState({
      elts: hours
    });
  }

  render(){
    return (
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#EFEFEF'}}>
        <View style={{flex: 1}}></View>
        <Button 
          style={{backgroundColor: '#CE543D', height: 50, marginVertical: 40, marginHorizontal: 20}}
          onPress={()=> {AsyncStorage.clear();Actions.login({type: ActionConst.RESET})}}>
          <Text style={{color: 'white', textAlign: 'center', marginTop: 15, fontSize: 15}}>
            {I18n.t('logout')}
          </Text>
        </Button>
      </View>
    )
  }
}