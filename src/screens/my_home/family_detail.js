import React, { Component } from 'react';
import {View,Text,TouchableOpacity,Image,ListView} from "react-native";
import Icon from "react-native-vector-icons/EvilIcons"
import update from 'react-addons-update';
import DatePicker from 'react-native-datepicker';
import moment from "moment"
import _ from "lodash";

import {MainStyle} from '../../styles/mainStyle';
import Button from '../../components/button.js';
import * as Util from '../../components/utils';
import * as Apis from '../../const/Apis.js';
import TextInput from '../../components/text_input.js';


//Languaje
import I18n from 'react-native-i18n'
import Lang from '../../const/lang'
I18n.fallbacks = true
I18n.translations= Lang;


const per =[
  {name:"Authorizing Visitors",can:true},
  {name:"Send Package",can:true},
  {name :"Create Events",can:true},
  {name : "Booking events",can:false},
  {name : "Schedule work order",can:false},
]

export default class FamilyDetail extends Component{
  constructor(props){
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    console.log(this.props.data);
    this.state = {
      searchText: '',
      data:this.props.data,
      // permissions : (this.props.data.permissions)? _.toArray(this.props.data.permissions) : {},
      email:'',
      date: '',
      error:"",
    }
  }

  changeImage(){
    this.props.mergeData("tns prroo")
    this.setState({
      data:update(this.state.data,{
        $merge:{isUser:true}
      })
    })
  }

  createUser(){
      const {email,date} =this.state;
      if(email==''){
        this.setState({error:`${I18n.t("email")} ${I18n.t("not_null")}`})
        return
      }

      if(!Util.validateEmail(email)){
        this.setState({error:`${I18n.t("email")} ${I18n.t("incorrect")}`})
        return
      }

      if(date==''){
        this.setState({error:`${I18n.t("birth_date")} ${I18n.t("not_null")}`})
        return
      }

      this.setState({error:''})

      var body={
        email:this.state.email.toLowerCase(),
        id:this.state.data.child.id
      }

      Util.fetchApi(`${Apis.EMAIL_API}`,{},body,"POST")
      .then(result=>{
        console.log(result);
        if(result.valid){
          Util.fetchApi(`${Apis.CREATE_FAMILY_USER}?role=familiar_movil`,{},body,"POST")
          .then(users=>{
            console.log(users);
          })
        }
      })
  }

  setPermission(data,rowID){
    const id=this.state.data.user.id;
    const per=data.name

    Util.fetchApi(`${Apis.FAMILY_PERMISSION}/${id}?permission=${data.name}`)
    .then(result=>{
      if(result.valid){
        data.can=result.can

        this.setState({
          permissions:update(this.state.data.permissions,
            {
              [rowID]: {
                $merge: {...data}
              }
            })
        })

      }
    })
  }

  onChange(date){
    this.setState({
      date
    })
  }

  _renderCreateUser(){
    const { data } = this.state;
    return(
      <View>
        <View style={{paddingHorizontal:20,justifyContent:"flex-start",paddingVertical:20,paddingHorizontal:10}}>
          <Text style={{fontWeight:"500"}}>{I18n.t("no_user")}</Text>
          <Text style={{fontSize:10,fontWeight:"100"}}>{I18n.t("please_create_user")}</Text>
        </View>
        <View style={{backgroundColor:"#fff",top:5,paddingHorizontal:5}}>
          <TextInput
            value={this.state.email}
            onChange={email => this.setState({email}) }
            placeholder={`${data.child.first_name} ${data.child.last_name}' ${I18n.t("email").toLowerCase()}`}
            placeholderTextColor='#c9c9c9'
            inputStyles={{height:40, textAlign: 'left', color: 'black', marginLeft: 10}}
            containerStyles={{borderBottomWidth:1, borderColor: '#c9c9c9',marginTop:5, height: 40}}
            keyboardType={"email-address"}
          />
          <DatePicker
            style={{width: 280}}
            date={this.state.date}
            mode="date"
            placeholder={`${data.child.first_name} ${data.child.last_name}' ${I18n.t("birth_date").toLowerCase()}`}
            format="ddd D MMM YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                margin: 10,
                borderWidth: 0,
                justifyContent:"center",
                alignItems:"flex-start",
                left:0,
                borderBottomWidth:1,
                borderColor:"#c9c9c9"
              }
            }}
            showIcon={false}
            onDateChange={this.onChange.bind(this)}
          />
        </View>
        <Text style={{color:"red",textAlign:"center",top:10}}>{this.state.error}</Text>
          <Button style={{backgroundColor: '#18AEC3', height: 35, marginTop: 50,alignItems:"center",justifyContent:"center",bottom:5}}
            onPress={()=>this.createUser()}>
            <Text style={{alignSelf:"center",color: 'white',textAlign:"center"}}>{I18n.t("create_user")}</Text>
          </Button>
      </View>

    )
  }

  _renderPermissions(){
    const { data } = this.state;
    return(
      <View>
        <View style={{paddingLeft:20,justifyContent:"flex-start",paddingVertical:20,paddingHorizontal:10}}>
          <Text style={{fontWeight:"500"}}>
            {I18n.t("permissions")}
          </Text>
          <Text style={{fontSize:10,fontWeight:"100"}}>{I18n.t("permissions_info")}</Text>
        </View>
        <View style={{flex:1}}>
          <ListView
            renderRow={(data,sectionID, rowID) =><Row data={data} rowID={rowID} setPermission={this.setPermission.bind(this)}/>}
            dataSource={this.ds.cloneWithRows(this.state.data.permissions)}
            enableEmptySections
          />
        </View>
      </View>
    )
  }

  render(){
    const {ds,data} = this.state;
    return(
      <View style={[MainStyle.detailScreen,{backgroundColor:"#e4e1e1",flex:1}]}>

        <View style={{paddingLeft:20,backgroundColor:"#fff",justifyContent:"flex-start",alignItems:"center",paddingVertical:20,flexDirection:"row"}}>
          <TouchableOpacity onPress={()=>this.changeImage()}>
            <Image
              source={require("../../assets/panorama.jpg")}
              style={{width:50,height:50,resizeMode:"contain",borderRadius:25}}/>
          </TouchableOpacity>
          <View style={{marginHorizontal:10}}>
              <Text style={{fontWeight:"600"}}>{`${data.child.first_name} ${data.child.last_name}`}</Text>
              <Text style={{fontWeight:"100"}} >{data.relationshipType.name}</Text>
              <Text style={{fontWeight:"100"}}>{data.child.birth_date}</Text>
          </View>
        </View>
        {
          (!data.isUser)? this._renderCreateUser.bind(this)() : this._renderPermissions.bind(this)()
        }

      </View>
    );
  }

}

const Row = ({data,setPermission,rowID}) => (
  <View style={{flex:1,alignItems:"center",borderBottomWidth:1,borderColor:"#e4e1e1",paddingVertical:10,justifyContent:"space-between",paddingLeft:15,paddingRight:15,backgroundColor:"#fff",flexDirection:"row"}}>
    <Text>{I18n.t(data.name)}</Text>
    <TouchableOpacity onPress={()=>setPermission(data,rowID)}>
      <Icon name={ (data.can) ? "check":"minus" } size={35} color={(data.can) ? "green":"gray"}/>
    </TouchableOpacity>
  </View>
)
