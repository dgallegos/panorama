import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity,Image} from 'react-native';

//npm Package
import _ from 'lodash';
import moment from "moment";
import {Bars} from 'react-native-loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import update from 'react-addons-update';

//Components
import SearchBar from '../../components/search_bar.js';
import {MainStyle} from '../../styles/mainStyle';
import * as Util from '../../components/utils';
import * as Apis from '../../const/Apis.js';
import FormFamily from './family_form.js';
import Modall from "../../components/modal.js"

export default class Family extends Component{
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({rowHasChanged: (r1,r2) => r1 !== r2})
    this.state = {
      searchText: '',
      ds: this.ds.cloneWithRows([]),
      data:[],
      loading:true,
      FormData:''
    }
  }

  componentWillMount(){
    this.getData()
  }

  getData(){
    Util.userInfo().then(user=>{
      Util.fetchApi(`${Apis.FAMILY}/${user.resident_id}`)
      .then(result=>{
        console.log(result);

        this.setState({
          ds: this.ds.cloneWithRows(result.data),
          data:result.data,
          loading:false
        })

      })
    })
  }

  mergeData(obj){
    console.log(data);
  }

  onChange(query){
    if(query == ''){
      this.setState({
        searchText: query,
        ds: this.ds.cloneWithRows(this.state.data),
      });
      return;
    }

    let data = this.filter(this.state.data, query);

    this.setState({
      searchText: query,
      ds: this.ds.cloneWithRows(data),
      openModal:false,
    });
  }

  filter(array, query){
    const regex = new RegExp(`${query}`,'gi');
    console.log(array);
    return array.filter(r => regex.test(r.child.first_name) || regex.test(r.child.last_name));
  }

  _renderListViews(){
    const { searchText, ds,loading } = this.state;
    return(
      <View style={{flex:1}}>

        { ds.rowIdentities[0].length ?
          <View style={{flex:1}}>
            <ListView
              renderRow={data =><Row data={data} mergeData={this.mergeData.bind(this)}/>}
              dataSource={ds}
              enableEmptySections
            />
          </View>:
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>No results</Text>
          </View>
        }

      </View>

    )
  }

  openModal(FormData){
    console.log(FormData);
    this.setState({modalOpen:true,FormData})
  }


  render(){
    const { searchText, dsSend, dsReceiving, favorites, last,loading } = this.state;
    return(
      <View style={{flex: 1, marginBottom: 50, backgroundColor: '#EFEFEF'}}>
        {/*Search Bar*/}
        <SearchBar
          styleContainer={{backgroundColor:'#C9C9CE'}}
          style={MainStyle.searchBar}
          value={searchText}
          placeholder='Search'
          onChange={this.onChange.bind(this)}
          IconName={"search"}
          IconPosition={"left"}
        />
      {
        (loading) ?
          <View style={MainStyle.centerOnScreen}>
            <Bars size={10} color="#18AEC3"/>
          </View>
           :
          this._renderListViews()
      }

      </View>
    )
  }
}

const Row = ({data,mergeData})=>(
  <TouchableOpacity
    onPress={()=>Actions.family_detail({data:data,mergeData:mergeData}) }
    style={{backgroundColor: '#fff', flexDirection: 'row', alignItems: 'center', borderWidth: 0.5, borderColor: '#D9D9D9'}}>
    <View style={{marginHorizontal: 15, marginVertical: 20}}>
      <Image
        source={require("../../assets/panorama.jpg")}
        style={{width:50,height:50,resizeMode:"contain",borderRadius:25}}/>
    </View>
    <View >
      <Text style={{color:"#9c9595",fontSize:16}}>{`${data.child.first_name} ${data.child.last_name}`}</Text>
      <Text style={{fontSize:10,color:"#9c9595",fontSize:12}}>{data.relationshipType.name}</Text>
    </View>

    <View style={{flex: 1, alignItems: 'flex-end'}}>
      <Icon style={{marginRight: 10}} color={"#18AEC3"} name='angle-right' size={30}/>
    </View>
  </TouchableOpacity>
)
