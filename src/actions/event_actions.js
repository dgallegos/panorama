import * as types from './types.js';

export const addEventsNews = (events, news) => ({
  type: types.ADD_EVENTS_NEWS,
  events,
  news
});