import * as types from './types.js';

export const setUser = res => ({
  type: types.SET_USER,
  res
});