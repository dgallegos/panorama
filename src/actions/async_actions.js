import * as types from './types.js';

export const disableButton = () => ({
  type: types.DISABLE_BUTTON
});

export const enableButton = () => ({
  type: types.ENABLE_BUTTON
});

export const fetchSuccess = success_field => ({
  type: types.FETCH_SUCCESS,
  success_field
});

export const fetchFailure = failure_field => ({
  type: types.FETCH_FAILURE,
  failure_field
});

export const fetchRequest = (success_field, failure_field)=> ({
  type: types.FETCH_REQUEST,
  success_field,
  failure_field
});

export const setRefreshing = refreshing => ({
  type: types.IS_REFRESHING,
  refreshing
});