import * as types from './types.js';

export const toggleModal = component => ({
  type: types.TOGGLE_MODAL,
  component
});

export const changeHeightModal = height => ({
  type: types.CHANGE_MODAL_HEIGHT,
  height
});

export const incrementStep = () => ({
  type: types.INCREMENT_STEP
});

export const decrementStep = () => ({
  type: types.DECREMENT_STEP
})

