import * as types from './types.js';
import {fetchApi, userInfo} from '../components/utils.js';
import {VISIT, COMMON_AREA} from '../const/Apis.js';
import moment from 'moment';
import * as asyncActions from './async_actions.js';

/**
 * Loads today's visitors
 * @param  array visitors 
 */
export const loadVisitors = visitors => ({
  type: types.LOAD_VISITORS,
  visitors
});

/**
 * Set current visitor
 * @param  array visitor   
 */
export const setCurrentVisitor = visitor => ({
  type: types.SET_CURRENT_VISITOR,
  visitor
});


/**
 * Adds all history of visitors (visitor can appear many times)
 * @param array visitors
 */
export const populateHistory = visitors => ({
  type: types.POPULATE_HISTORY,
  visitors
})

/**
 * Adds common areas to state
 * @param array areas
 */
export const populateCommonAreas = areas => ({
  type: types.POPULATE_COMMON_AREAS,
  areas
})

/**
 * flush visitVisitors and history
 */
export const flushState = () => ({
  type: types.FLUSH_STATE
});

/**
 * Adds history of visitors by id (unique)
 * @param  array visitors 
 */
export const addHistoryVisitors = visitors => ({
  type: types.LOAD_HISTORY,
  visitors
});

/**
 * Adds visitors to the visit to be created
 * @param  array visitors 
 */
export const addVisitVisitor = (visitor) => ({
  type: types.ADD_VISITOR_TO_VISIT,
  visitor
});

/**
 * Remove visitor from visit
 * @param integer id 
 */
export const removeVisitVisitor = id => ({
  type: types.REMOVE_VISITOR_FROM_VISIT,
  id
});

/**
 * Updates visit form
 * @param string name, name of input in form
 * @param string value, value of input
 */
export const updateVisitForm = (name, value) => ({
  type: types.UPDATE_VISIT_FORM,
  name,
  value
});

/**
 * Updates visitor form in batch
 * @param string name, name of input in form
 * @param string value, value of input
 */
export const updateBatchVisitorForm = form => ({
  type: types.UPDATE_BATCH_VISITOR_FORM,
  form
});  

export const addVisitor = visitors => ({
  type: types.ADD_VISITOR,
  visitors
});

export const removeVisitor = visitors => ({
  type: types.REMOVE_VISITOR,
  visitors
});


export const toggleModal = component => ({
  type: types.TOGGLE_MODAL,
  component
})

//Async actions
export const saveVisitor = (data, cb) => {
  return (dispatch, getState) => {
    return userInfo().then( userData => {
      const {visitors} = getState();
      const visitVisitors = Object.keys(visitors.visitVisitors).reduce((result, key) => {
        result.push(visitors.visitVisitors[key].name)
        return result
      },[]);

      const area = visitors.commonAreas.length ? 
        visitors.commonAreas.filter(area => area.name == data.location)[0].id : 
        data.location
      // Save visit
      let body = {
        common_area_id: area,
        scheduled_datetime: visitors.form.scheduled_datetime,
        limit_scheduled_datetime: visitors.form.limit_scheduled_datetime,
        visitVisitors,
        condo_name: userData.condominiums.hasOwnProperty(0) ? userData.condominiums[0].name : '',
        resident_id: userData.resident_id,
        resident_name: userData.full_name,
        status: 'Pendiente',
        created_by: userData.user_id
      };
      

      dispatch(asyncActions.fetchRequest('fetch_success_visit', 'fetch_failure_visit'));
      // POST visitors
      fetchApi(VISIT,{},body,'POST')
      .then(data=> {
        dispatch(asyncActions.fetchSuccess('fetch_success_visit'));
      })
      .catch(err => dispatch(asyncActions.fetchFailure('fetch_failure_visit')))
    });
  }
}

export const loadHistory = () => {
  return dispatch => {
    return userInfo().then(userData=>{
      fetchApi(`${VISIT}?resident=${userData.resident_id}&date=history`)
      .then(data => {
        let visitors = [];
        visitors = data.reduce((result, visit) => {
          visit.visitVisitors.forEach(visitor => {
            result.push({
              id: visitor.visitor.id,
              name: visitor.visitor ? visitor.visitor.name: '',
              location: visit.commonArea ? visit.commonArea.name : '',
              time: moment(visit.scheduled_datetime).format('hh:mm'),
              date: moment(visit.scheduled_datetime).format('dd/MM/YY') 
            })
          });
          return result;
        }, [])
        
        dispatch(addHistoryVisitors(visitors))
        dispatch(populateHistory(visitors))
      })
    });
  }
}

export const loadCommonAreas = () => {
  return dispatch => {
    return fetchApi(`${COMMON_AREA}`).then(data => {
      dispatch(populateCommonAreas(data.data))
    })
    // .catch(err => dispatch(asyncActions.fetch))
  }
}

export const loadToday = cb => {
  return dispatch =>{
    return userInfo().then(userData => {
      dispatch(asyncActions.fetchRequest('fetch_success_today', 'fetch_failure_today'));
      fetchApi(`${VISIT}?resident=${userData.resident_id}&from=now`)
      .then(data => {
        console.log(data)
        dispatch(asyncActions.fetchSuccess('fetch_success_today'));
        let visitors = [];
        // get visitors
        data.forEach(visit =>{
          let v = visit.visitVisitors.map(visitVisitor => {
            return {
              id: visitVisitor.visitor.id,
              name: visitVisitor.visitor.name, 
              location: visit.commonArea ? visit.commonArea.name : '', 
              date: moment(visit.scheduled_datetime).format('DD/MMM h:mm a'),
              status: visitVisitor.visitor.arrive ? 'Arrived' : 'Pending',
              visit: visit
            }
          })
          visitors = visitors.concat(v);
        });
        dispatch(loadVisitors(visitors));
        if(cb)
          cb(visitors);
      })
      .catch(err => dispatch(asyncActions.fetchFailure('fetch_failure_today')))
    });
  }
}