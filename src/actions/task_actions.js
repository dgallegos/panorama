import * as types from './types.js';
import {fetchApi, userInfo} from '../components/utils.js';
import {REQUEST, DISCIPLINE, DISCIPLINE_DATES, REQUEST_LOGS} from '../const/Apis.js';
import {Alert} from 'react-native';
import * as asyncActions from './async_actions.js';
/**
 * Populates tasks on main task view
 * @param  array tasks   
 */
export const populateTasks = tasks => ({
  type: types.POPULATE_TASKS,
  tasks
});

/**
 * Populates tasks logs
 * @param  array tasks   
 */
export const populateTaskLogs = logs => ({
  type: types.POPULATE_TASK_LOGS,
  logs
});

/**
 * Set current task log
 * @param  array tasks   
 */
export const setCurrentTaskLog = task => ({
  type: types.SET_CURRENT_TASK_LOG,
  task
});

/**
 * Populates disciplines on loading request modal
 * @param  array disciplines 
 */
export const populateDisciplines = disciplines => ({
  type: types.POPULATE_DISCIPLINES,
  disciplines
});

/**
 * Populates disciplines on loading request modal
 * @param  array disciplines 
*/
export const populateDisciplineDates = dates => ({
  type: types.POPULATE_DISCIPLINE_DATES,
  dates
});

/**
 * Populates disciplines on loading request modal
 * @param  array disciplines 
 */
export const populateDisciplineDatesCalendar = dates => ({
  type: types.POPULATE_DISCIPLINE_DATES_CALENDAR,
  dates
});

/**
 * flush taskDisciplines and form
 */
export const flushTaskState = () => ({
  type: types.FLUSH_TASK_STATE
});

/**
 * Add new discipline to taskDisciplines
 * @param  object discipline 
 */
export const addTaskDiscipline = discipline => ({
  type: types.ADD_TASK_DISCIPLINE,
  discipline
});

/**
 * Remove discipline from taskDisciplines
 * @param  object discipline 
 */
export const removeTaskDiscipline = discipline => ({
  type: types.REMOVE_TASK_DISCIPLINE,
  discipline
});

/**
 * Updates task form
 * @param string name, name of input in form
 * @param string value, value of input
 */
export const updateTaskForm = (name, value) => ({
  type: types.UPDATE_TASK_FORM,
  name,
  value
}); 

/**
 * Updates task form in batch
 * @param string name, name of input in form
 * @param string value, value of input
 */
export const updateBatchTaskForm = form => ({
  type: types.UPDATE_BATCH_TASK_FORM,
  form
}); 


// Async actions
export const loadDisciplines = () => 
  dispatch => 
      fetchApi(`${DISCIPLINE}`)
      .then(data => {
        dispatch(populateDisciplines(data.data))
      })



export const loadTaskLogs = (id) => 
  dispatch => {
      dispatch(asyncActions.fetchRequest('fetch_success_request_logs', 'fetch_failure_request_logs'));
      return fetchApi(`${REQUEST_LOGS}?task_id=${id}`)
      .then(data => {
        console.log(data)
        dispatch(asyncActions.fetchSuccess('fetch_success_request_logs'));
        dispatch(populateTaskLogs(data.data));
        return data.data;
      })
      .catch(err => {
        console.log(err)
        dispatch(asyncActions.fetchFailure('fetch_failure_request_logs'))
      })
  }

export const loadDisciplineDates = (id) => 
  dispatch => {
      dispatch(asyncActions.fetchRequest('fetch_success_discipline_dates', 'fetch_failure_discipline_dates'));
      fetchApi(`${DISCIPLINE_DATES}${id}?time=+5 days`)
      .then(data => {
        //task_scheduled
        dispatch(asyncActions.fetchSuccess('fetch_success_discipline_dates'));
        dispatch(populateDisciplineDates(data.data.task_scheduled));
      })
      .catch(err => dispatch(asyncActions.fetchFailure('fetch_failure_discipline_dates')))
  }

export const loadDisciplineDatesCalendar = (id, date) => 
  dispatch => {
      dispatch(asyncActions.fetchRequest('fetch_success_discipline_dates_calendar', 'fetch_failure_discipline_dates_calendar'));
      fetchApi(`${DISCIPLINE_DATES}${id}?date=${date}`)
      .then(data => {
        //task_scheduled
        dispatch(asyncActions.fetchSuccess('fetch_success_discipline_dates_calendar'));
        dispatch(populateDisciplineDatesCalendar(data.data.task_scheduled));
      })
      .catch(err => dispatch(asyncActions.fetchFailure('fetch_failure_discipline_dates_calendar')))
  }


export const loadTasks = (cb) => {
  return dispatch => {
    return userInfo().then( userData => {
      dispatch(asyncActions.fetchRequest('fetch_success_tasks', 'fetch_failure_tasks'));
      fetchApi(`${REQUEST}?contact_id=${userData.resident_id}`)
      .then(data => {
        dispatch(asyncActions.fetchSuccess('fetch_success_tasks'));
        cb(data);
      })
      .catch(err => dispatch(asyncActions.fetchFailure('fetch_failure_tasks')))
    })
  }
}

export const saveTask = () => {
  return (dispatch, getState) => {
    return userInfo().then( userData => {
      const {tasks} = getState();
      // add needed field for TaskDisciplines
      // tasks.taskDisciplines.forEach( discipline => discipline.discipline_id = discipline.id);

      const body = {
        ...tasks.form,
        contact_id: userData.resident_id,
        images: [],
        status_id: 3,
        priority_id: 1,
        created_by: userData.user_id
      }

      dispatch(asyncActions.fetchRequest('fetch_success_request_task', 'fetch_failure_request_task'));
      fetchApi(REQUEST,{},body,'POST')
      .then(data=> {
        if(data.valid){
          dispatch(asyncActions.fetchSuccess('fetch_success_request_task'));
        }else{
          dispatch(asyncActions.fetchFailure('fetch_failure_request_task'))
        }
        // cb(data)
      })
      .catch(err => {
        dispatch(asyncActions.fetchFailure('fetch_failure_request_task'))
      })
    })
  }
}
