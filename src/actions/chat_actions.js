import * as types from './types';

export const addMessage = messages => ({
  type: types.ADD_MESSAGE,
  messages
});

export const flushChat= () => ({
  type: types.FLUSH_CHAT
});