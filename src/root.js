import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableHighlight, TouchableOpacity, Platform} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { Router, Scene, Actions, Modal,ActionConst } from 'react-native-router-flux';
import ModalWindow from 'react-native-modalbox';
import I18n from 'react-native-i18n'
import Icon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import {Bubbles} from 'react-native-loader';
//Components
import Badge from './components/badge.js';
import {TabIcon, TabIconSVG }from './components/tab_icon.js';
import TextInput from './components/text_input.js';
import Button from './components/button.js';
//screens
import QuickVisit from './screens/visitors/quick_visit.js';
import NewVisitModal from './screens/visitors/new_visit_modal.js';
import Login from './screens/login.js';
import Main from './screens/main.js';
import Event from './screens/event.js';
import Home from './screens/home.js';
import Visitors from './screens/visitors.js';
import Request from './screens/request.js';
import ResetPassword from './screens/reset_password'
import Package from './screens/package'
import PackageDetail from './screens/package/PackageDetail'
import FamilyDetail from "./screens/my_home/family_detail"
import MakeFavorite from './screens/visitors/make_favorite.js';
import MoveCancel from './screens/visitors/move_cancel.js';
import NewVisit from './screens/visitors/new_visit.js';
import VisitForm from './screens/visitors/visit_form.js';
import NewRequest from './screens/request/new_request.js';
import TaskLogs from './screens/request/request_logs.js';
import RequestView from './screens/request/view_request.js';
import * as Utils from './components/utils';
import UserHeader from './components/user_header.js';
import AddVisitors from './screens/visitors/add_visitors.js';
import VisitCalendar from './screens/visitors/visit_calendar.js'
import TaskForm from './screens/request/task_form.js';
import TaskCalendar from './screens/request/task_calendar.js';
import Disciplines from './screens/request/new_request/disciplines.js';
import Chat from './screens/chat.js';
//Redux
import { bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';
import * as visitorActions from './actions/visitors_actions';
import * as modalActions from './actions/modal_actions';
import * as credentialActions from './actions/credential_actions';
import * as asyncActions from './actions/async_actions';
import NewRequestModal from './screens/request/new_request_modal.js';
//Language
import Lang from './const/lang.js';
I18n.fallbacks = true;
I18n.translations= Lang;
// Push Notifications
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import firebase from './firebase.js';

const RouterWithRedux = connect()(Router);

let path = /\bes?\b/;
let language = path.test(I18n.locale) ? "es":"en";

I18n.locale=language

const components = {
  new_visit_modal: NewVisitModal,
  new_request: NewRequestModal,
};

/**
 * a. Event Invite textinput
 * b. Favorites and Suggestions
 * c. Request Log
 */
const scenes = (isGuest, actions, styles, log) => Actions.create(
  <Scene key='modal' component={Modal}>
    <Scene key='root'>
      {/*Modals*/}
      <Scene
        key='access'
        direction='horizontal'
        component={QuickVisit}
        hideNavBar
      />
      <Scene
        key='make_favorite'
        direction='vertical'
        component={MakeFavorite}
        hideNavBar
      />
      <Scene
        key='move_cancel'
        direction='vertical'
        component={MoveCancel}
        hideNavBar
      />
      <Scene
        key='add_visitors'
        direction='vertical'
        component={AddVisitors}
        navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
        navigationBarStyle={styles.navbar}
        titleStyle={styles.title}
        title={I18n.t('add_visitors')}
        backButtonImage={require("panorama/src/assets/back_button.png")}
        onBack={()=>{
          Actions.pop();
          actions.toggleModal('new_visit_modal')
        }}
      />                  
      <Scene
        key='new_visit'
        title={I18n.t('announce_visit')}
        component={NewVisit}
        navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
        navigationBarStyle={styles.navbar}
        titleStyle={styles.title}
        leftButtonImage={{uri: null}}
      />
      <Scene
        key='request_disciplines'
        title={I18n.t('disciplines')}
        component={Disciplines}
        navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
        navigationBarStyle={styles.navbar}
        titleStyle={styles.title}
        leftButtonImage={{uri: null}}
        onBack={()=>{
          Actions.pop();
          actions.toggleModal('new_request')
        }}
      />
      {/*Login*/}
      <Scene
        key='login'
        component={()=><Login onLog={log} />}
        title='login'
        hideNavBar={true}
        initial={isGuest}
        type="replace"
      />
      <Scene
        key='reset_password'
        component={ResetPassword}
        title='Reset Your Password'
        type="replace"
        initial={false}
        navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
        navigationBarStyle={styles.navbar}
        titleStyle={styles.title}
        hideNavBar
      />
      {/* Tab Container */}
      <Scene
        key='tabbar'
        tabs={true}
        initial={!isGuest}
        type="replace"
        tabBarStyle={{backgroundColor: '#FAFAFA', alignItems: 'center', justifyContent: 'center'}}
      >
        {/* News */}
        <Scene key='news' title='News' icon={()=><TabIcon icon='news' text={I18n.t('news')}/>}>
          <Scene
            key='main'
            component={Main}
            title={I18n.t('news')}
            renderTitle={()=><UserHeader/>}
            hideNavBar={false}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
          />
          <Scene
            key='event'
            title={I18n.t('event')}
            component={Event}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 65, resizeMode:'stretch'}}
            navigationBarStyle={[styles.navbar,{borderBottomWidth: 0}]}
            titleStyle={{color: 'white', paddingBottom : 10}}
            hideTabBar
            backButtonImage={require("panorama/src/assets/back_button.png")}
            backButtonTextStyle={{color: "#fff"}}
          />
        </Scene>
        {/* Visitors */}
        <Scene 
          key='visitor' 
          title='Visitors' 
          icon={()=><TabIcon icon='visitor' text={I18n.t('visitors')}/>}>
          <Scene
            key='visitors'
            title ={I18n.t('visitors')}
            component={()=><Visitors/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            hideNavBar={false}
            renderRightButton={()=>
              <TouchableOpacity
                style={{paddingVertical: 10, paddingHorizontal: 20, top: -5}}
                onPress={() => Actions.visitors_form()}
              >
                <EntypoIcon name='new-message' color='#fff' size={25}/>
              </TouchableOpacity>}>
          </Scene>
          <Scene
            key='visitors_form'
            duration={0}
            title ={I18n.t('create_visit')}
            component={()=><VisitForm/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            backButtonImage={require("panorama/src/assets/back_button.png")}/>
          <Scene
            key='visit_calendar'
            duration={0}
            title ={I18n.t('visitors')}
            component={()=><VisitCalendar/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            renderBackButton={()=><View/>}
            titleStyle={styles.title}/>
        </Scene>
        {/*Package*/}
        <Scene key='package' title='package' icon={()=> <TabIconSVG icon="archive" text={I18n.t('package')}/>}>
          <Scene key='packages'
            title ={I18n.t('package')}
            component={Package}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
          />
          <Scene key='package_detail'
            title ={I18n.t('package_detail')}
            component={PackageDetail}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            backButtonImage={require("panorama/src/assets/back_button.png")}
          />
        </Scene>
        {/* Requests*/}
        <Scene key='request' title='request' icon={()=><TabIcon icon='request' text={I18n.t('request')}/>}>
          <Scene
            key='requests'
            title ={I18n.t('request')}
            component={()=><Request/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            renderRightButton={()=>
              <TouchableOpacity
                style={{paddingVertical: 10, paddingHorizontal: 20, top: -5}}
                onPress={()=> Actions.task_form()}
              >
                <EntypoIcon name='new-message' color='#fff' size={25}/>
              </TouchableOpacity>
            }
          />
          <Scene
            key='task_form'
            duration={0}
            title ={I18n.t('create_task')}
            component={()=><TaskForm/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            backButtonImage={require("panorama/src/assets/back_button.png")}
            titleStyle={styles.title}/>
          <Scene
            key='task_calendar'
            duration={0}
            title ={I18n.t('calendar')}
            component={()=><TaskCalendar/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            renderBackButton={()=><View/>}
            titleStyle={styles.title}/>
          <Scene
            key='task_logs'
            title ={I18n.t('task_history')}
            component={()=><TaskLogs/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            backButtonImage={require("panorama/src/assets/back_button.png")}
            titleStyle={styles.title}/>
        </Scene>
        <Scene
          key='assistance' 
          title='chat' 
          icon={()=> <TabIcon icon='assistance' text={I18n.t('chat')}/>}
        >
          <Scene
            key='chat'
            title ={I18n.t('chat')}
            component={()=><Chat/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
          />
          <Scene
            key='visitors_form_chat'
            duration={0}
            title ={I18n.t('create_visit')}
            component={()=><VisitForm/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            backButtonImage={require("panorama/src/assets/back_button.png")}/>
          <Scene
            key='visit_calendar_chat'
            duration={0}
            title ={I18n.t('visitors')}
            component={()=><VisitCalendar/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            renderBackButton={()=><View/>}
            titleStyle={styles.title}/>
          <Scene
            key='task_form_chat'
            duration={0}
            title ={I18n.t('create_task')}
            component={()=><TaskForm/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            backButtonImage={require("panorama/src/assets/back_button.png")}
            titleStyle={styles.title}/>
            <Scene
            key='task_calendar_chat'
            duration={0}
            title ={I18n.t('calendar')}
            component={()=><TaskCalendar/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            renderBackButton={()=><View/>}
            titleStyle={styles.title}/>
        </Scene>
        {/* Home */}
        <Scene key='home' title='My Home' icon={()=><TabIconSVG icon='cog' text={I18n.t('config')}/>}>
          <Scene
            key='my_home'
            title={I18n.t('my_home')}
            component={()=><Home/>}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
          />
          <Scene
            key='family_detail'
            title ={I18n.t('family')}
            component={FamilyDetail}
            navigationBarBackgroundImage={{uri: 'bg-gradient', height: 80, resizeMode:'stretch'}}
            navigationBarStyle={styles.navbar}
            titleStyle={styles.title}
            backButtonImage={require("panorama/src/assets/back_button.png")}
          />
        </Scene>
      </Scene>  
    </Scene>
    {/* Tab Container End*/}
  </Scene>
);

class Root extends Component {
  componentDidMount(){
    if(Platform.OS == 'ios') StatusBar.setBarStyle('light-content', true);
    const {actions} = this.props;

    actions.fetchRequest('fetch_success_credentials', 'fetch_failure_credentials');

    this.log();
    this.notifications();
  }

  async notifications(){
    FCM.requestPermissions();
    // FCM.getFCMToken().then(token => {
    //   console.log(token)  // Store this on backend DB
    // });
    // 
    const {routes} = this.props;
    const resident = await Utils.userInfo();
    const roomName = `${resident.email.split('@')[0]}_${resident.user_id}`;
    FCM.subscribeToTopic(`room-${roomName}`);

    
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
      // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
      // console.log(notif)
      if(notif.opened_from_tray){

        Actions.assistance()
        return;
      }
      // console.log(notif['gcm.notification.sender'] !== resident.full_name)
      // if(notif._notificationType == 'will_present_notification' && 
      //   notif['gcm.notification.sender'] != resident.full_name){
      //   console.log(notif.aps)
      //   FCM.presentLocalNotification({
      //     title: notif.aps.alert.title,
      //     body: notif.aps.alert.body,
      //     sound: 'default',
      //     priority: 'high',
      //     click_action: 'ACTION',
      //     icon: 'ic_launcher',
      //     show_in_foreground: true
      //   });
      // }

      if (Platform.OS === 'ios') {
        switch (notif._notificationType) {
          case NotificationType.Remote:
              notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
              break;
          case NotificationType.NotificationResponse:
              notif.finish();
              break;
          case NotificationType.WillPresent:
            if(notif['gcm.notification.sender'] != resident.full_name) notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
            break;
        }
      }
    });

  }

  async log(){
    const {actions} = this.props;
    const token = await Utils.hasToken();

    if(token){
      actions.setUser(false); // Is logged in, so is not guest
    }else{
      actions.setUser(true); // Is not logged in, so it is guest
    }
    actions.fetchSuccess('fetch_success_credentials')
    
    return token
  }

  renderLoading(){
    return(
      <View style={{flex: 1, backgroundColor: "#18AEC3", alignItems: 'center', justifyContent: 'center'}}>
        <Image source={require('./assets/logo.png')} style={{marginBottom: 30}}/>
        <Bubbles size={15} color="#fff" />
      </View>
    )
  }

  render() {
    const {modal, actions, credentials, async} = this.props
    const {is_guest} = credentials;
    const {fetch_success_credentials, fetch_failure_credentials} = async;

    let FormComponent = null;
    if(modal.component) FormComponent = components[modal.component];
    
    if(!fetch_success_credentials && !fetch_failure_credentials){
      return this.renderLoading();
    }
    
    return(
      <View style={{flex:1}}>
        <View style={{flex: 1}}>
          <RouterWithRedux scenes={scenes(is_guest, actions, styles, this.log.bind(this))}/>
          <ModalWindow 
            style={{height: modal.height , width: 300, borderRadius: 12}} 
            isOpen={modal.open}
            onClosed={() => modal.open && actions.toggleModal('RESET')}
          >
            {
              modal.component && 
              <FormComponent/>
            }
          </ModalWindow>
        </View>
      </View>
    )
  }
}

export default connect(
  state => ({...state}),
  dispatch => ({
    actions: bindActionCreators({...visitorActions, ...modalActions, ...credentialActions, ...asyncActions}, dispatch)
  })
)(Root)

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navbar: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  title: {
    color: 'white',
    textAlignVertical: 'center',
    marginTop: 5
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});
