import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import initialState from '../reducers/initial_state.js';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

const logger = createLogger();

const middleware = global.__DEV__ ? [thunk, logger]: [thunk]

const configureStore = (initialState) => {
  return createStore(
    rootReducer, 
    initialState,
    applyMiddleware(...middleware)
  )
}

export default configureStore;