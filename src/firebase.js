import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyARHm9PQMV_DceIzkYHdiDo1F2cJ556MmA",
  authDomain: "panorama-afa9a.firebaseapp.com",
  databaseURL: "https://panorama-afa9a.firebaseio.com",
  projectId: "panorama-afa9a",
  storageBucket: "panorama-afa9a.appspot.com",
  messagingSenderId: "1032292615683"
};

if(firebase.apps.length == 0)
      firebase.initializeApp(config);

export default firebase;