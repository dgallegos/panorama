import * as types from '../actions/types';
import initialState from './initial_state.js';

export default function chat(state = initialState.chat, action = {}) {
  switch (action.type) {
    case types.ADD_MESSAGE:
      return {
        ...state,
        messages: action.messages
      }

    case types.FLUSH_CHAT:
      return {
        ...state,
        messages: []
      }

    default:
      return state;
  }
}