import * as types from '../actions/types';
import initialState from './initial_state.js';

const modal = (state=initialState.modal, action={}) =>{
  switch(action.type){
    case types.TOGGLE_MODAL:{
      let s = {};
      if(action.component == 'RESET')
        s = {
          component: null,
          step: 1
        }

      return {
        ...state,
        open: !state.open,
        component: action.component,
        ...s
      }
    }

    case types.CHANGE_MODAL_HEIGHT:
      return {
        ...state,
        height: action.height
      }

    case types.INCREMENT_STEP:
      return {
        ...state,
        step: state.step + 1
      }

    case types.DECREMENT_STEP:{
      let s = {};
      if(state.step - 1 == 0){
        s = {
          open: false,
          component: null,
          step: 1
        }
      }
      return {
        ...state,
        step: state.step - 1,
        ...s
      }
    }

    default: 
      return state
  }
}

export default modal;