import * as types from '../actions/types';
import initialState from './initial_state.js';
import { combineReducers } from 'redux';

const visitors = (state=initialState.visitors, action={}) =>{
  switch(action.type){
    case types.LOAD_VISITORS:
      return {
        ...state,
        visitors: action.visitors,
      }

    case types.ADD_VISITOR:
      return {
        ...state,
        ...action.visitors
      };


    case types.POPULATE_HISTORY:
      return {
        ...state,
        allVisitorsHistory: action.visitors
      } 

    case types.SET_CURRENT_VISITOR:
      return {
        ...state,
        currentVisitor: action.visitor
      } 

    case types.UPDATE_VISIT_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          [action.name]: action.value
        }
      }

    case types.UPDATE_BATCH_VISITOR_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.form
        }
      }

    case types.POPULATE_COMMON_AREAS:
      return {
        ...state,
        commonAreas: action.areas
      } 

    case types.LOAD_HISTORY:
      const visitorsObject = action.visitors.reduce((result, visitor)=>{
        result[visitor.id] = visitor;
        return result
      },{});
      return {
        ...state,
        history: {
          ...state.history,
          ...visitorsObject
        }
      }

    case types.REMOVE_VISITOR:
      return {
        ...state,
        visitors: state.visitors.filter(v => v.id != action.visitors.id)
      }

    case types.ADD_VISITOR_TO_VISIT:
      return {
        ...state,
        visitVisitors: {
          ...state.visitVisitors,
          [action.visitor.id]: action.visitor
        }
      }

    case types.REMOVE_VISITOR_FROM_VISIT:
      return {
        ...state,
        visitVisitors: Object.keys(state.visitVisitors).reduce((result, key) => {
          if(key != action.id)
            result[key] = state.visitVisitors[key];
          return result;
        },{})
      }

    case types.FLUSH_STATE:
      return {
        ...state,
        visitVisitors: {},
        history: {}
      }

    case types.TOGGLE_MODAL:
      if(action.component == 'RESET')
        return {
          ...state,
          visitVisitors: {},
          history: {}
        }
      return state;



    default: 
      return state;
  }
}

export default visitors;