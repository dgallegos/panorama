import { combineReducers } from 'redux';
import modal from './modal.js';
import tasks from './tasks.js';
import routes from './routes.js';
import visitors from './visitors.js';
import async from './async.js';
import credentials from './credentials.js';
import chat from './chat.js';

const rootReducer = combineReducers({
  modal,
  routes,
  visitors,
  tasks,
  chat,
  credentials,
  async
});

export default rootReducer;