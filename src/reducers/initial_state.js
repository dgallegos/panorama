import moment from 'moment';

export default {
  modal: {
    height: 180,
    open: false,
    component: null,
    step: 1
  },
  visitors:{
    form: {
      common_area_id: '',
      scheduled_datetime: moment(),
      limit_scheduled_datetime: moment(),
      condo_name: '',
      resident_id: '',
      resident_name: '',
      status: 'Pendiente',
    },
    visitors: [],
    history: {},
    visitVisitors: {},
    allVisitorsHistory: {},
    commonAreas: []
  },
  tasks: {
    form: {
      due_on: moment(),
      due_ending: moment(),
      task_description: '',
      // comments: '',
      hour:'',
      date:'',
    },
    tasks: [],
    disciplines: [],
    taskDisciplines: [],
    discipline: '',
    disciplineDates: {},
    disciplineDatesCalendar: {},
    logs: [],
    taskLog: {}
  },
  async: {
    disabled: false,
    fetch_success_today: false,
    fetch_success_history: false,
    fetch_success_events: false,
    fetch_success_tasks: false,
    fetch_success_visit: false,
    fetch_failure_today: false,
    fetch_failure_history: false,
    fetch_failure_visit: false,
    fetch_failure_events: false,
    fetch_failure_tasks: false,
    fetch_failure_credentials: false,
    fetch_success_credentials: false,
    is_refreshing: false,
    fetch_success_request_task: false,
    fetch_failure_request_task: false,
    fetch_success_request_logs: false,
    fetch_failure_request_logs: false,
    loading: false
  },
  credentials: {
    is_guest: true
  },
  chat: {
    messages: [],
  },
  events: {
    
  }
};