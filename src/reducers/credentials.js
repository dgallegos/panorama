import * as types from '../actions/types';
import initialState from './initial_state.js';

export default function credentials(state = initialState.credentials, action = {}) {
  switch (action.type) {
    
    case types.SET_USER:
      return {
        ...state,
        is_guest: action.res
      };

    default:
      return state;
  }
}