import * as types from '../actions/types';
import initialState from './initial_state.js';

export default function async(state = initialState.async, action = {}) {
  switch (action.type) {
    
    case types.DISABLE_BUTTON:
      return {
        ...state,
        disabled: true
      };
    
    case types.ENABLE_BUTTON:
      return {
        ...state,
        disabled: false
      };

    case types.FETCH_REQUEST:
      return {
        ...state,
        [action.success_field]: false,
        [action.failure_field]: false,
        loading: true,
      }

    // before fetching set to false by using FETCH_REQUEST
    case types.FETCH_SUCCESS:
      return {
        ...state,
        [action.success_field]: true,
        loading: false
      }

    // call on catch
    case types.FETCH_FAILURE:
      return {
        ...state,
        [action.failure_field]: true,
        loading: false
      }

    case types.REFRESHING:
      return{
        ...state,
        is_refreshing: actions.refreshing
      }

    default:
      return state;
  }
}