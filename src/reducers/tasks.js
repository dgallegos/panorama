import * as types from '../actions/types';
import initialState from './initial_state.js';

const tasks = (state=initialState.tasks, action={}) =>{
  switch(action.type){
    case types.POPULATE_TASKS:
      return {
        ...state,
        tasks: action.tasks
      }

    case types.POPULATE_DISCIPLINES:
      return {
        ...state,
        disciplines: action.disciplines
      }

    case types.POPULATE_TASK_LOGS:
      return {
        ...state,
        logs: action.logs
      }

    case types.SET_CURRENT_TASK_LOG:
      return {
        ...state,
        taskLog: action.task
      }


    case types.POPULATE_DISCIPLINE_DATES:
      return {
        ...state,
        disciplineDates: action.dates
      }

    case types.POPULATE_DISCIPLINE_DATES_CALENDAR:
      return {
        ...state,
        disciplineDatesCalendar: action.dates
      }

    case types.ADD_TASK_DISCIPLINE:
      return {
        ...state,
        taskDisciplines: [
          ...state.taskDisciplines,
          action.discipline
        ]
      }

    case types.REMOVE_TASK_DISCIPLINE:
      return {
        ...state,
        taskDisciplines: state.taskDisciplines.filter(discipline => discipline.id != action.discipline.id)
      }

    case types.UPDATE_TASK_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          [action.name]: action.value
        }
      }

    case types.UPDATE_BATCH_TASK_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          ...action.form
        }
      }

    case types.FLUSH_TASK_STATE:
      return {
        ...state,
        taskDisciplines: [],
        form: initialState.tasks.form
      }

    case types.TOGGLE_MODAL:
      if(action.component == 'RESET')
        return {
          ...state,
          form: initialState.tasks.form
        }
      return state;

    default: 
      return state
  }
}

export default tasks;