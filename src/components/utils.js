import React from 'react';
import {AsyncStorage } from 'react-native';

export const validateEmail= (email)=> {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


export const formatCurrency = (value,currency)=>{
  value =  parseFloat(value)
  if(value < 1){
    var num = `${currency} ${value.toFixed(2)}`
    return num.toString()
  }
  if(typeof value === "number" && value > 1){
    var num = `${currency} ${value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}`
    return num.toString()
  }
  return '';
}

export const login=(b)=>{
  return fetch('https://frontdesk.panoramalife.com/api/resident/login',{
    method:'POST',
    headers:{
      'access_token':'movil',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Auth':b
    }
  })
  .then(response=>{
    if(!response.ok){
      console.log(response)
      return;
    }
    return response.json()
  })
  .catch(err=>console.log(err))
  .then(result=>{
    if(result.valid){
      return AsyncStorage.multiSet([
        ["Auth",b],
        ["access_token",result.access_token],
        ['userInfo',JSON.stringify(result.resident)]
      ])
      .then(()=>{
        return result
      });
    }
    if(!result.valid){
      return result
    }
  })
  .catch(err=>({
      valid: false
  }))
}

const getToken=()=>{
  return AsyncStorage.getItem('access_token')
}

export const removeTo=()=>{
  AsyncStorage.removeItem('access_token')
}

export const hasToken=()=>{
  return getToken()
}

export const getPermissions=()=>{
  return AsyncStorage.getItem('userInfo')
    .then(res=>JSON.parse(res))
    .catch(err=>err)
    .then(result => {
      if(result==null){
        return "no permissions";
      }
      return result.permissions;
    })
}

export const userInfo=()=>{
  return AsyncStorage.getItem('userInfo')
    .then(res => JSON.parse(res))
}

export const fetchApi =(url,headers,body,method)=>{
  return getToken().then(token=>{
    return fetch(url,{
      method: method==''?"GET":method,
      headers:{
        'access_token':token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        ...headers
      },
      body: JSON.stringify(body)
    })
    .then(res=>res.json())
  })
}
