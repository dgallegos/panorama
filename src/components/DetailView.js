import React, { Component } from 'react';
import { View, Text, TextInput, ListView, TouchableOpacity} from 'react-native';
import {MainStyle} from '../../styles/mainStyle';
import moment from "moment";


const Detail =({label,value})=>{
  return <RowView />
}

const RowView = ({label,value}) => (
  <View style={{flexDirection:"row",borderBottomWidth:1, borderColor: 'rgba(151,151,151,0.5)', backgroundColor: '#fff'}}>
    <View style={{width: 150, backgroundColor:"", padding: 20, borderRightWidth: 1, borderColor: 'rgba(151,151,151,0.5)'}}>
      <Text style={{fontWeight: '500'}}>{label}</Text>
    </View>
    <View style={{flex: 1, padding: 20}}>
      <Text>{value}</Text>
    </View>
  </View>
);

export default DetailView
