import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const Badge = ({count}) => (
  <View style={styles.badge}>
    <Text style={styles.badgeText}>{count}</Text>
  </View>
);

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: 3,
    right: 5, 
    width: 16, 
    height: 16, 
    borderRadius: 8, 
    alignItems: 'center', 
    justifyContent: 'center', 
    backgroundColor: '#FE3824'
  },
  badgeText: {
    backgroundColor: 'transparent', 
    textAlign: 'center', 
    textAlignVertical: 'center', 
    paddingVertical: 2, 
    paddingHorizontal: 4, 
    color: '#fff', 
    fontWeight: 'bold', 
    fontFamily: '.HelveticaNeueInterface-MediumP4'
  }
});

export default Badge;