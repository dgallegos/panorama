import React from 'react';
import { Text, View, StyleSheet, TouchableHighlight,TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const SwipeRow =({
  onPressEdit, 
  onPressDelete, 
  iconColor='white',
  editText='Edit',
  deleteText='Delete'
})=>(
  <View style={styles.rowBack}>
    <TouchableOpacity
      onPress={onPressEdit}
      style={[styles.backRightBtn, styles.backRightBtnLeft]}>
        <View style={{alignItems: 'center'}}>
          <Icon size={20} color={iconColor} name={'calendar'}/>
          <Text style={{color: 'white'}}>{editText}</Text>
        </View>
    </TouchableOpacity>
    <TouchableOpacity style={[styles.backRightBtn, styles.backRightBtnRight]}
      onPress={onPressDelete}>
        <View style={{alignItems: 'center'}}>
          <Icon size={20} color={iconColor} name={'trash-o'}/>
          <Text style={{color: 'white'}}>{deleteText}</Text>
        </View>
    </TouchableOpacity>
  </View>
);


const styles = StyleSheet.create({
  rowBack: {
    alignItems: 'center',
    backgroundColor: '#DDD',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
  },
  rowFront: {
    alignItems: 'center',
    backgroundColor: '#A9BCD0',
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    justifyContent: 'center',
    height: 50,
  },
  backRightBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75
  },
  backRightBtnLeft: {
    backgroundColor: '#4A90E2',
    borderWidth:1,
    borderColor:'#fff',
    right: 75
  },
  backRightBtnRight: {
    backgroundColor: '#F14040',
    borderWidth:1,
    borderColor:'#fff',
    right: 0
  },
})

export default SwipeRow;
