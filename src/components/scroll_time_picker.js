import React, {Component} from 'react';
import {Text, View, TouchableOpacity, ScrollView} from 'react-native';
import update from 'immutability-helper';

export default class ScrollTimePicker extends Component{
  constructor(props){
    super(props);
    this.state = {
      pressed: {},
      pressedOnlyOne: -10,
    }
  }

  componentDidMount(){
    this.setState({
      pressed: this.props.elts.reduce((r,c) => {
        r[c] = 0;
        return r;
      },{})
    })
  }

  onPress(i){
    const {pressed} = this.state;
    const {single} = this.props;
    const pressedValues = Object.keys(pressed).map(Number).filter(idx => pressed[idx])
    
    if(single){
      this.setState({
        pressedOnlyOne: i
      });
      return;
    }
    
    if(pressedValues.length > 0){
      const min = Math.min.apply(null, pressedValues);
      const max = Math.max.apply(null, pressedValues);
      if(i == min && i == max){
        pressed[i] = 0;
      }

      if(i > max){
        for( let j = min; j <= i; j++){
          pressed[j] = 1;
        }
      }else if(i < min){
        for(let j = i; j <= max; j++)
          pressed[j] = 1;
      }else{
        for(let j = min; j <= max; j++){
          if(j == i) continue;
          pressed[j] = 0;
        }
      }

      this.setState({
        pressed
      })
    }else{
      this.setState({
        pressed: update(this.state.pressed,{
          $merge: {
            [i]: !this.state.pressed[i]
          }
        })
      })
    }
  }

  _selectPressed(i){
    const {pressedOnlyOne, pressed} = this.state;
    const {single} = this.props;

    if(single){
      if( i == pressedOnlyOne)
        return pressedOnlyOne
    }
    return pressed[i]
  }

  render(){
    const {elts} = this.props;
    const {pressed} = this.state;
    return(
      <View style={{height: 50, marginTop: 20}}>
        <ScrollView
          automaticallyAdjustContentInsets={false}
          scrollEventThrottle={200}
          horizontal={true}
          style={{backgroundColor: '#fff', height: 60, borderBottomWidth: 1, borderBottomColor: '#939095'}}>
          {
            elts.map(i => 
              <View 
                key={i} 
                style={{flex: 1, borderRightWidth: 1, justifyContent: 'center', borderRightColor: '#939095'}}>
                <View style={{padding: 1, marginBottom: 2}}>
                  <Text>{i > 12 ? i % 12 : i % 13 } {i >= 12 ? 'PM' : 'AM'}</Text>
                </View>
                <TouchableOpacity 
                  style={{flex: 1, backgroundColor: this._selectPressed(i) ? '#EEE842' : '#fff'}}
                  onPress={() => this.onPress(i)}
                >
                  <View style={{width: 70, flex: 1, flexDirection: 'row'}}/>
                </TouchableOpacity>
              </View>
            )
          }
        </ScrollView>
      </View>
    )
  }
};