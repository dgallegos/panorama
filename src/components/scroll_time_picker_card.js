import React, {Component} from 'react';
import {Text, View, TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import update from 'immutability-helper';

export const ScrollCardPicker = ({ 
  items, 
  pressed, 
  onPress, 
  display= i => i,
  scrollViewStyles,
  cardContainerStyles,
  cardTextStyles,
  cardStyles,
  selectedBackgroundColor = '#EEE842',
  unselectedBackgroundColor = '#fff',
  renderCard,
  containerStyle,
  selectedTextColor = '#fff',
  ...scrollViewProps
}) => (
  <View style={[styles.container, containerStyle]}>
  <ScrollView
      automaticallyAdjustContentInsets={false}
      scrollEventThrottle={200}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      style={[styles.scrollView, scrollViewStyles || {}]}
      {...scrollViewProps}>
      {
        items.map((item,i)=> 
          <TouchableOpacity
            key={i} 
            style={[styles.cardContainer, cardContainerStyles || {}]}
            onPress={() => onPress(item)}
          >
            {
              renderCard ? 
                renderCard(item, pressed):
                <View style={[{backgroundColor: (pressed == item) ? selectedBackgroundColor : unselectedBackgroundColor}, styles.card, cardStyles || {}]}>
                  <Text style={[styles.cardText, cardTextStyles || {}, {color: (pressed == item) ? selectedTextColor : '#000'}]}>
                    {display(item)}
                  </Text>
                </View>                
            }
          </TouchableOpacity>
        )
      }
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container:{
    height: 50, 
  },
  scrollView:{
    height: 20
  },
  cardContainer:{
    marginHorizontal: 5
  },
  card:{ 
    paddingHorizontal: 8, 
    borderRadius: 5, 
    width: 70, 
    paddingVertical: 10
  },
  cardText:{
    textAlign: 'center'
  },
});