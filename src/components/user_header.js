import React , {Component} from "react"
import {View, Text, StyleSheet, Platform} from "react-native";
import {Actions, ActionConst } from 'react-native-router-flux';
import I18n from 'react-native-i18n'
//Language
import Lang from '../const/lang.js';
I18n.fallbacks = true;
I18n.translations= Lang;


import {userInfo} from "./utils.js"

export default class UserHeader extends Component{
  constructor(props){
    super(props)
    this.state = {
      full_name : ''
    }
  }

  componentDidMount(){
    userInfo().then( user => {
      if(user == null){
        Actions.login({type: ActionConst.RESET});
        return;
      }

      this.setState({
        full_name: user.full_name
      })
    });
  }

  render(){
    return(
      <View style={styles.container}>
        <Text style={{color:"#fff",fontSize:12}}>{I18n.t('welcome_home')}</Text>
        <Text style={{color:"#fff",fontSize:19}}>{this.state.full_name}</Text>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container:{
    flexGrow:1,
    flexDirection:"column",
    justifyContent:"flex-end",
    marginVertical:0,
    marginHorizontal:15,
    paddingVertical:(Platform.OS=='android')? 0 : 25
  }

})
