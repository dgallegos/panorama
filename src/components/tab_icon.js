import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export const TabIcon = ({icon, text}) =>(
  <View style={styles.icon}>
    <Image style={styles.imageIcon} source={{uri: icon}}/>
    <Text style={styles.text}>{text}</Text>
  </View>
);

export const TabIconSVG = ({icon, text}) =>(
  <View style={styles.icon}>
    <Icon name={icon} color={"#fff"} size={22} style={{alignSelf: 'center', color: '#18AEC3'}}/>
    <Text style={styles.text}>{text}</Text>
  </View>
);

const styles = StyleSheet.create({
  icon: {
    flex: 1, 
    alignItems: 'stretch', 
    justifyContent: 'center', 
    position: 'relative',
  },
  imageIcon: {
    width: 25, 
    height: 20, 
    alignSelf: 'center', 
    resizeMode: 'contain'
  },
  text: {
    color: '#18AEC3', 
    fontSize: 12, 
    textAlign: 'center'
  },
});
