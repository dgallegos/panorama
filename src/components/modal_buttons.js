'use strict';
import React, {Component} from 'react';
import { Text, View, TouchableOpacity, StyleSheet, TextInput, ScrollView, Picker, Alert } from 'react-native';
import Button from './button.js';

export const ModalButtons = ({oneLabel, twoLabel, oneAction, twoAction}) => (
  <View style={styles.buttonsContainer}>
    <Button 
      style={{flex: 1, alignItems: 'center', paddingVertical: 12, borderTopColor: 'rgba(77,77,77,.22)', borderTopWidth: 1, borderRightWidth: 1, borderRightColor: 'rgba(77,77,77,.22)'}}
      onPress={oneAction}
    > 
      <Text style={styles.buttonText}>{oneLabel}</Text>
    </Button>
    <Button 
      style={styles.buttonContainer}
      onPress={twoAction}
    >
      <Text style={styles.buttonText}>{twoLabel}</Text>
    </Button>
  </View>
);

export const ModalButtonsTop = ({oneLabel, twoLabel, oneAction, twoAction, middleText}) => (
  <View style={styles.buttonsContainerTop}>
    <Button 
      
      onPress={oneAction}
    > 
      <Text style={styles.buttonText}>{oneLabel}</Text>
    </Button>
    <View 
      style={styles.buttonContainerTop}>
        <Text style={{fontSize: 15}}>{middleText}</Text>
    </View>
    <Button 
      onPress={twoAction}
    >
      <Text style={styles.buttonText}>{twoLabel}</Text>
    </Button>
  </View>
)

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
  },
  buttonContainer: {
    flex: 1, 
    alignItems: 'center', 
    paddingVertical: 12, 
    borderTopColor: 'rgba(77,77,77,.22)', 
    borderTopWidth: 1
  },
  buttonText: {
    color: '#0076FF', 
    fontSize: 15
  },
  buttonContainerTop: {
    flex: 1,
    alignItems: 'center'
  },
  buttonsContainerTop: {
    flexDirection: 'row',
    backgroundColor: '#F1F1EF',
    paddingVertical: 14,
    paddingHorizontal: 18,
    justifyContent: 'center'
  }
})