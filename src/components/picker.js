'use strict'
import React, {Component} from 'react'
import {StyleSheet, View, Text, TouchableWithoutFeedback, Picker} from 'react-native'
import DismissKeyboard from 'dismissKeyboard';

/**
 * Picker component that hides on selecting an item
 * Props:
 * @label   
 * @onChange
 * @value
 * @options
 * @style
 * @keyVal: unique id of each option
 * @display: label for the selected item
 */
class Select extends Component {
  state = {
    pickerMode: 'hidden',
  }

  togglePicker(){
    DismissKeyboard()
    let mode = this.state.pickerMode === 'hidden' ? 'visible' : 'hidden'
    this.setState({pickerMode: mode})
  }

  showPicker(){
    const { value, onChange, options, keyVal, display } = this.props;
    return (
      <Picker
        selectedValue={value}
        onValueChange={onChange}
      >
        {
          options.map(option => 
            <Picker.Item 
              key={option[keyVal]}
              label={option[display]} 
              value={option[keyVal]}
            />)
        }
      </Picker>
    )
  }

  render(){
    const { value, label, options, keyVal, display } = this.props;
    const displayValue = options.filter(option => option[keyVal] == value);
    return(
      <View style={[styles.inputContainer, this.props.style || {}]}>
        <TouchableWithoutFeedback 
          onPress={this.togglePicker.bind(this)} 
        >
          <View style={styles.container}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>{label}</Text>
            </View>
            <View style={styles.labelContainer}>
              <Text style={styles.labelValue}>
                {displayValue.hasOwnProperty(0) && displayValue[0][display]}
              </Text>
             </View>
          </View>
        </TouchableWithoutFeedback>

        {/* Picker */}
        { this.state.pickerMode === 'visible' ? this.showPicker() : <View/>}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'row',
    borderBottomColor: 'rgb(197,195,201)', 
    borderBottomWidth: 1
  },
  labelContainer: {
    flex: 1, 
    paddingTop: 11, 
    paddingLeft: 10,
    paddingBottom: 10,
    height: 40
  },
  label:{
    fontWeight: '500',
    fontSize: 14
  },
  labelValue: {
    fontSize: 14
  }
});

export default Select;
