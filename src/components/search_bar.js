import React, { PropTypes } from 'react';
import { View, TextInput, StyleSheet,Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const SearchBar = ({
  value,
  placeholder='',
  onChange,
  placeholderTextColor = '#8E8E93',
  style,
  styleContainer,
  IconName,
  IconPosition
}) => (
  <View style={styleContainer}>

    <View style={{flexGrow:1}}>
      {(IconPosition=='left')?<Icon name={IconName} color={"#9c9595"} style={styles.icon}/>:null}
      <TextInput
        style={style}
        onChangeText={onChange}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        underlineColorAndroid={"transparent"}
        autoCorrect={false}
        editable
        />
      {(IconPosition=='right')?<Icon name={IconName} style={styles.icon}/>:null}
    </View>


  </View>
);

const styles= StyleSheet.create({
  icon:{
    top:15,
    left:15,
    position:"absolute",
    backgroundColor:"transparent",
    zIndex:10
  }
})
export default SearchBar;
