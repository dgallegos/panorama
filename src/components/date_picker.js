'use strict'
import React, {Component} from 'react';
import {StyleSheet, View, Text, DatePickerIOS, DatePickerAndroid,TimePickerAndroid, TouchableWithoutFeedback,Platform} from 'react-native';
import DismissKeyboard from 'dismissKeyboard';
import moment from 'moment';
import Picker from 'react-native-datepicker';

// Props: date, label
const DatePicker = ({ customStyles, format, placeholder, mode, date, onChange, label, labelContainerStyles}) => (
  <View style={styles.container}>
    {
      label&&
      <View style={[styles.labelContainer, labelContainerStyles]}>
        <Text style={styles.labelText}>
          {label}
        </Text>
      </View>
    }
    <Picker
      style={{flex: 1}}
      date={date}
      mode={mode}
      placeholder={placeholder}
      format={format}
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={customStyles}
      showIcon={false}
      onDateChange={onChange}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row', 
    marginTop: 5, 
    paddingHorizontal: 10, 
    borderBottomColor: 'rgb(197,195,201)', 
    borderBottomWidth: 1
  },
  labelContainer: {
    // flex: 1
  }, 
  labelText: {
    fontWeight: '500', 
    textAlignVertical: 'center', 
    marginTop: 10
  },
  dateText: {
    fontSize: 15
  }
});

export default DatePicker
