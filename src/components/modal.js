import React, {Component} from 'react';
import { View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import TextInput from './text_input.js';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import Button from './button.js';

const Modal=({
  title,
  accept,
  cancel,
  disabledButton,
  children
}) => (
  <View style={styles.modalOverlay}>
    <View style={styles.modalContainer}>
      <View style={styles.modalHeader}>
        <Text style={styles.modalHeaderText}>{title}</Text>
      </View>
      <View>
        {children}
        {/*Buttons*/}
        <View style={styles.buttonContainer}>
          <Button
            style={[styles.button, {borderRightWidth: 1}]}
            onPress={cancel}
          >
            <Text style={styles.buttonText}>Cancel</Text>
          </Button>
          <Button
            style={styles.button}
            onPress={accept}
            disabled={disabledButton}
          >
            <Text style={styles.buttonText}>Accept</Text>
          </Button>
        </View>
      </View>
    </View>
  </View>
)

const styles = StyleSheet.create({
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
  },
  modalContainer: {
    backgroundColor: '#fff',
    marginHorizontal: 40,
    marginVertical: 200,
  },
  modalHeader: {
    height: 40,
    backgroundColor: '#18AEC3',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalHeaderText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'rgba(24,174,195,.7)',
    padding: 10,
    borderColor: '#fff'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '600'
  }
})

export default Modal;
