import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Button = ({
  label, 
  onPress, 
  children,
  style,
  disabled=false
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={style || {}}
    disabled={disabled}
  >
    {children}
  </TouchableOpacity>
);

export default Button;