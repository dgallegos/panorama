import React, { Component } from 'react';
import { AppRegistry, View } from 'react-native';
import Root from './src/root.js';
import { connect, Provider } from 'react-redux';
import configureStore from './src/store/store.js';

const store = configureStore();

export default class panorama extends Component {
  state = {
    store
  }

  render() {
    return (
      <Provider store={this.state.store}>
        <Root/>
      </Provider>
    )
  }
}

AppRegistry.registerComponent('panorama', () => panorama);
